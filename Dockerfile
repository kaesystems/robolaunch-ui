ARG BUILDER=node:18-alpine3.15
FROM ${BUILDER}
RUN npm install -g serve
COPY . ./app
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
RUN npm i
RUN npm run build
CMD ["serve", "-s", "build"]