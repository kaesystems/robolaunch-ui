import React from 'react';

const Loader = () => {
  return (
    <div className="w-[100%] py-10 flex justify-center">
      <div className="relative flex justify-center items-center mx-auto w-32 h-auto">
        <img className="w-12 animate-spin" src="/logo/ring.svg" alt="Robolaunch" />
      </div>
    </div>
  );
};

export default Loader;
