import React from 'react';

const PageLoader = () => {
  return (
    <div className="w-[100%] py-10 flex justify-center">
      <div className="relative flex justify-center items-center mx-auto w-32 h-auto">
        <img className="absolute w-16" src="/logo/logo.svg" alt="Robolaunch" />
        <img className="animate-spin" src="/logo/ring.svg" alt="Robolaunch" />
      </div>
    </div>
  );
};

export default PageLoader;
