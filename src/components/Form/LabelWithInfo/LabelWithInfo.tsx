import React, { useContext, useState } from 'react';
import { ThemeContext } from '../../../context/ThemeContext';

interface ILabelWithInfo {
  label: string;
  info: string;
}

const LabelWithInfo = ({ label, info }: ILabelWithInfo) => {
  const { theme, setTheme } = useContext(ThemeContext);
  const [isInfoOpen, setIsInfoOpen] = useState(false);
  return (
    <div className="flex items-center py-[0.4rem]">
      <p className="text-layer-600 dark:text-lowContrast text-preTitle  font-normal z-40">
        {label}
      </p>
      <button
        className="bg-transparent border-none pl-2 relative "
        type="button"
        onMouseEnter={() => setIsInfoOpen(true)}
        onMouseLeave={() => setIsInfoOpen(false)}
        tabIndex={-1}
      >
        <img src={`/icons/${theme}/labelinfo.svg`} alt="info" width="14px" />
        {isInfoOpen && (
          <div className="absolute rounded border border-light-400 p-3 bg-white text-layer-100 font-medium bottom-full right-full z-50 min-w-[200px] text-sm">
            <p>{info}</p>
          </div>
        )}
      </button>
    </div>
  );
};

export default LabelWithInfo;
