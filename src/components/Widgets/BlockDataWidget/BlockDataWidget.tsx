import React, { useContext } from 'react';
import { ThemeContext } from '../../../context/ThemeContext';

const BlockDataWidget = (value) => {
  const { theme } = useContext(ThemeContext);

  return (
    <div className="dark:bg-blue-100 border dark:border-layer-400 rounded-2xl transition-all p-6">
      <div className="flex flex-col pb-4">
        <span className="text-lg font-semibold">General Statistics</span>
        <span className="text-sm dark:text-blue-200">{value.value.type} Base Data</span>
      </div>
      <div className="grid grid-cols-2 gap-y-12 gap-x-5">
        <div>
          <div className="flex items-center gap-2">
            <img className="w-10 scale-150" src={`/icons/${theme}/${value.value.data1.icon}.svg`} />
            <span className="text-xl font-semibold uppercase">{value.value.data1.count}</span>
          </div>
          <div className="text-base text-blue-200">{value.value.data1.title}</div>
        </div>
        <div>
          <div>
            <div className="flex items-center">
              <img
                className="w-8 scale-125"
                src={`/icons/${theme}/${value.value.data1.subdata1.icon}.svg`}
              />
              <span className="text-sm">
                {value.value.data1.subdata1.count} {value.value.data1.subdata1.subtitle}
              </span>
            </div>
            <div className="flex items-center">
              <img
                className="w-8 scale-125"
                src={`/icons/${theme}/${value.value.data1.subdata2.icon}.svg`}
              />
              <span className="text-sm">
                {value.value.data1.subdata2.count} {value.value.data1.subdata2.subtitle}
              </span>
            </div>
          </div>
        </div>

        <div>
          <div className="flex items-center gap-2">
            <img className="w-10 scale-150" src={`/icons/${theme}/${value.value.data2.icon}.svg`} />
            <span className="text-xl font-semibold uppercase">{value.value.data2.count}</span>
          </div>
          <div className="text-base text-blue-200">{value.value.data2.title}</div>
        </div>
        <div>
          <div>
            <div className="flex items-center">
              <img
                className="w-8 scale-125"
                src={`/icons/${theme}/${value.value.data2.subdata1.icon}.svg`}
              />
              <span className="text-sm">
                {value.value.data2.subdata1.count} {value.value.data2.subdata1.subtitle}
              </span>
            </div>
            <div className="flex items-center">
              <img
                className="w-8 scale-125"
                src={`/icons/${theme}/${value.value.data2.subdata2.icon}.svg`}
              />
              <span className="text-sm">
                {value.value.data2.subdata2.count} {value.value.data2.subdata2.subtitle}
              </span>
            </div>
          </div>
        </div>

        <div>
          <div className="flex items-center gap-2">
            <img className="w-10 scale-150" src={`/icons/${theme}/${value.value.data3.icon}.svg`} />
            <span className="text-xl font-semibold uppercase">{value.value.data3.count}</span>
          </div>
          <div className="text-base text-blue-200">{value.value.data3.title}</div>
        </div>
        <div>
          <div>
            <div className="flex items-center">
              <img
                className="w-8 scale-125"
                src={`/icons/${theme}/${value.value.data3.subdata1.icon}.svg`}
              />
              <span className="text-sm">
                {value.value.data3.subdata1.count} {value.value.data3.subdata1.subtitle}
              </span>
            </div>
            <div className="flex items-center">
              <img
                className="w-8 scale-125"
                src={`/icons/${theme}/${value.value.data3.subdata2.icon}.svg`}
              />
              <span className="text-sm">
                {value.value.data3.subdata2.count} {value.value.data3.subdata2.subtitle}
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BlockDataWidget;
