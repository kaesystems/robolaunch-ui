import { Field, Form, Formik } from 'formik';
import React, { useContext } from 'react';
import Button from '../Button/Button';
import InputField from '../Form/InputField/InputField';
import Modal from '../Modal/Modal';
import * as Yup from 'yup';
import slugify from 'react-slugify';
import { ThemeContext } from '../../context/ThemeContext';

interface ICreateModal {
  handleClose: any;
}

const CreateModal = ({ handleClose }: ICreateModal) => {
  const [isPage, setIsPage] = React.useState('organization');
  const mockCurrentOrg = 'Organization1';
  const { theme } = useContext(ThemeContext);

  return (
    <Modal title="Create Wizard" handleClose={handleClose}>
      <div className="w-full flex lg:block gap-4">
        <div className="w-3/12 lg:w-full lg:grid lg:grid-rows-1 lg:pb-4">
          <div className="xl:hidden">
            <select
              onChange={(e) => {
                if (e.target.value === 'robot') {
                  window.location.href = `/${slugify(mockCurrentOrg)}/deploy-robot`;
                } else {
                  setIsPage(e.target.value);
                }
              }}
              className="input-select"
              name=""
              id=""
            >
              <option value="organization">Organization</option>
              <option value="department">Department</option>
              <option value="roboticscloud">Robotics Cloud</option>
              <option value="fleet">Fleet</option>
              <option value="robot">Robot</option>
            </select>
          </div>
          <ul className="flex flex-col justify-center gap-2  xl:h-96 p-2 bg-layer-200 text-white border border-layer-500 rounded lg:hidden">
            <li
              onClick={() => setIsPage('organization')}
              className={`hover:bg-layer-400 cursor-pointer p-2 font-semibold border border-layer-400 rounded ${
                isPage === 'organization' ? 'bg-layer-500' : 'bg-layer-300'
              }`}
            >
              <div className="flex items-center gap-2">
                <img className="w-8 scale-125" src={`/icons/${theme}/organizations.svg`} />
                <span>Create Organization</span>
              </div>
            </li>
            <li
              onClick={() => setIsPage('department')}
              className={`hover:bg-layer-400 cursor-pointer p-2 font-semibold border border-layer-400 rounded ${
                isPage === 'department' ? 'bg-layer-500' : 'bg-layer-300'
              }`}
            >
              <div className="flex items-center gap-2">
                <img className="w-8 scale-125" src={`/icons/${theme}/departments.svg`} />
                <span>Create Departments</span>
              </div>
            </li>
            <li
              onClick={() => setIsPage('roboticscloud')}
              className={`hover:bg-layer-400 cursor-pointer p-2 font-semibold border border-layer-400 rounded ${
                isPage === 'roboticscloud' ? 'bg-layer-500' : 'bg-layer-300'
              }`}
            >
              <div className="flex items-center gap-2">
                <img className="w-8 scale-125" src={`/icons/${theme}/server.svg`} />
                <span>Create Robotics Cloud</span>
              </div>
            </li>
            <li
              onClick={() => setIsPage('fleet')}
              className={`hover:bg-layer-400 cursor-pointer p-2 font-semibold border border-layer-400 rounded ${
                isPage === 'fleet' ? 'bg-layer-500' : 'bg-layer-300'
              }`}
            >
              <div className="flex items-center gap-2">
                <img className="w-8 scale-125" src={`/icons/${theme}/fleet.svg`} />
                <span>Create Fleet</span>
              </div>
            </li>
            <li
              onClick={() => {
                window.location.href = `/${slugify(mockCurrentOrg)}/deploy-robot`;
              }}
              className={`hover:bg-layer-400 cursor-pointer p-2 font-semibold border border-layer-400 rounded ${
                isPage === 'robot' ? 'bg-layer-500' : 'bg-layer-300'
              }`}
            >
              <div className="flex items-center gap-2">
                <img className="w-8 scale-125" src={`/icons/${theme}/robots.svg`} />
                <span>Create Robot</span>
              </div>
            </li>
            <li
              onClick={() => setIsPage('inviteuser')}
              className={`hover:bg-layer-400 cursor-pointer p-2 font-semibold border border-layer-400 rounded ${
                isPage === 'inviteuser' ? 'bg-layer-500' : 'bg-layer-300'
              }`}
            >
              <div className="flex items-center gap-2">
                <img className="w-8 scale-125" src={`/icons/${theme}/users.svg`} alt="" />
                <span>Invite User</span>
              </div>
            </li>
          </ul>
        </div>
        <div className="w-9/12 lg:w-full lg:min-h-80 xl:!h-96 xl:overflow-auto bg-layer-200 text-white border border-layer-500 rounded">
          {isPage === 'organization' && <OrganizationPage />}
          {isPage === 'department' && <DepartmentPage />}
          {isPage === 'inviteuser' && <InviteUserPage />}
          {isPage === 'roboticscloud' && <CreateRoboticsCloud />}
          {isPage === 'fleet' && <CreateFleet />}
        </div>
      </div>
    </Modal>
  );
};

const OrganizationPage = () => {
  const CreateOrganizationSchema = Yup.object().shape({
    name: Yup.string()
      .required('Required')
      .min(3, 'Min 3 Character')
      .max(64, 'Max 64 Character')
      .matches(/^\w[a-zA-Z@#0-9.]*$/, 'Only english characters and numbers')
  });
  return (
    <div className="flex flex-col items-center p-10  animate__animated animate__fadeIn">
      <span className="text-md font-bold pb-4">Create Organization</span>
      <Formik
        initialValues={{
          name: '',
          isEnterprise: false
        }}
        onSubmit={(value) => {
          console.log(value);
        }}
        validationSchema={CreateOrganizationSchema}
      >
        <Form autoComplete="off">
          <div className="grid grid-cols-1 grid-rows-1">
            <div className="col-start-1 col-end-2">
              <p>
                Want to create a new organization? All right, so let&apos;s give the organization a
                new name.
              </p>
              <div className="my-2">
                <InputField id="name" info="name address of the user." label="Name">
                  <Field
                    className="h-text rounded p-2 dark:bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-light-100 focus:ring-1 focus:dark:ring-primary-100 placeholder:italic"
                    id="name"
                    name="name"
                    label="Name"
                    placeholder="e.g MyJobsOrganization"
                  />
                </InputField>
                <InputField id="isEnterprise" info="name address of the user." label="isEnterprise">
                  <Field className="input-checkbox my-1" name="isEnterprise" type="checkbox" />
                </InputField>
              </div>
            </div>
          </div>
          <div className="flex justify-center ">
            <Button
              disabled={false}
              style="primary"
              icon="plus"
              type="submit"
              text="Create Organization"
            />
          </div>
        </Form>
      </Formik>
    </div>
  );
};

const DepartmentPage = () => {
  const CreateDepartmentSchema = Yup.object().shape({
    departmentName: Yup.string()
      .required('Required')
      .min(3, 'Min 3 Character')
      .max(64, 'Max 64 Character')
      .matches(/^\w[a-zA-Z@#0-9.]*$/, 'Only English Characters and Numbers')
  });
  return (
    <div
      className="flex flex-col items-center p-10 animate__animated animate__fadeIn 
    "
    >
      <span className="text-md font-bold pb-4">Create Department</span>

      <Formik
        initialValues={{ departmentName: '' }}
        onSubmit={(value) => console.log(value)}
        validationSchema={CreateDepartmentSchema}
      >
        <Form className="flex flex-col items-center gap-8" action="">
          <InputField id="departmentName" info="Type Department Name" label="Department Name">
            <Field
              className="h-text rounded p-2 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
              id="departmentName"
              name="departmentName"
              placeholder="e.g. Sales"
              type="text"
              required
            />
          </InputField>
          <div className="pt-1 lg:pt-8">
            <Button
              disabled={false}
              style="primary"
              type={'submit'}
              text="Create Department"
            ></Button>
          </div>
        </Form>
      </Formik>
    </div>
  );
};

const InviteUserPage = () => {
  const InviteUserSchema = Yup.object().shape({
    email: Yup.string().email('Invalid email').required('Email is required')
  });
  return (
    <div
      className="flex flex-col items-center p-10 animate__animated animate__fadeIn
    "
    >
      <span className="text-md font-bold pb-4">Invite User</span>

      <Formik
        initialValues={{
          email: ''
        }}
        onSubmit={(values) => {
          console.log(values);
        }}
        validationSchema={InviteUserSchema}
      >
        <Form>
          <div className="grid grid-cols-2 gap-3">
            <div className="col-start-1 col-end-3">
              <InputField id="email" label="User Email Address" info="Type User Email Address">
                <Field
                  id="email"
                  name="email"
                  type="text"
                  placeholder="example@domain.com"
                  className="h-text rounded p-2 bg-light-100 dark:bg-layer-100 border border-light-400 dark:border-layer-500 outline-none focus:outline-1 focus:outline-layer-600 focus:border-none"
                  required
                />
              </InputField>
            </div>
          </div>
          <div className="w-full flex justify-center pt-6">
            <Button disabled={false} style="primary" type="submit" text="Invite User"></Button>
          </div>
        </Form>
      </Formik>
    </div>
  );
};

const CreateRoboticsCloud = () => {
  const createCloudInstanceSchema = Yup.object().shape({
    region: Yup.string().required('Required'),
    instancetype: Yup.string().required('Required'),
    instancename: Yup.string()
      .required('Required')
      .matches(/^\w[a-zA-Z@#0-9.]*$/, 'Only English Characters and Numbers')
      .min(4, 'Minimum 4 Character')
      .max(16, 'Maximum 16 Character'),
    instancedisksize: Yup.number().required('Required').min(50, 'Minimum 50 GB')
  });
  return (
    <div className="flex flex-col items-center p-10 animate__animated animate__fadeIn">
      <span className="text-md font-bold pb-4">Create Robotics Cloud</span>

      <Formik
        initialValues={{
          region: 'eu-central-1',
          instancetype: 't3.medium',
          instancename: '',
          instancedisksize: 100
        }}
        onSubmit={(values) => {
          console.log(values);
        }}
        validationSchema={createCloudInstanceSchema}
      >
        <Form>
          <InputField id="region" label="Region" info="Select Region">
            <Field className="input-select" id="region" as="select" name="region">
              <option value="eu-central-1">Europe | eu-central-1</option>
            </Field>
          </InputField>
          <InputField id="instancetype" label="Instance Type" info="Select Instance Type">
            <Field className="input-select" id="instancetype" as="select" name="instancetype">
              <option value="t3.medium">T3 Medium</option>
              <option value="t3.large">T3 Large</option>
            </Field>
          </InputField>
          <InputField id="instancename" label="Instance Name" info="Type Instance Name">
            <Field
              className="input-text"
              id="instancename"
              name="instancename"
              placeholder="eg. myCloudInstance"
            ></Field>
          </InputField>
          <InputField
            id="instancedisksize"
            label="Instance Disk Size"
            info="Type Instance Disk Size"
          >
            <Field
              className="input-text"
              type="number"
              id="instancedisksize"
              name="instancedisksize"
              placeholder="eg. 100"
            ></Field>
          </InputField>
          <div className="flex justify-center pb-2 pt-6">
            <Button
              text="Create Fleet"
              type="submit"
              disabled={false}
              style="primary"
              icon="plus"
            />
          </div>
        </Form>
      </Formik>
    </div>
  );
};

const CreateFleet = () => {
  const createFleetSchema = Yup.object().shape({
    fleetType: Yup.string().required('Required'),
    fleetName: Yup.string()
      .required('Required')
      .matches(/^\w[a-zA-Z@#0-9.]*$/, 'Only English Characters and Numbers')
      .min(4, 'Minimum 4 Character')
      .max(16, 'Maximum 16 Character')
  });
  return (
    <div className="flex flex-col items-center p-10 animate__animated animate__fadeIn">
      <span className="text-md font-bold pb-4">Create Fleet</span>

      <Formik
        initialValues={{
          fleetType: 'cloudonly',
          fleetName: ''
        }}
        onSubmit={(values) => {
          console.log(values);
        }}
        validationSchema={createFleetSchema}
      >
        <Form>
          <InputField id="fleetType" label="Fleet Type" info="Select Fleet Type">
            <Field className="input-select" id="fleetType" as="select" name="fleetType">
              <option value="cloudonly">Cloud Only Fleet</option>
              <option value="cloudpowered">Cloud Powered Fleet</option>
            </Field>
          </InputField>
          <InputField id="fleetName" label="Fleet Name" info="Type Fleet Name">
            <Field
              className="input-text"
              id="fleetName"
              name="fleetName"
              placeholder="eg. myFleet"
            ></Field>
          </InputField>
          <div className="flex justify-center pb-2 pt-6">
            <Button
              text="Create Fleet"
              type="submit"
              disabled={false}
              style="primary"
              icon="plus"
            />
          </div>
        </Form>
      </Formik>
    </div>
  );
};

export default CreateModal;
