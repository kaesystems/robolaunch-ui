import React, { useContext, useState } from 'react';
import { ThemeContext } from '../../context/ThemeContext';
import Button from '../Button/Button';
import OrganizationTableOptions from './OrganizationTableOptions';
import DepartmentTableOptions from './DepartmentTableOptions';
import RoboticsCloudsTableOptions from './RoboticsCloudsTableOptions';
import FleetTableOptions from './FleetTableOptions';
import ConnectedPhysicalInstancesTableOptions from './ConnectedPhysicalInstancesTableOptions';

interface ITable {
  title: string;
  icon: string;
  createButtonText: string;
  tableInstance: any;
  handleCreate: any;
}

const Table = ({ title, icon, tableInstance, createButtonText, handleCreate }: ITable) => {
  const [isFilterInput, setIsFilterInput] = useState('');
  const { theme } = useContext(ThemeContext);

  const handleFilterChange = (e) => {
    const value = e.target.value || undefined;
    tableInstance.setFilter('key', value);
    setIsFilterInput(value);
  };

  {
    console.log(tableInstance);
  }

  return (
    <div className="flex flex-col w-full border shadow-md dark:bg-layer-200 border-light-400 dark:border-layer-400 rounded-2xl">
      <div className="flex justify-between p-3">
        <div className="flex items-center">
          <img src={`/icons/${theme}/${icon}.svg`} />
          <span className="uppercase text-sm text-white font-bold">{title}</span>
        </div>
        <div className="flex items-center gap-2 text-sm">
          <div className="flex">
            <img className="relative w-5 left-7" src={`/icons/${theme}/search.svg`} />
            <input
              value={isFilterInput}
              onChange={handleFilterChange}
              className="input-text pl-8"
              placeholder={`My${title}`}
            />
          </div>
          <div onClick={handleCreate()}>
            <Button
              disabled={false}
              style={`primary`}
              type={`submit`}
              text={createButtonText}
              icon={`plus`}
            />
          </div>
        </div>
      </div>

      <table
        className="rounded-b bg-light-200 dark:bg-layer-200 text-sm"
        {...tableInstance.getTableProps()}
      >
        <thead>
          {tableInstance.headerGroups.map((headerGroup) => (
            <tr key={headerGroup.id} {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th
                  className={`text-left p-4 dark:text-light-100`}
                  key={column.id}
                  {...column.getHeaderProps(column.getSortByToggleProps())}
                >
                  {column.render('Header')}
                  <span>{column.isSorted ? (column.isSortedDesc ? '🔽' : '🔼') : ''}</span>
                </th>
              ))}
            </tr>
          ))}
        </thead>

        <tbody {...tableInstance.getTableBodyProps()} className="font-normal">
          {tableInstance.page?.map((row) => {
            tableInstance.prepareRow(row);
            {
              switch (title) {
                case 'Teams':
                  return <OrganizationTableOptions key={row.id} row={row} />;
                case 'Robotics Clouds':
                  return <DepartmentTableOptions key={row.id} row={row} />;
                case 'Connected Physical Instances':
                  return <ConnectedPhysicalInstancesTableOptions key={row.id} row={row} />;
                case 'Fleets':
                  return <RoboticsCloudsTableOptions key={row.id} row={row} />;
                case 'Robots':
                  return <FleetTableOptions key={row.id} row={row} />;
              }
            }
          })}
        </tbody>
      </table>
      <div className="flex items-center justify-center gap-20 p-3">
        {tableInstance.canPreviousPage ? (
          <Button
            disabled={false}
            style="primary"
            type={'submit'}
            text="<"
            handleClick={() => {
              tableInstance.previousPage();
            }}
          />
        ) : (
          <div></div>
        )}

        <div className="flex gap-2 font-medium text-sm dark:text-white">
          <span>{tableInstance.state.pageIndex + 1}</span>
          <span>/</span>
          <span>{tableInstance.pageCount}</span>
        </div>

        {tableInstance.canNextPage ? (
          <Button
            disabled={false}
            style="primary"
            type={'submit'}
            text=">"
            handleClick={() => {
              tableInstance.nextPage();
            }}
          />
        ) : (
          <div></div>
        )}
      </div>
    </div>
  );
};

export default Table;
