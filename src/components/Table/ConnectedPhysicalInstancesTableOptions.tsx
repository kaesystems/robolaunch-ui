import React, { useContext, useRef, useState } from 'react';
import { ThemeContext } from '../../context/ThemeContext';

const OrganizationTableOptions = ({ row }) => {
  const ref = useRef<HTMLUListElement>();

  const [isOptionsOpen, setIsOptionsOpen] = useState(false);

  const [isOpen, setIsOpen] = useState(false);
  const { theme } = useContext(ThemeContext);

  const handleToggleOptions = (e) => {
    e.stopPropagation();
    setIsOptionsOpen(!isOptionsOpen);
  };

  return (
    <>
      <tr
        className="odd:bg-light-300 dark:odd:bg-layer-300 relative dark:text-light-100"
        {...row.getRowProps()}
      >
        {row.cells.map((cell, index) => (
          <td className="p-4" key={index} {...cell.getCellProps()}>
            {cell.render('Cell')}
          </td>
        ))}
        <td className="w-[2rem]">
          <div className="relative">
            <div onClick={handleToggleOptions} className="cursor-pointer">
              <img src={`/icons/${theme}/dots.svg`} />
            </div>
            {isOptionsOpen && (
              <ul
                ref={ref}
                className="absolute top-100 right-full w-[120px] border border-light-400 dark:border-layer-600 rounded bg-light-200 dark:bg-layer-300 z-50"
              >
                <li className="flex items-center font-semibold hover:bg-light-300 dark:hover:bg-layer-400 p-3 cursor-pointer dark:text-light-100">
                  <img src={`/icons/${theme}/robot/edit.svg`} className="mr-2" />
                  <p>Settings</p>
                </li>
                <li className="flex items-center font-semibold hover:bg-light-300 dark:hover:bg-layer-400 p-3 cursor-pointer dark:text-light-100">
                  <img src={`/icons/${theme}/robot/edit.svg`} className="mr-2" />
                  <p>Share</p>
                </li>
                <li className="flex items-center font-semibold hover:bg-light-300 dark:hover:bg-layer-400 p-3 cursor-pointer dark:text-light-100">
                  <img src={`/icons/${theme}/robot/edit.svg`} className="mr-2" />
                  <p>Invite</p>
                </li>
                <li className="flex items-center bg-red-100 hover:bg-red-200 text-white font-semibold p-3 cursor-pointer">
                  <img src="/icons/trash.svg" className="mr-2" />
                  <p>Delete</p>
                </li>
              </ul>
            )}
          </div>
        </td>
      </tr>
    </>
  );
};

export default OrganizationTableOptions;
