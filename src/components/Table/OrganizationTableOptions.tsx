import { Field, Form, Formik } from 'formik';
import React, { useContext, useEffect, useRef, useState } from 'react';
import Modal from 'react-responsive-modal';
import { ThemeContext } from '../../context/ThemeContext';
import { emailSchema, teamSchema } from '../../schemas/Schemas';
import Button from '../Button/Button';
import InputField from '../Form/InputField/InputField';

const OrganizationTableOptions = ({ row }) => {
  const { theme } = useContext(ThemeContext);
  const ref = useRef<HTMLUListElement>();
  const [isOptionsOpen, setIsOptionsOpen] = useState(false);
  const [isShowSettingsModal, setIsShowSettingsModal] = useState(false);
  const [isShowInviteUserModal, setIsShowInviteUserModal] = useState(false);
  const [isShowDeleteModal, setIsShowDeleteModal] = useState(false);
  const [isSubmitButton, setIsSubmitButton] = useState(true);

  useEffect(() => {
    const checkIfClickedOutside = (e) => {
      if (isOptionsOpen && ref.current && !ref.current.contains(e.target)) {
        setIsOptionsOpen(false);
      }
    };
    document.addEventListener('mousedown', checkIfClickedOutside);
    return () => {
      document.removeEventListener('mousedown', checkIfClickedOutside);
    };
  }, [isOptionsOpen]);

  const handleToggleOptions = (e) => {
    e.stopPropagation();
    setIsOptionsOpen(!isOptionsOpen);
  };

  const handleSettings = () => {
    setIsShowSettingsModal(!isShowSettingsModal);
  };
  const handleInviteUser = () => {
    setIsShowInviteUserModal(!isShowInviteUserModal);
  };
  const handleDelete = () => {
    setIsShowDeleteModal(!isShowDeleteModal);
  };

  return (
    <tr
      className="odd:bg-light-300 dark:odd:bg-layer-300 relative dark:text-light-100"
      {...row.getRowProps()}
    >
      {row.cells.map((cell, index) => (
        <td className="p-4" key={index} {...cell.getCellProps()}>
          {cell.render('Cell')}
        </td>
      ))}
      <td className="w-[2rem]">
        <div className="relative">
          <div onClick={handleToggleOptions} className="cursor-pointer">
            <img src={`/icons/${theme}/dots.svg`} />
          </div>
          {isOptionsOpen && (
            <ul
              ref={ref}
              className="absolute top-100 right-full w-40 border border-light-400 dark:border-layer-600 rounded bg-light-200 dark:bg-layer-300 z-50"
            >
              <li
                onClick={() => handleSettings()}
                className="flex items-center font-semibold hover:bg-light-300 dark:hover:bg-layer-400 p-3 cursor-pointer dark:text-light-100"
              >
                <img src={`/icons/${theme}/settings.svg`} className="mr-2 w-6 scale-150" />
                <p>Settings</p>
              </li>
              <li
                onClick={() => handleInviteUser()}
                className="flex items-center font-semibold hover:bg-light-300 dark:hover:bg-layer-400 p-3 cursor-pointer dark:text-light-100"
              >
                <img src={`/icons/${theme}/add-user.svg`} className="mr-2 w-6 scale-150" />
                <p>Invite User</p>
              </li>
              <li
                onClick={() => handleDelete()}
                className="flex items-center bg-red-100 hover:bg-red-200 text-white font-semibold p-3 cursor-pointer"
              >
                <img src="/icons/trash.svg" className="mr-2" />
                <p>Delete</p>
              </li>
            </ul>
          )}
        </div>
      </td>

      <Modal open={isShowSettingsModal} onClose={() => handleSettings()} center>
        <div className="flex flex-col items-center">
          <span className="text-md font-semibold pb-4">{row.original.key} Settings</span>
          <span className="text-sm">You can change the name of the fleet you want to create.</span>
          <Formik
            initialValues={{
              teamName: ''
            }}
            onSubmit={(value) => {
              console.log(value);
              handleSettings();
            }}
            validationSchema={teamSchema}
          >
            <Form className="flex flex-col items-center gap-8">
              <InputField id="teamName" info="Type New Team Name" label="Team Name">
                <Field className="input-text" name="teamName" type="text" placeholder="NewMyTeam" />
              </InputField>
              <Button disabled={false} style="primary" type={'submit'} text={`Rename Team`} />
            </Form>
          </Formik>
        </div>
      </Modal>

      <Modal open={isShowDeleteModal} onClose={() => handleDelete()} center>
        <div className="flex flex-col items-center">
          <span className="text-md font-semibold pb-4">{row.original.key} Delete</span>
          <span className="text-sm">
            Are you sure you want to delete the team? If you are sure, write the name of the team{' '}
            <b>{row.original.key}</b>.
          </span>
          <Formik
            initialValues={{}}
            onSubmit={() => {
              console.log(row);
              handleDelete();
            }}
          >
            <Form className="flex flex-col items-center gap-8">
              <InputField id="teamName" info="Type Team Name" label="Team Name">
                <input
                  placeholder="MyTeam"
                  onChange={(e) => {
                    if (row.original.key === e.target.value) {
                      setIsSubmitButton(false);
                    } else {
                      setIsSubmitButton(true);
                    }
                  }}
                  className="input-text"
                  name="teamName"
                  type="text"
                />
              </InputField>
              <Button
                disabled={isSubmitButton}
                style="primary"
                type={'submit'}
                text={`Delete Team`}
              />
            </Form>
          </Formik>
        </div>
      </Modal>

      <Modal open={isShowInviteUserModal} onClose={() => handleInviteUser()} center>
        <div className="flex flex-col items-center">
          <span className="text-md font-semibold pb-4">Invite User to {row.original.key}</span>
          <span className="text-sm">To invite a user, enter the user&apos;s email address.</span>
          <Formik
            initialValues={{
              email: ''
            }}
            onSubmit={(value) => {
              console.log(value);
              handleInviteUser();
            }}
            validationSchema={emailSchema}
          >
            <Form className="flex flex-col items-center gap-8">
              <InputField id="email" info="Type User's Email" label="Email Address">
                <Field
                  className="input-text"
                  name="email"
                  type="text"
                  placeholder="email@domain.com"
                />
              </InputField>
              <Button
                disabled={false}
                style="primary"
                type={'submit'}
                text={`Invite User to Team`}
              />
            </Form>
          </Formik>
        </div>
      </Modal>
    </tr>
  );
};

export default OrganizationTableOptions;
