import { Form, Formik } from 'formik';
import React, { useContext, useEffect, useRef, useState } from 'react';
import Modal from 'react-responsive-modal';
import { ThemeContext } from '../../context/ThemeContext';
import Button from '../Button/Button';
import InputField from '../Form/InputField/InputField';

const RoboticsCloudsTableOptions = ({ row }) => {
  const { theme } = useContext(ThemeContext);
  const ref = useRef<HTMLUListElement>();
  const [isOptionsOpen, setIsOptionsOpen] = useState(false);
  const [isShowDeleteModal, setIsShowDeleteModal] = useState(false);
  const [isSubmitButton, setIsSubmitButton] = useState(true);

  useEffect(() => {
    const checkIfClickedOutside = (e) => {
      if (isOptionsOpen && ref.current && !ref.current.contains(e.target)) {
        setIsOptionsOpen(false);
      }
    };
    document.addEventListener('mousedown', checkIfClickedOutside);
    return () => {
      document.removeEventListener('mousedown', checkIfClickedOutside);
    };
  }, [isOptionsOpen]);

  const handleToggleOptions = (e) => {
    e.stopPropagation();
    setIsOptionsOpen(!isOptionsOpen);
  };

  const handleDelete = () => {
    setIsShowDeleteModal(!isShowDeleteModal);
  };

  return (
    <tr
      className="odd:bg-light-300 dark:odd:bg-layer-300 relative dark:text-light-100"
      {...row.getRowProps()}
    >
      {row.cells.map((cell, index) => (
        <td className="p-4" key={index} {...cell.getCellProps()}>
          {cell.render('Cell')}
        </td>
      ))}
      <td className="w-[2rem]">
        <div className="relative">
          <div onClick={handleToggleOptions} className="cursor-pointer">
            <img src={`/icons/${theme}/dots.svg`} />
          </div>
          {isOptionsOpen && (
            <ul
              ref={ref}
              className="absolute top-100 right-full w-[120px] border border-light-400 dark:border-layer-600 rounded bg-light-200 dark:bg-layer-300 z-50"
            >
              <li className="flex items-center font-semibold hover:bg-light-300 dark:hover:bg-layer-400 p-3 cursor-pointer dark:text-light-100">
                <img src={`/icons/${theme}/robot/edit.svg`} className="mr-2" />
                <p>Settings</p>
              </li>
              <li className="flex items-center font-semibold hover:bg-light-300 dark:hover:bg-layer-400 p-3 cursor-pointer dark:text-light-100">
                <img src={`/icons/${theme}/robot/edit.svg`} className="mr-2" />
                <p>Share</p>
              </li>
              <li className="flex items-center font-semibold hover:bg-light-300 dark:hover:bg-layer-400 p-3 cursor-pointer dark:text-light-100">
                <img src={`/icons/${theme}/robot/edit.svg`} className="mr-2" />
                <p>Invite</p>
              </li>
              <li
                onClick={() => handleDelete()}
                className="flex items-center bg-red-100 hover:bg-red-200 text-white font-semibold p-3 cursor-pointer"
              >
                <img src="/icons/trash.svg" className="mr-2" />
                <p>Delete</p>
              </li>
            </ul>
          )}
        </div>
      </td>
      <Modal open={isShowDeleteModal} onClose={() => handleDelete()} center>
        <div className="flex flex-col items-center">
          <span className="text-md font-semibold pb-4">{row.original.key} Delete</span>
          <span className="text-sm">
            Are you sure you want to delete the fleet? If you are sure, write the name of the fleet{' '}
            <b>{row.original.key}</b>.
          </span>
          <Formik
            initialValues={{}}
            onSubmit={() => {
              console.log(row);
              handleDelete();
            }}
          >
            <Form className="flex flex-col items-center gap-8">
              <InputField id="fleetName" info="Type Fleet Name" label="Fleet Name">
                <input
                  placeholder="MyFleet"
                  onChange={(e) => {
                    if (row.original.key === e.target.value) {
                      setIsSubmitButton(false);
                    } else {
                      setIsSubmitButton(true);
                    }
                  }}
                  className="input-text"
                  name="fleetName"
                  type="text"
                />
              </InputField>
              <Button
                disabled={isSubmitButton}
                style="primary"
                type={'submit'}
                text={`Delete Fleet`}
              />
            </Form>
          </Formik>
        </div>
      </Modal>
    </tr>
  );
};

export default RoboticsCloudsTableOptions;
