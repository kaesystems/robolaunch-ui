import React from 'react';

const RobotHero = () => {
  return (
    <div className="grid grid-cols-6 grid-rows-1 bg-light-200 dark:bg-layer-300 w-full p-4 text-sm">
      <div className="col-start-1 col-end-2 flex flex-col items-center justify-center h-full ">
        <div className="bg-light-400 dark:bg-layer-400 rounded-full p-4">
          <img src="/images/robot.png" alt="robot" />
        </div>
        <p className="font-medium p-2">robot01</p>
        <p className="font-bold">Jackal</p>
      </div>
      <div className="col-start-2 col-end-7 p-4">
        <ul className="grid grid-cols-5 grid-rows-3 gap-x-4 gap-y-2 ">
          <li className="flex items-start justify-start flex-col">
            <p className="text-sm text-layer-600 dark:text-white">Status</p>
            <p className="font-semibold text-layer-100 dark:text-white">Building</p>
          </li>
          <li className="flex items-start justify-start flex-col">
            <p className="text-sm text-layer-600 dark:text-white">Robot Name</p>
            <p className="font-semibold text-layer-100 dark:text-white">Jackal</p>
          </li>
          <li className="flex items-start justify-start flex-col">
            <p className="text-sm text-layer-600 dark:text-white">Robot Type</p>
            <p className="font-semibold text-layer-100 dark:text-white">Virtual</p>
          </li>
          <li className="flex items-start justify-start flex-col">
            <p className="text-sm text-layer-600 dark:text-white">Fleet Name</p>
            <p className="font-semibold text-layer-100 dark:text-white">fleet01</p>
          </li>
          <li className="flex items-start justify-start flex-col">
            <p className="text-sm text-layer-600 dark:text-white">Disk</p>
            <p className="font-semibold text-layer-100 dark:text-white">100GB</p>
          </li>
          <li className="flex items-start justify-start flex-col">
            <p className="text-sm text-layer-600 dark:text-white">CPU</p>
            <p className="font-semibold text-layer-100 dark:text-white">4</p>
          </li>
          <li className="flex items-start justify-start flex-col">
            <p className="text-sm text-layer-600 dark:text-white">GPU</p>
            <p className="font-semibold text-layer-100 dark:text-white">Nvidia v100</p>
          </li>
          <li className="flex items-start justify-start flex-col">
            <p className="text-sm text-layer-600 dark:text-white">RAM</p>
            <p className="font-semibold text-layer-100 dark:text-white">4GB</p>
          </li>
          <li className="flex items-start justify-start flex-col">
            <p className="text-sm text-layer-600 dark:text-white">ROS Distro</p>
            <p className="font-semibold text-layer-100 dark:text-white">Galactic</p>
          </li>
          <li className="flex items-start justify-start flex-col">
            <p className="text-sm text-layer-600 dark:text-white">Teleoperation</p>
            <p className="font-semibold text-layer-100 dark:text-white">Active</p>
          </li>
          <li className="flex items-start justify-start flex-col">
            <p className="text-sm text-layer-600 dark:text-white">VDI</p>
            <p className="font-semibold text-layer-100 dark:text-white">Active</p>
          </li>
          <li className="flex items-start justify-start flex-col">
            <p className="text-sm text-layer-600 dark:text-white">Cloud IDE</p>
            <p className="font-semibold text-layer-100 dark:text-white">Active</p>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default RobotHero;
