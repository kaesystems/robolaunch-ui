import React from 'react';

interface IAddField {
  text: string;
  handleClick: any;
}

const AddField = ({ text, handleClick }: IAddField) => {
  return (
    <button
      className="flex justify-center items-center w-full py-4 bg-light-100 dark:bg-layer-100 text-layer-300 dark:text-white font-bold border border-lowContrast dark:border-layer-600 transition-all uppercase rounded hover:bg-light-300 dark:hover:bg-layer-300 cursor-pointer h-[40px] text-sm mr-1 my-2"
      onClick={handleClick}
      type="button"
    >
      {text}
    </button>
  );
};

export default AddField;
