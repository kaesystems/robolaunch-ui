import React from 'react';
import Button from '../Button/Button';
import Loader from '../Loader/Loader';
import { ThemeContext } from '../../context/ThemeContext';
import { useContext } from 'react';
import UserTableOptions from '../../pages/private/UserRoleManagement/TablesOptions/OrganizationUsersTableOptions';
import DepartmentsTableOptions from '../../pages/private/UserRoleManagement/TablesOptions/DepartmentsTableOptions';
import DepartmentUsersTableOptions from '../../pages/private/UserRoleManagement/TablesOptions/DepartmentUsersTableOptions';
import { useAppSelector } from '../../hooks/redux';
import { useLocation } from 'react-router';

interface ITable {
  tableInstance: any;
  loading: boolean;
  filterInput: string;
  handleFilterChange: any;
  title: string;
  placeholderSearch: string;
  buttonTitle?: string;
  textCreateButton?: string;
  createType: string;
  createLink?: string;
  handleOpenModal: any;
  tableType?: string;
}

const Table = ({
  tableInstance,
  loading,
  filterInput,
  handleFilterChange,
  title,
  placeholderSearch,
  buttonTitle,
  handleOpenModal,
  tableType
}: ITable) => {
  const { theme } = useContext(ThemeContext);
  const { currentUser } = useAppSelector((state) => state.user);
  const location = useLocation();

  return (
    <>
      <div className="flex flex-col w-full shadow-md border light:bg-light-200 border-light-400 dark:border-layer-600 rounded">
        <div className="flex items-center justify-between p-2 pr-4 pt-4 rounded-t bg-light-200 dark:bg-layer-200">
          <div className="flex items-center">
            <img src={`icons/${theme}/${title}.svg`} alt="" />
            <p className="text-sm uppercase font-semibold p-2 dark:text-light-100">{title}</p>
          </div>
          <div className="flex items-center">
            <label className="relative text-gray-400 focus-within:text-gray-600 block mr-4">
              <img
                src={`/icons/${theme}/search.svg`}
                width="20px"
                className="pointer-events-none absolute top-1/2 transform -translate-y-1/2 left-3"
              />
              <input
                type="text"
                value={filterInput}
                onChange={handleFilterChange}
                placeholder={placeholderSearch}
                className="pl-10 bg-light-100 dark:bg-layer-100 border border-light-400 dark:border-layer-600 max-w-[200px] p-2 rounded-lg focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 text-layer-100 dark:text-light-300"
              />
            </label>
            {currentUser.admin && (
              <div>
                <Button
                  disabled={false}
                  style="primary"
                  icon="plus"
                  type="button"
                  text={buttonTitle}
                  handleClick={handleOpenModal}
                />
              </div>
            )}
            {!currentUser.admin &&
              location.pathname.split('/')[2] === 'department' &&
              currentUser.departments.map((dep, index) => {
                if (dep.name == location.pathname.split('/')[3] && dep.admin) {
                  return (
                    <div key={index}>
                      <Button
                        disabled={false}
                        style="primary"
                        icon="plus"
                        type="button"
                        text={buttonTitle}
                        handleClick={handleOpenModal}
                      />
                    </div>
                  );
                }
              })}
          </div>
        </div>
        {loading ? (
          <Loader />
        ) : (
          <>
            <table
              className="rounded-b bg-light-200 dark:bg-layer-200"
              {...tableInstance.getTableProps()}
            >
              <thead>
                {tableInstance.headerGroups.map((headerGroup) => (
                  <tr key={headerGroup.id} {...headerGroup.getHeaderGroupProps()}>
                    {headerGroup.headers.map((column) => (
                      <th
                        className="text-left p-4 text-sm dark:text-light-100"
                        key={column.id}
                        {...column.getHeaderProps(column.getSortByToggleProps())}
                      >
                        {column.render('Header')}
                        <span>
                          {column.isSorted ? (column.isSortedDesc ? '  ⇩  ' : '  ⇧  ') : ''}
                        </span>
                      </th>
                    ))}
                  </tr>
                ))}
              </thead>
              <tbody {...tableInstance.getTableBodyProps()} className="font-normal">
                {tableInstance.page?.map((row) => {
                  {
                    tableInstance.prepareRow(row);
                  }
                  return (
                    <>
                      {currentUser.admin && (
                        <tr
                          key={row.id}
                          {...row.getRowProps()}
                          className="odd:bg-light-300 odd:dark:bg-layer-300 dark:border-t dark:border-layer-600"
                        >
                          {row.cells.map((cell, index) => (
                            <td
                              className="select-none p-4 text-sm dark:text-light-100"
                              key={index}
                              {...cell.getCellProps()}
                            >
                              {cell.render('Cell')}
                            </td>
                          ))}
                          {tableType == 'users' && (
                            <UserTableOptions rowValue={row.values.username} />
                          )}
                          {tableType == 'departments' && (
                            <DepartmentsTableOptions rowValue={row.values.name} />
                          )}
                          {tableType == 'departmentUsers' && (
                            <DepartmentUsersTableOptions rowValue={row.values} />
                          )}
                        </tr>
                      )}
                      {!currentUser.admin &&
                        location.pathname.split('/')[2] === 'department' &&
                        currentUser.departments.map((dep, index) => {
                          if (dep.name == location.pathname.split('/')[3] && dep.admin) {
                            return (
                              <tr
                                key={row.id}
                                {...row.getRowProps()}
                                className="odd:bg-light-300 odd:dark:bg-layer-300 dark:border-t dark:border-layer-600"
                              >
                                {row.cells.map((cell, index) => (
                                  <td
                                    className="select-none p-4 text-sm dark:text-light-100"
                                    key={index}
                                    {...cell.getCellProps()}
                                  >
                                    {cell.render('Cell')}
                                  </td>
                                ))}
                                {tableType == 'users' && (
                                  <UserTableOptions rowValue={row.values.username} />
                                )}
                                {tableType == 'departments' && (
                                  <DepartmentsTableOptions rowValue={row.values.name} />
                                )}
                                {tableType == 'departmentUsers' && (
                                  <DepartmentUsersTableOptions rowValue={row.values} />
                                )}
                              </tr>
                            );
                          }
                        })}
                      {!currentUser.admin && location.pathname.split('/')[2] == 'departments' && (
                        <>
                          {currentUser.departments.map((dep) => {
                            if (dep.name == row.original.name && dep.admin) {
                              return (
                                <tr
                                  key={row.id}
                                  {...row.getRowProps()}
                                  className="odd:bg-light-300 odd:dark:bg-layer-300 dark:border-t dark:border-layer-600"
                                >
                                  {row.cells.map((cell, index) => (
                                    <td
                                      className="select-none p-4 text-sm dark:text-light-100"
                                      key={index}
                                      {...cell.getCellProps()}
                                    >
                                      {cell.render('Cell')}
                                    </td>
                                  ))}
                                  {tableType == 'users' && (
                                    <UserTableOptions rowValue={row.values.username} />
                                  )}
                                  {tableType == 'departments' && (
                                    <DepartmentsTableOptions rowValue={row.values.name} />
                                  )}
                                  {tableType == 'departmentUsers' && (
                                    <DepartmentUsersTableOptions rowValue={row.values} />
                                  )}
                                </tr>
                              );
                            }
                          })}
                        </>
                      )}
                      {!currentUser.admin && location.pathname.split('/')[2] == 'users' && (
                        <>
                          <tr
                            key={row.id}
                            {...row.getRowProps()}
                            className="odd:bg-light-300 odd:dark:bg-layer-300 dark:border-t dark:border-layer-600"
                          >
                            {row.cells.map((cell, index) => (
                              <td
                                className="select-none p-4 text-sm dark:text-light-100"
                                key={index}
                                {...cell.getCellProps()}
                              >
                                {cell.render('Cell')}
                              </td>
                            ))}
                            {currentUser.admin && (
                              <>
                                {tableType == 'users' && (
                                  <UserTableOptions rowValue={row.values.username} />
                                )}
                              </>
                            )}
                            {tableType == 'departments' && (
                              <DepartmentsTableOptions rowValue={row.values.name} />
                            )}
                            {tableType == 'departmentUsers' && (
                              <DepartmentUsersTableOptions rowValue={row.values} />
                            )}
                          </tr>
                        </>
                      )}
                    </>
                  );
                })}
              </tbody>
            </table>
            <div className="flex justify-center gap-20 p-3">
              {tableInstance.pageCount != 1 || 0 ? (
                <>
                  {tableInstance.canPreviousPage && (
                    <Button
                      disabled={false}
                      style="primary"
                      type={'submit'}
                      text="<"
                      handleClick={() => {
                        tableInstance.previousPage();
                      }}
                    ></Button>
                  )}
                  {tableInstance.canNextPage && (
                    <Button
                      disabled={false}
                      style="primary"
                      type={'submit'}
                      text=">"
                      handleClick={() => {
                        tableInstance.nextPage();
                      }}
                    ></Button>
                  )}
                </>
              ) : (
                ''
              )}
              {tableInstance.data?.length == 0 && (
                <div className="p-2">
                  <p className="">Data Not Found</p>
                </div>
              )}
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default Table;
