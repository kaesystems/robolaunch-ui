import React, { useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { IDepartment } from '../../types/types';
import { ThemeContext } from '../../context/ThemeContext';
import slugify from 'react-slugify';
import { useLocation } from 'react-router';
import { useAppSelector, useAppDispatch } from '../../hooks/redux';
import { setIsOpenMenu } from '../../store/features/menu/menuSlice';

function MainMenu() {
  const [isShowDepartment, setIsShowDepartment] = useState('');
  const [isShowRoboticsCloud, setIsShowRoboticsCloud] = useState('');
  const [isShowFleet, setIsShowFleet] = useState('');
  const { isOpenMenu } = useAppSelector((state) => state.menu);

  const dispatch = useAppDispatch();
  const { theme } = useContext(ThemeContext);

  const url = useLocation();

  console.log(slugify('Aasd'));

  const mockCurrentOrg = 'Organization1';
  const departments: IDepartment[] = [
    {
      name: 'Department1',
      instances: [
        {
          name: 'Instance1',
          fleets: [
            {
              name: 'Fleet1',
              cloudOnly: true,

              robots: [
                {
                  name: 'Robot1'
                }
              ]
            },
            {
              name: 'Fleet2',
              cloudOnly: true,

              robots: [
                {
                  name: 'Robot1'
                }
              ]
            },
            {
              name: 'Fleet3',
              cloudOnly: false,

              robots: [
                {
                  name: 'Robot1'
                }
              ]
            }
          ]
        },
        {
          name: 'Instance3',
          fleets: [
            {
              name: 'Fleet5',
              cloudOnly: true,

              robots: [
                {
                  name: 'Robot8'
                }
              ]
            },
            {
              name: 'Fleet7',
              cloudOnly: true,

              robots: [
                {
                  name: 'Robot9'
                }
              ]
            },
            {
              name: 'Fleet8',
              cloudOnly: false,

              robots: [
                {
                  name: 'Robot6'
                }
              ]
            }
          ]
        }
      ]
    },
    {
      name: 'Department2',
      instances: [
        {
          name: 'Instance2',
          fleets: [
            {
              name: 'Fleet2',
              cloudOnly: true,

              robots: [
                {
                  name: 'Robot2'
                }
              ]
            }
          ]
        }
      ]
    }
  ];

  return (
    <nav
      className={`fixed top-0 left-0 h-screen dark:bg-layer-200 animate__animated animate__fadeIn transition-all ${
        isOpenMenu ? 'w-80' : 'w-16'
      }`}
    >
      <div className="h-28 flex justify-center items-center">
        <Link to={`/${slugify(mockCurrentOrg)}`}>
          <img
            className={`${isOpenMenu ? 'w-52 h-auto' : 'w-8 h-auto'}`}
            src={`${
              isOpenMenu ? `/icons/${theme}/robolaunch.svg` : `/icons/${theme}/robolaunch-mini.svg`
            }`}
          />
        </Link>
      </div>
      <div className="h-[1px] w-full bg-layer-500"></div>
      <div className="flex -mt-3 pb-2 justify-end">
        <img
          onClick={() => {
            dispatch(setIsOpenMenu());
          }}
          className={`"w-6  dark:bg-layer-500 rounded-b cursor-pointer ${
            isOpenMenu ? 'rotate-90' : '-rotate-90'
          }`}
          src={`/icons/${theme}/arrow.svg`}
        />
      </div>
      <div className="flex flex-col h-screen mx-2 dark:text-white text-sm">
        <Link to={`/dashboard`}>
          <div
            className={`flex items-center ${
              url.pathname.split('/')[1] == 'dashboard' && 'bg-layer-400 rounded-lg'
            }`}
          >
            <img src={`/icons/${theme}/dashboard.svg`} />
            {isOpenMenu && <span>General Dashboard</span>}
          </div>
        </Link>
        <div className="flex items-center">
          <img src={`/icons/${theme}/organizations.svg`} />
          {isOpenMenu && (
            <select className="input-select" name="" id="">
              <option value="Org #1">Org #1</option>
              <option value="Org #2">Org #2</option>
            </select>
          )}
        </div>
        <Link to={`/${slugify(mockCurrentOrg)}`}>
          <div
            className={`flex items-center ${
              url.pathname.split('/')[1] == slugify(mockCurrentOrg) &&
              url.pathname.split('/')[2] == undefined &&
              'bg-layer-400 rounded-lg'
            }`}
          >
            <img src={`/icons/${theme}/organization.svg`} />
            {isOpenMenu && <span>Organization Dashboard</span>}
          </div>
        </Link>

        {/* DEPARTMENT MAP */}
        <div className="py-2">
          <ul>
            {departments.map((department) => {
              {
                console.log(url.pathname.split('/')[2]);
              }

              return (
                <li key={department.name}>
                  <div
                    className={`flex items-center dark:text-white ${
                      url.pathname.split('/')[1] === slugify(mockCurrentOrg) &&
                      url.pathname.split('/')[2] === slugify(department.name) &&
                      url.pathname.split('/')[3] === undefined &&
                      'bg-layer-400 rounded-lg'
                    }`}
                  >
                    {isOpenMenu && (
                      <img
                        onClick={() => {
                          if (isShowDepartment === department.name) {
                            setIsShowDepartment('');
                          } else {
                            setIsShowDepartment(department.name);
                          }
                          setIsShowRoboticsCloud('');
                          setIsShowFleet('');
                        }}
                        src={`/icons/${theme}/arrow.svg`}
                        className={`${
                          isShowDepartment === `${department.name}` &&
                          'rotate-180 transition-all duration-500'
                        }`}
                      />
                    )}
                    <Link
                      to={`/${slugify(mockCurrentOrg)}/${slugify(department.name)}`}
                      className="flex items-center w-full dark:text-white"
                    >
                      <img src={`/icons/${theme}/team.svg`} />
                      {isOpenMenu && <span>{department.name}</span>}
                    </Link>
                  </div>

                  {/* INSTANCE MAP */}
                  <ul>
                    {isShowDepartment === department.name &&
                      department.instances.map((instance, index) => {
                        return (
                          <li key={instance.name}>
                            <div
                              className={`flex items-center animate__animated animate__faster animate__fadeInDown ${
                                isOpenMenu && 'ml-2 '
                              } ${
                                url.pathname.split('/')[1] === slugify(mockCurrentOrg) &&
                                url.pathname.split('/')[2] === slugify(department.name) &&
                                url.pathname.split('/')[3] === slugify(instance.name) &&
                                url.pathname.split('/')[4] === undefined &&
                                'bg-layer-400 rounded-lg'
                              }`}
                            >
                              {isOpenMenu && (
                                <img
                                  onClick={() => {
                                    if (isShowRoboticsCloud === `Robotics Cloud #${index + 1}`) {
                                      setIsShowRoboticsCloud('');
                                    } else {
                                      setIsShowRoboticsCloud(`Robotics Cloud #${index + 1}`);
                                    }
                                    setIsShowFleet('');
                                  }}
                                  src={`/icons/${theme}/arrow.svg`}
                                  className={`${
                                    isShowRoboticsCloud === `Robotics Cloud #${index + 1}` &&
                                    'rotate-180 transition-all duration-500'
                                  }`}
                                />
                              )}
                              <Link
                                to={`/${slugify(mockCurrentOrg)}/${slugify(
                                  department.name
                                )}/${slugify(instance.name)}`}
                                className="flex items-center w-full"
                              >
                                <img src={`/icons/${theme}/server.svg`} />
                                {isOpenMenu && <span>Robotics Cloud #{index + 1}</span>}
                              </Link>
                            </div>

                            {/* CONNECTED PHYSICAL INSTANCE MAP */}
                            <ul>
                              {isShowRoboticsCloud === `Robotics Cloud #${index + 1}` && (
                                <li>
                                  <div
                                    className={`flex items-center dark:text-white animate__animated animate__faster animate__fadeInDown ${
                                      isOpenMenu && 'ml-12'
                                    } ${
                                      url.pathname.split('/')[1] === slugify(mockCurrentOrg) &&
                                      url.pathname.split('/')[2] === slugify(department.name) &&
                                      url.pathname.split('/')[3] === slugify(instance.name) &&
                                      url.pathname.split('/')[4] ===
                                        slugify('connected-physical-instances') &&
                                      'bg-layer-400 rounded-lg'
                                    }`}
                                  >
                                    <Link
                                      to={`/${slugify(mockCurrentOrg)}/${slugify(
                                        department.name
                                      )}/${slugify(instance.name)}/connected-physical-instances`}
                                      className="flex items-center w-full"
                                    >
                                      <img src={`/icons/${theme}/cpu.svg`} />
                                      {isOpenMenu && <span>Connected Physical Instances</span>}
                                    </Link>
                                  </div>
                                </li>
                              )}
                            </ul>

                            {/* FLEETS MAP */}
                            <ul>
                              {isShowRoboticsCloud === `Robotics Cloud #${index + 1}` &&
                                instance.fleets.map((fleet) => {
                                  return (
                                    <li key={fleet.name}>
                                      <div
                                        className={`flex items-center dark:text-white animate__animated animate__faster animate__fadeInDown ${
                                          isOpenMenu && 'ml-6 '
                                        } ${
                                          url.pathname.split('/')[1] === slugify(mockCurrentOrg) &&
                                          url.pathname.split('/')[2] === slugify(department.name) &&
                                          url.pathname.split('/')[3] === slugify(instance.name) &&
                                          url.pathname.split('/')[4] === slugify(fleet.name) &&
                                          url.pathname.split('/')[5] === undefined &&
                                          'bg-layer-400 rounded-lg'
                                        }`}
                                      >
                                        {isOpenMenu && (
                                          <img
                                            onClick={() => {
                                              if (isShowFleet === fleet.name) {
                                                setIsShowFleet('');
                                              } else {
                                                setIsShowFleet(fleet.name);
                                              }
                                            }}
                                            src={`/icons/${theme}/arrow.svg`}
                                            className={`${
                                              isShowFleet === `${fleet.name}` &&
                                              'rotate-180 transition-all duration-500'
                                            }`}
                                          />
                                        )}
                                        <Link
                                          to={`/${slugify(mockCurrentOrg)}/${slugify(
                                            department.name
                                          )}/${slugify(instance.name)}/${slugify(fleet.name)}`}
                                          className="flex items-center w-full"
                                        >
                                          <img src={`/icons/${theme}/fleet.svg`} />
                                          {isOpenMenu && <span>{fleet.name}</span>}
                                        </Link>
                                      </div>
                                      {/* STATIC ROBOTS & TASK MANAGEMENT */}
                                      {isShowFleet === fleet.name && (
                                        <ul>
                                          {/* <li>
                                            <div
                                              className={`flex items-center dark:text-white animate__animated animate__faster animate__fadeInDown ${
                                                isOpenMenu && 'ml-14'
                                              } ${
                                                url.pathname.split('/')[1] ===
                                                  slugify(mockCurrentOrg) &&
                                                url.pathname.split('/')[2] ===
                                                  slugify(department.name) &&
                                                url.pathname.split('/')[3] ===
                                                  slugify(instance.name) &&
                                                url.pathname.split('/')[4] ===
                                                  slugify(fleet.name) &&
                                                url.pathname.split('/')[5] === slugify('robots') &&
                                                'bg-layer-400 rounded-lg'
                                              }`}
                                            >
                                              <Link
                                                to={`/${slugify(mockCurrentOrg)}/${slugify(
                                                  department.name
                                                )}/${slugify(instance.name)}/${slugify(
                                                  fleet.name
                                                )}/robots`}
                                                className="flex items-center w-full"
                                              >
                                                <img src={`/icons/${theme}/robots.svg`} />
                                                {isOpenMenu && <span>Robots</span>}
                                              </Link>
                                            </div>
                                          </li> */}
                                          <li>
                                            <div
                                              className={`flex items-center dark:text-white animate__animated animate__faster animate__fadeInDown ${
                                                isOpenMenu && 'ml-14'
                                              } ${
                                                url.pathname.split('/')[1] ===
                                                  slugify(mockCurrentOrg) &&
                                                url.pathname.split('/')[2] ===
                                                  slugify(department.name) &&
                                                url.pathname.split('/')[3] ===
                                                  slugify(instance.name) &&
                                                url.pathname.split('/')[4] ===
                                                  slugify(fleet.name) &&
                                                url.pathname.split('/')[5] ===
                                                  slugify('task-management') &&
                                                'bg-layer-400 rounded-lg'
                                              }`}
                                            >
                                              <Link
                                                to={`/${slugify(mockCurrentOrg)}/${slugify(
                                                  department.name
                                                )}/${slugify(instance.name)}/${slugify(
                                                  fleet.name
                                                )}/task-management`}
                                                className="flex items-center w-full"
                                              >
                                                <img src={`/icons/${theme}/task.svg`} />
                                                {isOpenMenu && <span>Task Management</span>}
                                              </Link>
                                            </div>
                                          </li>
                                        </ul>
                                      )}
                                    </li>
                                  );
                                })}
                            </ul>
                          </li>
                        );
                      })}
                  </ul>
                </li>
              );
            })}
          </ul>
        </div>
        <div className="h-[1px] w-full bg-layer-500" />
        <ul>
          <li>
            <Link
              className="flex items-center"
              to={`/${slugify(mockCurrentOrg)}/user-role-management/users`}
            >
              <img src={`/icons/${theme}/users.svg`} />
              {isOpenMenu && <span>User Role Management</span>}
            </Link>
          </li>
          <li>
            <Link className="flex items-center" to={`/${slugify(mockCurrentOrg)}/marketplace`}>
              <img src={`/icons/${theme}/marketplace.svg`} />
              {isOpenMenu && <span>MarketPlace</span>}
            </Link>
          </li>
          <li>
            <Link className="flex items-center" to={`/${slugify(mockCurrentOrg)}/accounting`}>
              <img src={`/icons/${theme}/accounting.svg`} />
              {isOpenMenu && <span>Accounting</span>}
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default MainMenu;
