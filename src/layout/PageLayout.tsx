import React from 'react';
import { useAppSelector } from '../hooks/redux';
import Header from './Header';
import 'animate.css';
import MainMenu from '../components/MainMenu/MainMenu';

const PageLayout = ({ children }) => {
  const { isOpenMenu, isCreateMenu } = useAppSelector((state) => state.menu);

  return (
    <>
      <div className="flex">
        <MainMenu />
        <div
          className={`w-full bg-light-100 dark:bg-layer-100 transition-all duration-[40ms] ${
            isOpenMenu && 'lg:blur-2xl'
          } blur-none`}
          style={isOpenMenu ? { marginLeft: '20rem' } : { marginLeft: '4rem' }}
        >
          <Header />
          <div
            className={`w-full p-8 lg:p-6 animate__animated animate__fadeInUpBig ${
              isCreateMenu && 'blur-2xl'
            } blur-none`}
          >
            {children}
          </div>
        </div>
      </div>
    </>
  );
};

export default PageLayout;
