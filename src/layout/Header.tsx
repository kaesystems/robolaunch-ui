import React, { useEffect, useState, useRef, useContext } from 'react';
import { useAppDispatch, useAppSelector } from '../hooks/redux';
import { ThemeContext } from '../context/ThemeContext';
import { Link } from 'react-router-dom';
import Modal from '../components/Modal/Modal';
import ReactModal from 'react-modal';
import { modalStyles } from '../utils/css/modalStyles';
import Button from '../components/Button/Button';
import { userLogout } from '../store/features/user/userSlice';
import { changeCurrentOrganization } from '../store/features/organization/organizationSlice';
import CreateOrganization from '../pages/private/UserRoleManagement/Modals/CreateOrganizationModal';
import CreateBanner from '../components/CreateBanner/CreateBanner';
import { setIsCreateMenu, setIsOpenMenu } from '../store/features/menu/menuSlice';
import { useLocation } from 'react-router';

const Header = () => {
  const { theme, setTheme } = useContext(ThemeContext);
  const [profileSelectOpen, setProfileSelectOpen] = useState(false);
  const [isChangeOrgOpen, setIsChangeOrgOpen] = useState(false);
  const [isOrganizationCreateModalOpen, setIsOrganizationCreateModalOpen] = useState(false);
  const [selectedOrg, setSelectedOrg] = useState('');
  const ref = useRef<HTMLDivElement>();

  const dispatch = useAppDispatch();
  const { organizations, currentOrganization } = useAppSelector((state) => state.organization);
  const { isCreateMenu } = useAppSelector((state) => state.menu);
  const url = useLocation();

  useEffect(() => {
    const checkIfClickedOutside = (e) => {
      if (profileSelectOpen && ref.current && !ref.current.contains(e.target)) {
        setProfileSelectOpen(false);
      }
    };
    document.addEventListener('mousedown', checkIfClickedOutside);
    return () => {
      document.removeEventListener('mousedown', checkIfClickedOutside);
    };
  }, [profileSelectOpen]);

  const ProfileDropdown = () => {
    setProfileSelectOpen(true);
  };
  const handleLogout = () => {
    dispatch(userLogout());
  };
  const handleCloseChangeOrg = () => {
    setIsChangeOrgOpen(false);
  };
  const handleOpenChangeOrg = () => {
    setIsChangeOrgOpen(true);
    setProfileSelectOpen(false);
  };

  const handleOpenOrganizationCreateModal = () => {
    setIsOrganizationCreateModalOpen(true);
  };
  const handleCloseOrganizationCreateModal = () => {
    setIsOrganizationCreateModalOpen(false);
  };

  return (
    <header className="flex h-20 w-full items-center bg-light-100 dark:bg-layer-200 text-sm shadow-sm p-2 px-4 animate__animated animate__fadeInDownBig">
      <div className="flex w-full justify-between items-center">
        <div className="flex gap-2 capitalize font-semibold test-center text-white p-2 bg-layer-400 rounded-lg">
          {url.pathname.split('/')[1] !== undefined && (
            <span className="text-white ">
              <Link to={`/${url.pathname.split(`/`)[1]}`}>{url.pathname.split('/')[1]}</Link>
            </span>
          )}
          {url.pathname.split('/')[2] !== undefined && <span>/</span>}
          {url.pathname.split('/')[2] !== undefined && (
            <span className="text-white ">
              <Link to={`/${url.pathname.split(`/`)[1]}/${url.pathname.split(`/`)[2]}`}>
                {url.pathname.split('/')[2]}
              </Link>
            </span>
          )}
          {url.pathname.split('/')[3] !== undefined && <span>/</span>}
          {url.pathname.split('/')[3] !== undefined && (
            <span className="text-white ">
              <Link
                to={`/${url.pathname.split(`/`)[1]}/${url.pathname.split(`/`)[2]}/${
                  url.pathname.split(`/`)[3]
                }`}
              >
                {url.pathname.split('/')[3]}
              </Link>
            </span>
          )}
          {url.pathname.split('/')[4] !== undefined && <span>/</span>}
          {url.pathname.split('/')[4] !== undefined && (
            <span className="text-white ">
              <Link
                to={`/${url.pathname.split(`/`)[1]}/${url.pathname.split(`/`)[2]}/${
                  url.pathname.split(`/`)[3]
                }/${url.pathname.split(`/`)[4]}`}
              >
                {url.pathname.split('/')[4]}
              </Link>
            </span>
          )}
          {url.pathname.split('/')[5] !== undefined && <span>/</span>}
          {url.pathname.split('/')[5] !== undefined && (
            <span className="text-white ">
              <Link
                to={`/${url.pathname.split(`/`)[1]}/${url.pathname.split(`/`)[2]}/${
                  url.pathname.split(`/`)[3]
                }/${url.pathname.split(`/`)[4]}/${url.pathname.split(`/`)[5]}`}
              >
                {url.pathname.split('/')[5]}
              </Link>
            </span>
          )}
        </div>
        <ul className="flex gap-2 lg:gap-2">
          <li
            onClick={() => {
              dispatch(setIsCreateMenu());
            }}
            className={`dark:bg-primary-100 flex justify-center items-center rounded-lg px-2 border border-light-300 dark:border-layer-600 cursor-pointer transition-all hover:bg-light-300 dark:hover:bg-primary-200 ${
              isCreateMenu && 'bg-light-300 dark:bg-primary-300'
            }`}
          >
            <div className="flex items-center pr-4 pl-2 gap-1 ">
              <img className="select-none" src={`/icons/${theme}/plus.svg`} alt="questions" />
              <p className="text-dark dark:text-white font-semibold tracking-wide uppercase select-none">
                Create
              </p>
            </div>
          </li>
          <li
            onClick={() => setTheme(theme === 'dark' ? 'light' : 'dark')}
            className="cursor-pointer flex w-12 justify-center items-center bg-light-200 dark:bg-layer-300 rounded-lg border border-light-300 dark:border-layer-600 transition-all hover:bg-light-300 dark:hover:bg-layer-400"
          >
            <div>
              {theme === 'dark' ? (
                <img src="/icons/dark/sun.svg" />
              ) : (
                <img src="/icons/light/moon.svg" />
              )}
            </div>
          </li>
          <li className="bg-light-200 dark:bg-layer-300 flex rounded-lg border border-light-300 dark:border-layer-600 cursor-pointer transition-all hover:bg-light-300 dark:hover:bg-layer-400">
            <img src={`/icons/${theme}/help.svg`} alt="questions" />
          </li>
          <li className="bg-light-200 dark:bg-layer-300 flex rounded-lg border border-light-300 dark:border-layer-600 cursor-pointer transition-all hover:bg-light-300 dark:hover:bg-layer-400">
            <img src={`/icons/${theme}/bell.svg`} alt="notifications" />
          </li>
          <li
            onClick={ProfileDropdown}
            className="bg-light-200 dark:bg-layer-300 flex rounded-lg border border-light-300 dark:border-layer-600 cursor-pointer transition-all hover:bg-light-300 dark:hover:bg-layer-400"
          >
            <img src={`/icons/${theme}/user.svg`} alt="user" />
          </li>
        </ul>
        <ReactModal
          isOpen={isChangeOrgOpen}
          onRequestClose={handleCloseChangeOrg}
          shouldCloseOnOverlayClick={true}
          style={modalStyles}
          contentLabel="Fleet Create Modal"
        >
          <Modal title="Change Active Organization" handleClose={handleCloseChangeOrg}>
            <div>
              <div className="flex gap-3 items-center">
                <select
                  className="border text-base border-light-400 dark:border-layer-600 bg-light-200 dark:bg-layer-300 hover:bg-light-300 dark:hover:bg-layer-400 cursor-pointer transition-all rounded text-layer-100 dark:text-white w-[14rem] h-[3rem]"
                  name=""
                  id=""
                  onChange={(e) => {
                    setSelectedOrg(e.target.value);
                  }}
                  defaultValue={currentOrganization.name}
                >
                  {organizations.map((org) => {
                    return (
                      <option key={org.name} value={org.name}>
                        {org.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div
                className="flex justify-center pt-8"
                onClick={() => {
                  dispatch(
                    changeCurrentOrganization({
                      name: selectedOrg
                    })
                  );
                  setIsChangeOrgOpen(false);
                  window.location.href = '/';
                }}
              >
                <Button
                  disabled={false}
                  type="button"
                  style="primary"
                  text="Change Active Organization"
                  icon="change"
                ></Button>
              </div>
            </div>
          </Modal>
        </ReactModal>
        <ReactModal
          isOpen={isOrganizationCreateModalOpen}
          onRequestClose={handleCloseOrganizationCreateModal}
          shouldCloseOnOverlayClick={false}
          style={modalStyles}
          contentLabel="Create Organization"
        >
          <CreateOrganization handleClose={handleCloseOrganizationCreateModal} />
        </ReactModal>
        {profileSelectOpen && (
          <div
            ref={ref}
            className="absolute drop-shadow-2xl z-50 bg-light-200 dark:bg-layer-200 border border-light-300 dark:border-layer-600 rounded-lg top-[4.25rem] right-4 w-[244px] animate__animated animate__fadeIn animate__faster"
          >
            <ul>
              <Link onClick={() => setProfileSelectOpen(!profileSelectOpen)} to="/profile">
                <li className="flex items-center gap-2 border border-b-2 border-white dark:border-layer-300 hover:bg-light-300 dark:hover:bg-layer-400 cursor-pointer">
                  <img src={`/icons/${theme}/user.svg`} alt="Profile" />
                  <span className="text-base dark:text-light-100">Profile</span>
                </li>
              </Link>
              <li
                onClick={() => {
                  handleOpenOrganizationCreateModal();
                }}
                className="flex items-center gap-2 border border-b-2 border-white dark:border-layer-300 hover:bg-light-300 dark:hover:bg-layer-400 cursor-pointer"
              >
                <img className="p-3" src={`/icons/${theme}/plus.svg`} alt="Profile" />
                <span className="text-base dark:text-light-100">Create Organization</span>
              </li>
              {organizations.length !== 0 && (
                <li
                  onClick={handleOpenChangeOrg}
                  className="flex items-center gap-2 border border-b-2 border-white dark:border-layer-300 hover:bg-light-300 dark:hover:bg-layer-400 cursor-pointer"
                >
                  <img src={`/icons/${theme}/change.svg`} alt="Change" />
                  <span className="text-base dark:text-light-100">Change Organization</span>
                </li>
              )}
              <li
                onClick={handleLogout}
                className="flex items-center gap-2 border border-b-2 border-white dark:border-layer-300 hover:bg-light-300 dark:hover:bg-layer-400 cursor-pointer"
              >
                <img src={`/icons/${theme}/logout.svg`} alt="Logout" />
                <span className="text-base dark:text-light-100">Logout</span>
              </li>
            </ul>
          </div>
        )}
      </div>
      {isCreateMenu && <CreateBanner />}
    </header>
  );
};
export default Header;
