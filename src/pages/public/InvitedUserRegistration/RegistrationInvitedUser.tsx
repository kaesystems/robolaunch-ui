import { Form, Field, Formik } from 'formik';
import React from 'react';
import { useSearchParams } from 'react-router-dom';
import InputField from '../../../components/Form/InputField/InputField';
import { useAppDispatch } from '../../../hooks/redux';
import { invitedUserRegistration } from '../../../store/features/organization/organizationSlice';
import * as Yup from 'yup';

const RegistrationInvitedUser = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const dispatch = useAppDispatch();

  const InviteSchema = Yup.object().shape({
    user: Yup.object().shape({
      username: Yup.string()
        .matches(/^\w[a-zA-Z@#0-9.]*$/, 'Only English Characters and Numbers')
        .min(3, 'Min 3 Character')
        .max(32, 'Max 32 Character')
        .required('Username is required'),
      firstName: Yup.string()
        .min(2, 'Min 2 Character')
        .max(32, 'Max 32 Character')
        .required('First Name is required'),
      lastName: Yup.string()
        .min(2, 'Min 2 Character')
        .max(32, 'Max 32 Character')
        .required('Last Name is required')
    })
  });

  return (
    <div className="h-screen flex justify-center items-center robolaunch-bg">
      <div className="px-8 pt-8 bg-layer-400 text-white text-center rounded-lg">
        <img className="mx-auto w-48 h-auto" src="../../../logo/plain.svg" alt="Robolaunch" />
        <p className="py-4 text-base font-medium">Welcome to Robolaunch!</p>
        <p className="text-base font-light">Invited User</p>
        <div>
          <Formik
            validationSchema={InviteSchema}
            initialValues={{
              organization: {
                name: searchParams.get('organization')
              },
              user: {
                username: '',
                firstName: '',
                lastName: '',
                email: searchParams.get('email')
              },
              token: searchParams.get('token')
            }}
            onSubmit={(values, { resetForm }) => {
              console.log('Values from form+ ', values);
              dispatch(invitedUserRegistration(values));
              resetForm();
            }}
          >
            <Form className="grid justify-items-center items-center gap-1">
              <InputField id="Org" info="Organization" label="Organization">
                <Field
                  className="h-10 rounded p-2 text-light-400 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic cursor-not-allowed"
                  value={searchParams.get('organization')}
                  disabled
                />
              </InputField>
              <InputField id="Org" info="Email" label="Email">
                <Field
                  className="h-10 rounded p-2 text-light-400 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic cursor-not-allowed"
                  value={searchParams.get('email')}
                  disabled
                />
              </InputField>

              <InputField id="user.username" info="Type Username" label="Username">
                <Field
                  className="h-10 rounded p-2 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                  id="user.username"
                  name="user.username"
                  placeholder="e.g. Doe9213"
                />
              </InputField>

              <InputField id="user.firstName" info="Type first name" label="First Name">
                <Field
                  className="h-10 rounded p-2 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                  id="user.firstName"
                  name="user.firstName"
                  placeholder="e.g. John"
                />
              </InputField>

              <InputField id="user.lastName" info="Type last Name" label="Last Name">
                <Field
                  className="h-10 rounded p-2 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                  id="user.lastName"
                  name="user.lastName"
                  placeholder="e.g. Doe"
                />
              </InputField>
              <button
                className="w-full h-10 font-semibold my-6 bg-primary-200 rounded hover:bg-primary-400"
                type="submit"
              >
                Register
              </button>
            </Form>
          </Formik>
        </div>
      </div>
    </div>
  );
};
export default RegistrationInvitedUser;
