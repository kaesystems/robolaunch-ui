import React from 'react';
import { Formik, Field, Form } from 'formik';
import InputField from '../../../components/Form/InputField/InputField';
import { Link } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../../hooks/redux';
import { forgotPassword } from '../../../store/features/user/userSlice';
import * as Yup from 'yup';
import 'animate.css';

function ForgotPassword() {
  const { isFetching } = useAppSelector((state) => state.user);
  const dispatch = useAppDispatch();

  const ForgotPasswordSchema = Yup.object().shape({
    username: Yup.string()
      .required('Required')
      .matches(/^\w[a-zA-Z@#0-9.]*$/, 'Only English Characters and Numbers')
      .min(3, 'Min 3 Character')
      .max(32, 'Max 32 Character')
  });

  return (
    <div className="h-screen flex justify-center items-center robolaunch-bg animate__animated animate__fadeIn">
      <div className="px-8 pt-8 bg-layer-400 text-white text-center rounded-lg">
        <img className="mx-auto w-48 h-auto" src="../../../logo/plain.svg" alt="Robolaunch" />
        <p className="py-4 text-base font-medium">Welcome to Robolaunch!</p>
        <p className="text-base font-light">Forgot Password</p>
        <div className="py-4">
          <Formik
            initialValues={{
              username: ''
            }}
            onSubmit={(values, { resetForm }) => {
              dispatch(forgotPassword(values));
              resetForm();
            }}
            validationSchema={ForgotPasswordSchema}
          >
            <Form className="grid justify-items-center items-center">
              <fieldset disabled={isFetching}>
                <InputField id="username" info="Type Username" label="Username">
                  <Field
                    className="h-10 rounded p-2 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                    id="username"
                    name="username"
                    placeholder="e.g. Doe9213"
                  />
                </InputField>
              </fieldset>
              <button
                className="w-full h-10 mt-6 font-semibold bg-primary-200 rounded hover:bg-primary-400"
                type="submit"
              >
                Reset Password
              </button>
            </Form>
          </Formik>
        </div>
        <div className="pt-2 pb-4">
          <Link className="text-lowContrast text-sm" to="/">
            Back to Login
          </Link>
        </div>
      </div>
    </div>
  );
}
export default ForgotPassword;
