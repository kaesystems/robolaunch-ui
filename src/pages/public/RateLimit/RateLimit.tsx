import React, { useEffect } from 'react';
import 'animate.css';
import { useSearchParams } from 'react-router-dom';

const RateLimit = () => {
  const [searchParams, setSearchParams] = useSearchParams();

  useEffect(() => {
    if (!searchParams.get('rateLimit')) {
      window.location.href = '/login';
    }
  }, []);

  return (
    <div className="h-screen flex justify-center items-center robolaunch-bg animate__animated animate__fadeIn">
      <div className="sm:px-1 px-8 pt-8 sm:mx-4 lg:mx-8 xl:mx-12 bg-layer-400 text-white text-center rounded-lg">
        <img className="mx-auto w-48 h-auto" src="../../../logo/plain.svg" alt="Robolaunch" />
        <p className="text-base font-light p-8 mx-auto">
          We are currently unable to respond to your request as we have received too many requests
          from this IP address. Please try again in 1 hour.
        </p>
        <div></div>
      </div>
    </div>
  );
};
export default RateLimit;
