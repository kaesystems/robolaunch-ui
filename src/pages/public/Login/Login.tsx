import React, { useEffect, useState } from 'react';
import { Formik, Field, Form } from 'formik';
import InputField from '../../../components/Form/InputField/InputField';
import { Link, useSearchParams } from 'react-router-dom';
import EnvVariables from '../../../constants/EnvVariables';
import { clearStateLoginUser, loginUser } from '../../../store/features/user/userSlice';
import { useAppDispatch, useAppSelector } from '../../../hooks/redux';
import Loader from '../../../components/Loader/Loader';
import * as Yup from 'yup';
import 'animate.css';
import { toast } from 'react-toastify';

const Login = () => {
  const [showPassword, setShowPassword] = useState('password');
  const dispatch = useAppDispatch();
  const { isFetchingLoginUser } = useAppSelector((state) => state.user);
  const [searchParams, setSearchParams] = useSearchParams();
  const notificationProperties: object = { position: toast.POSITION.BOTTOM_RIGHT, theme: 'dark' };

  useEffect(() => {
    dispatch(clearStateLoginUser());
    localStorage.clear();

    if (searchParams.get('changedPassword'))
      toast.success('Initial password setting successful!', notificationProperties);
  }, []);

  const LoginSchema = Yup.object().shape({
    username: Yup.string()
      .required('Required')
      .matches(/^\w[a-zA-Z@#0-9.]*$/, 'Only English Characters and Numbers')
      .min(3, 'Min 3 Character')
      .max(32, 'Max 32 Character'),
    password: Yup.string()
      .required('Required')
      .min(8, 'Min 8 Character')
      .max(32, 'Max 32 Character')
  });

  return (
    <div className="h-screen flex justify-center items-center robolaunch-bg animate__animated animate__fadeIn">
      <div className="px-8 pt-8 sm:px-4 bg-layer-400 text-white text-center rounded-lg">
        <img className="mx-auto w-48 h-auto" src="../../../logo/plain.svg" alt="Robolaunch" />
        <p className="py-4 text-base font-medium">Welcome to Robolaunch!</p>
        <p className="text-base font-light">Login</p>
        <div>
          <Formik
            initialValues={{
              username: '',
              password: '',
              clientId: EnvVariables.CLIENT_ID,
              clientSecret: EnvVariables.CLIENT_SECRET,
              grantType: EnvVariables.GRANT_TYPE,
              scope: EnvVariables.SCOPE,
              responseType: EnvVariables.RESPONSE_TYPE
            }}
            onSubmit={(values, { resetForm }) => {
              dispatch(
                loginUser({
                  loginRequest: {
                    username: values.username,
                    password: values.password,
                    clientId: values.clientId,
                    clientSecret: values.clientSecret,
                    grantType: values.grantType,
                    scope: values.scope,
                    responseType: values.responseType
                  }
                })
              );
              resetForm();
            }}
            validationSchema={LoginSchema}
          >
            <Form className="grid justify-items-center items-center">
              <fieldset disabled={false}>
                <div className="flex flex-col gap-2">
                  <InputField id="username" info="Type Username" label="Username">
                    <Field
                      className="h-10 rounded p-2 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                      id="username"
                      name="username"
                      placeholder="e.g. Doe9213"
                      type="text"
                      disabled={isFetchingLoginUser}
                      required
                    />
                  </InputField>

                  <InputField id="password" info="Type Password" label="Password">
                    <Field
                      className="h-10 rounded p-2 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic transition-all"
                      id="password"
                      name="password"
                      placeholder="**********"
                      type={showPassword}
                      disabled={isFetchingLoginUser}
                      required
                    />
                    <div
                      onClick={() => {
                        showPassword === 'password'
                          ? setShowPassword('text')
                          : setShowPassword('password');
                      }}
                      className="flex items-center justify-center h-7 w-auto absolute top-1/2 right-2"
                    >
                      {showPassword === 'password' ? (
                        <img src="/icons/visibility.svg" alt="show password" />
                      ) : (
                        <img src="/icons/visibility_off.svg" alt="show password" />
                      )}
                    </div>
                  </InputField>
                </div>
              </fieldset>
              <button
                disabled={isFetchingLoginUser}
                className={`w-full h-10 mt-7 font-semibold bg-primary-200 rounded hover:bg-primary-400 ${
                  isFetchingLoginUser && '!bg-layer-100 border border-layer-600'
                }`}
                type="submit"
              >
                {isFetchingLoginUser ? (
                  <div className="mx-auto relative w-8 h-auto -top-9">
                    <Loader />
                  </div>
                ) : (
                  'Login'
                )}
              </button>
            </Form>
          </Formik>
        </div>
        <div className="flex justify-between pt-6 pb-4">
          <Link className="text-lowContrast text-sm" to="/register">
            Create a new account
          </Link>
          <Link className="text-lowContrast text-sm" to="/forgot-password">
            Forgot Password
          </Link>
        </div>
      </div>
    </div>
  );
};
export default Login;
