import React from 'react';
import { Formik, Field, Form } from 'formik';
import InputField from '../../../components/Form/InputField/InputField';

const onSubmit = (values) => {
  if (values.password == values.confirmpassword) {
    console.log(values);
    alert('click');
  }
};

const ChangePassword = () => {
  return (
    <>
      <div className="block w-[100vw] z-10">
        <div className="container h-[100vh] mx-auto grid justify-items-center items-center">
          <div className="bg-layer-400 px-10 pt-10 pb-5 text-center text-white rounded-lg">
            <img
              className="block mx-auto w-[88px]"
              src="../../../icons/dark/menu/robolaunch-mini.svg"
              alt="Robolaunch"
            />
            <h3 className="m-0 pt-8 pb-2">Set Password</h3>
            <span>Change Password For Your Account</span>
            <div className="py-4">
              <Formik
                initialValues={{
                  password: '',
                  confirmPassword: ''
                }}
                onSubmit={(values) => onSubmit(values)}
              >
                <Form className="grid justify-items-center items-center">
                  <InputField id="password" info="Type Password" label="Password">
                    <Field
                      className="h-input rounded p-2 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                      id="password"
                      name="password"
                      placeholder="Password"
                      type="password"
                    />
                  </InputField>
                  <InputField id="confirmPassword" info="Confirm Password" label="Confirm Password">
                    <Field
                      className="h-input rounded p-2 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                      id="confirmPassword"
                      name="confirmPassword"
                      placeholder="Confirm Password"
                      type="password"
                    />
                  </InputField>

                  <button
                    className="w-full h-10 mt-4 bg-primary-200 rounded hover:bg-primary-400"
                    type="submit"
                  >
                    Set Password
                  </button>
                </Form>
              </Formik>
            </div>
          </div>
        </div>
      </div>
      <div className="absolute register-bg w-[100vw] h-[100vh] z-0 "></div>
    </>
  );
};
export default ChangePassword;
