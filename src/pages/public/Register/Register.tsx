import React, { useEffect } from 'react';
import { Formik, Field, Form } from 'formik';
import InputField from '../../../components/Form/InputField/InputField';
import { useAppSelector, useAppDispatch } from '../../../hooks/redux';
import { registerUser, clearStateRegisterUser } from '../../../store/features/user/userSlice';
import { Link, Navigate } from 'react-router-dom';
import * as Yup from 'yup';
import 'animate.css';

const Register = () => {
  const dispatch = useAppDispatch();
  const { isFetchingRegisterUser, isSuccessRegisterUser } = useAppSelector((state) => state.user);

  const RegisterSchema = Yup.object().shape({
    user: Yup.object().shape({
      username: Yup.string()
        .matches(/^\w[a-zA-Z@#0-9.]*$/, 'Only English Characters and Numbers')
        .min(3, 'Min 3 Character')
        .max(32, 'Max 32 Character')
        .required('Username is required'),
      firstName: Yup.string()
        .min(2, 'Min 2 Character')
        .max(32, 'Max 32 Character')
        .required('First Name is required'),
      lastName: Yup.string()
        .min(2, 'Min 2 Character')
        .max(32, 'Max 32 Character')
        .required('Last Name is required'),
      email: Yup.string().email('Invalid email').required('Email is required')
    })
  });

  useEffect(() => {
    localStorage.clear();
    dispatch(clearStateRegisterUser());
  }, []);

  // if (isSuccessRegisterUser) {
  //   dispatch(clearStateRegisterUser());
  //   return <Navigate to="/login" />;
  // }

  return (
    <div className="h-screen flex justify-center items-center robolaunch-bg animate__animated animate__fadeIn">
      <div className="px-8 pt-8 sm:px-4 bg-layer-400 text-white text-center rounded-lg">
        <img className="mx-auto w-48 h-auto" src="../../../logo/plain.svg" alt="Robolaunch" />
        <p className="py-4 text-base font-medium">Welcome to Robolaunch!</p>
        <p className="text-base font-light">Register</p>
        <Formik
          validationSchema={RegisterSchema}
          initialValues={{
            user: {
              username: '',
              firstName: '',
              lastName: '',
              email: ''
            }
          }}
          onSubmit={(values, { resetForm }) => {
            dispatch(registerUser(values));
            resetForm();
          }}
        >
          <Form className="grid justify-items-center items-center ">
            <fieldset disabled={isFetchingRegisterUser}>
              <div className="flex flex-col gap-2">
                <InputField id="user.username" info="User name" label="Username">
                  <Field
                    className="h-10 rounded p-2 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                    id="user.username"
                    name="user.username"
                    placeholder="e.g. Doe9213"
                    required
                  />
                </InputField>

                <InputField id="user.firstName" info="First name" label="First Name">
                  <Field
                    className="h-10 rounded p-2 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                    id="user.firstName"
                    name="user.firstName"
                    placeholder="e.g. John"
                    required
                  />
                </InputField>

                <InputField id="user.lastName" info="Last Name" label="Last Name">
                  <Field
                    className="h-10 rounded p-2 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                    id="user.lastName"
                    name="user.lastName"
                    placeholder="e.g. Doe"
                    required
                  />
                </InputField>

                <InputField id="user.email" info="E-mail" label="E-mail">
                  <Field
                    className="h-10 rounded p-2 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                    id="user.email"
                    name="user.email"
                    placeholder="e.g. johndoe@gmail.com"
                    required
                  />
                </InputField>
              </div>
            </fieldset>
            <button
              className="w-full h-10 font-semibold mt-6 bg-primary-200 rounded hover:bg-primary-400"
              type="submit"
            >
              Register
            </button>
          </Form>
        </Formik>
        <div className="py-4">
          <Link className="text-lowContrast text-sm" to="/">
            Already have an account?
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Register;
