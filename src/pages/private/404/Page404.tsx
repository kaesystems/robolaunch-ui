import React from 'react';
import { Link } from 'react-router-dom';

function page404() {
  return (
    <div className="container mx-auto flex flex-col justify-center items-center h-[100vh]">
      <h3 className="text-[6rem]">404</h3>
      <Link className="underline text-base " to="/">
        Redirect to Main Page
      </Link>
    </div>
  );
}
export default page404;
