import React from 'react';
import 'animate.css';

interface ILoading {
  unmount: boolean;
}

const Loading = ({ unmount = false }: ILoading) => {
  return (
    <div
      className={`flex justify-center items-center fixed h-screen w-screen top-0 left-0 z-50 bg-layer-200 touch-none  animate__animated  ${
        unmount ? 'animate__fadeOut' : 'animate__fadeIn'
      } `}
    >
      <img className="absolute w-16" src="/logo/logo.svg" alt="Robolaunch" />
      <img className="animate-spin w-36" src="/logo/ring.svg" alt="Robolaunch" />
    </div>
  );
};
export default Loading;
