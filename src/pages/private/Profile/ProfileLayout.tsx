import React, { useContext, useState } from 'react';
import { ThemeContext } from '../../../context/ThemeContext';
import InputField from '../../../components/Form/InputField/InputField';
import { Formik, Field } from 'formik';
import { NavLink, Outlet, Route } from 'react-router-dom';
import Profile from './Profile';
import Account from './Account';
import Security from './Security';
import Organizations from './Organizations';

function ProfileLayout() {
  const { theme, setTheme } = useContext(ThemeContext);
  const [inPage, SetInPage] = useState('profile');
  return (
    <section>
      <div className="flex gap-x-8">
        <div className="w-64 shadow-md bg-light-200 dark:bg-layer-200 rounded border border-light-300 dark:border-layer-600 cursor-pointer transition-all h-fit p-3  ">
          <ul className="flex flex-col gap-y-2">
            <li
              onClick={() => SetInPage('profile')}
              className="flex items-center text-sm hover:bg-light-300 hover:rounded dark:hover:bg-layer-400 p-1 transition-all dark:text-light-100"
            >
              <img
                className="w-9"
                src={`/icons/${theme}/public_profile.svg`}
                alt="Public Profile"
              />
              <span>Public Profile</span>
            </li>
            <li
              onClick={() => SetInPage('account')}
              className="flex items-center text-sm hover:bg-light-300 hover:rounded dark:hover:bg-layer-400 p-1 transition-all dark:text-light-100"
            >
              <img className="w-9" src={`/icons/${theme}/user.svg`} alt="Account" />
              <span>Account</span>
            </li>
            <li
              onClick={() => SetInPage('security')}
              className="flex items-center text-sm hover:bg-light-300 hover:rounded dark:hover:bg-layer-400 p-1 transition-all dark:text-light-100"
            >
              <img className="w-9" src={`/icons/${theme}/shield.svg`} alt="Security" />
              <span>Security</span>
            </li>
            <li
              onClick={() => SetInPage('organizations')}
              className="flex items-center text-sm hover:bg-light-300 hover:rounded dark:hover:bg-layer-400 p-1 transition-all dark:text-light-100"
            >
              <img className="w-9" src={`/icons/${theme}/organizations.svg`} alt="Organizations" />
              <span>Organizations</span>
            </li>
          </ul>
        </div>
        <div className="flex-auto p-10 bg-light-200 dark:bg-layer-200 rounded border shadow-md border-light-300 dark:border-layer-600 transition-all dark:text-light-100">
          {inPage == 'profile' && <Profile />}
          {inPage == 'account' && <Account />}
          {inPage == 'security' && <Security />}
          {inPage == 'organizations' && <Organizations />}
        </div>
      </div>
    </section>
  );
}
export default ProfileLayout;
