import React from 'react';
import Button from '../../../components/Button/Button';

function Account() {
  return (
    <div>
      <h3 className="font-semibold m-0 pb-10">Account</h3>
      <h4 className="font-semibold text-base m-0 pb-3">Deactive Account</h4>
      <div>
        <div>
          <Button text="Deactive Account" disabled={false} type={'button'} style={'delete'} />
        </div>
      </div>
    </div>
  );
}
export default Account;
