import React from 'react';
import { Formik, Field } from 'formik';
import InputField from '../../../components/Form/InputField/InputField';
import { ThemeContext } from '../../../context/ThemeContext';
import Button from '../../../components/Button/Button';
import { useAppSelector } from '../../../hooks/redux';

function Profile() {
  const { theme } = React.useContext(ThemeContext);
  const { currentUser } = useAppSelector((state) => state.user);

  const { user } = useAppSelector((state) => state.user);

  return (
    <div>
      <h3 className="font-semibold m-0 pb-10">Public Profile</h3>
      <h4 className="font-semibold text-base m-0 pb-3">Account Details</h4>
      <div>
        <div className="w-full flex items-center justify-between">
          <div>
            <div className="flex flex-col gap-3">
              <p>{currentUser.username}</p>
            </div>
          </div>
          <img width={256} src={`/icons/${theme}/user.svg`} alt="" />
        </div>
        <Formik
          initialValues={{ firstName: '', lastName: '', email: '' }}
          onSubmit={() => console.log('click')}
        >
          <div className="grid grid-cols-2 gap-4">
            <div>
              <InputField id="firstName" info="First Name" label="First Name">
                <Field
                  className="h-input rounded p-2 dark:bg-layer-100 border bg-light-100 border-light-400 dark:border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                  name="firstname"
                  placeholder="First Name"
                  type="text"
                />
              </InputField>
            </div>
            <div>
              <InputField id="lastName" info="Last Name" label="Last Name">
                <Field
                  className="h-input rounded p-2 dark:bg-layer-100 border bg-light-100 border-light-400 dark:border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                  name="lastName"
                  placeholder="Last Name"
                  type="text"
                />
              </InputField>
            </div>
            <div>
              <InputField id="username" info="Username" label="Username">
                <Field
                  className="h-input rounded p-2 dark:bg-layer-100 border bg-light-100 border-light-400 dark:border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                  name="username"
                  placeholder="First Name"
                  type="text"
                  disabled
                />
              </InputField>
            </div>
            <div>
              <InputField id="email" info="E-mail" label="E-mail">
                <Field
                  className="h-input rounded p-2 dark:bg-layer-100 border bg-light-100 border-light-400 dark:border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                  name="email"
                  placeholder="E-mail"
                  type="text"
                />
              </InputField>
            </div>
          </div>
        </Formik>
        <div className="py-5">
          <Button style={'primary'} type={'button'} disabled={false} text="Save" />
        </div>
      </div>
    </div>
  );
}
export default Profile;
