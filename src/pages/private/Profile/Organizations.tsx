import React, { useState } from 'react';
import ReactModal from 'react-modal';
import Button from '../../../components/Button/Button';
import { useAppSelector } from '../../../hooks/redux';
import { modalStyles } from '../../../utils/css/modalStyles';
import CreateOrganization from '../UserRoleManagement/Modals/CreateOrganizationModal';

function Organizations() {
  const [isOrganizationCreateModalOpen, setIsOrganizationCreateModalOpen] = useState(false);
  const { organizations } = useAppSelector((state) => state.organization);
  const { currentUser } = useAppSelector((state) => state.user);

  const handleOpenOrganizationCreateModal = () => {
    setIsOrganizationCreateModalOpen(true);
  };
  const handleCloseOrganizationCreateModal = () => {
    setIsOrganizationCreateModalOpen(false);
  };

  return (
    <div>
      <h3 className="font-semibold m-0 pb-10">Organizations</h3>
      <h4 className="font-semibold text-base m-0 pb-1">Member Organizations</h4>
      <div>
        <div className="flex flex-col">
          {organizations.map((org, index) => {
            return (
              <div
                key={index}
                className="flex p-5 items-center justify-between odd:bg-light-100 dark:bg-layer-400 bg-light-200 odd:dark:bg-layer-300 border border-light-400 dark:border-layer-400 first:rounded-t-md last:rounded-b-md"
              >
                <span>{org.name}</span>
                <span className="border border-layer-300 dark:border-light-400 py-1 px-2 rounded-lg text-sm">
                  {org.name === `org-default-${currentUser.username}` && 'Personal Workspace'}
                  {org.name.split('-')[0] !== 'org' && 'Enterprise Workspace'}
                </span>
                <Button
                  disabled={false}
                  type={'button'}
                  style={'delete'}
                  text="Leave Organization"
                />
              </div>
            );
          })}
        </div>
      </div>
      <h4 className="font-semibold text-base m-0 pt-10 pb-1">Create Organization</h4>
      <div className="pt-4">
        <Button
          disabled={false}
          style="primary"
          type={'button'}
          handleClick={handleOpenOrganizationCreateModal}
          text="Create Organization"
        />
      </div>
      <ReactModal
        isOpen={isOrganizationCreateModalOpen}
        onRequestClose={handleCloseOrganizationCreateModal}
        shouldCloseOnOverlayClick={false}
        style={modalStyles}
        contentLabel="Create Organization"
      >
        <CreateOrganization handleClose={handleCloseOrganizationCreateModal} />
      </ReactModal>
    </div>
  );
}
export default Organizations;
