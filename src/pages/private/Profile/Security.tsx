import React from 'react';
import InputField from '../../../components/Form/InputField/InputField';
import { Formik, Field } from 'formik';

function Security() {
  return (
    <div>
      <h3 className="font-semibold m-0 pb-10">Security</h3>
      <h4 className="font-semibold text-base m-0 pb-1">Password</h4>
      <div>
        <Formik
          initialValues={{ oldpassword: '', password: '', confirmPassword: '' }}
          onSubmit={() => {
            console.log('click');
          }}
        >
          <div className="grid grid-cols-3 gap-4">
            <div>
              <InputField id="oldpassword" info="Old Password" label="Old Password">
                <Field
                  className="h-input bg-light-100 border-light-400 rounded p-2 dark:bg-layer-100 border dark:border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                  name="password"
                  placeholder="Password"
                  type="password"
                />
              </InputField>
            </div>
            <div>
              <InputField id="password" info="Password" label="Password">
                <Field
                  className="h-input bg-light-100 border-light-400 rounded p-2 dark:bg-layer-100 border dark:border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                  name="password"
                  placeholder="Password"
                  type="password"
                />
              </InputField>
            </div>
            <div>
              <InputField id="confirmPassword" info="Confirm Password" label="Confirm Password">
                <Field
                  className="h-input bg-light-100 border-light-400 rounded p-2 dark:bg-layer-100 border dark:border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                  name="confirmPassword"
                  placeholder="Confirm Password"
                  type="password"
                />
              </InputField>
            </div>
          </div>
        </Formik>
      </div>
    </div>
  );
}
export default Security;
