import React from 'react';

const Create = () => {
  return (
    <div className="w-full flex gap-4">
      <div className="w-1/6">
        <ul className="bg-layer-200 text-white border border-layer-500 rounded">
          <li>1</li>
          <li>2</li>
          <li>3</li>
          <li>4</li>
        </ul>
      </div>
      <div className="w-5/6 bg-layer-200 text-white border border-layer-500 rounded">
        Page Content
      </div>
    </div>
  );
};

export default Create;
