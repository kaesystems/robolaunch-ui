/* eslint-disable @typescript-eslint/ban-ts-comment */
import React, { useContext, useEffect, useState } from 'react';
import { useFilters, usePagination, useSortBy, useTable } from 'react-table';
import Modal from 'react-responsive-modal';
import { ThemeContext } from '../../../context/ThemeContext';
import ReactApexChart from 'react-apexcharts';
import Table from '../../../components/Table/Table';
import mockOrganization from '../../../mock/mockOrganization.json';
import BlockDataWidget from '../../../components/Widgets/BlockDataWidget/BlockDataWidget';
import { Field, Form, Formik } from 'formik';
import Button from '../../../components/Button/Button';
import InputField from '../../../components/Form/InputField/InputField';
import { teamSchema } from '../../../schemas/Schemas';
import useWindowDimensions from '../../../hooks/useWindowDimensions';

function Organization() {
  const mockCurrentOrg = 'Organization1';
  const { width, height } = useWindowDimensions();
  const [isActiveDepartmentModal, setIsActiveDepartmentModal] = useState('');
  const [isShowDepartmentModal, setIsShowDepartmentModal] = useState(false);
  const [isShowCreateModal, setIsShowCreateModal] = useState(false);
  const { theme } = useContext(ThemeContext);

  const handleIsShowDepartmentModal = (value) => {
    setIsActiveDepartmentModal(value);
    setIsShowDepartmentModal(true);
  };
  const handleIsCloseDepartmentModal = () => {
    setIsActiveDepartmentModal('');
    setIsShowDepartmentModal(false);
  };

  const handleCreate = () => {
    setIsShowCreateModal(!isShowCreateModal);
  };

  const data = React.useMemo(
    () =>
      mockOrganization.departments.map((department) => {
        return {
          key: department.departmentName,
          createdDate: department.createdDate,
          roboticsClouds: department.roboticsClouds,
          fleets: department.fleets,
          registeredPhysicalInstances: department.registeredPhysicalInstances,
          robots: department.robots,
          users: department.users
        };
      }),
    []
  );
  const columns = React.useMemo(
    () => [
      {
        Header: 'Department Name',
        accessor: 'key',
        Cell: ({ cell: { value } }) => (
          <div
            className="cursor-pointer underline"
            onClick={() => handleIsShowDepartmentModal(value)}
          >
            <span>{value}</span>
          </div>
        )
      },
      {
        Header: 'Robotic Clouds',
        accessor: 'roboticsClouds',
        Cell: ({ cell: { value } }) => (
          <div className="flex flex-col gap-2">
            <div className="flex gap-2 items-center">
              <span>
                <span className="md:hidden">Robotics Cloud: </span>
                <b>{value.totalCount}</b>
              </span>
            </div>
            <div className="flex gap-4 2xl:flex-col 2xl:gap-0 md:hidden">
              <div className="flex items-center">
                <img className="w-6 scale-125" src={`/icons/${theme}/power.svg`} />
                <span>
                  Active: <b>{value.activeCount}</b>
                </span>
              </div>
              <div className="flex gap-4">
                <div className="flex items-center">
                  <img className="w-6 scale-125" src={`/icons/${theme}/power-off.svg`} />
                  <span>
                    Stopped: <b>{value.stoppedCount}</b>
                  </span>
                </div>
              </div>
            </div>
          </div>
        )
      },
      {
        Header: 'Fleets',
        accessor: 'fleets',
        Cell: ({ cell: { value } }) => (
          <div className="flex flex-col gap-2">
            <div className="flex gap-2 items-center">
              <span>
                <span className="md:hidden">Fleet: </span>
                <b>{value.totalCount}</b>
              </span>
            </div>
            <div className="flex gap-4 2xl:flex-col 2xl:gap-0 md:hidden">
              <div className="flex items-center">
                <img className="w-6 scale-125" src={`/icons/${theme}/cloud.svg`} />
                <span>
                  Cloud Only: <b>{value.activeCount}</b>
                </span>
              </div>
              <div className="flex items-center">
                <img className="w-6 scale-125" src={`/icons/${theme}/cloud-powered.svg`} />
                <span>
                  Cloud Powered: <b>{value.stoppedCount}</b>
                </span>
              </div>
            </div>
          </div>
        )
      },
      {
        Header: 'Robots',
        accessor: 'robots',
        Cell: ({ cell: { value } }) => (
          <div className="flex flex-col gap-2">
            <div className="flex gap-2 items-center">
              <span>
                <span className="2xl:hidden">Robots: </span>
                <b>{value.totalCount}</b>
              </span>
            </div>
          </div>
        )
      },
      {
        Header: 'Date of Creation',
        accessor: 'createdDate',

        Cell: ({ cell: { value } }) => <div className="flex flex-col gap-2">{value}</div>
      },

      {
        Header: 'Users',
        accessor: 'users',
        Cell: ({ cell: { value } }) => (
          <div className="flex flex-col gap-2">
            <div className="flex gap-2 items-center">
              <span>
                <span className="xl:hidden">Users: </span>
                <b>{value.totalCount}</b>
              </span>
            </div>
            <div className="flex gap-4 2xl:hidden">
              <div className="flex items-center">
                <img className="w-6 scale-150" src={`/icons/${theme}/user.svg`} />
                <span>
                  User: <b>{value.userCount}</b>
                </span>
              </div>
              <div className="flex items-center">
                <img className="w-6 scale-150" src={`/icons/${theme}/manager.svg`} />
                <span>
                  Manager: <b>{value.managerCount}</b>
                </span>
              </div>
            </div>
          </div>
        )
      }
    ],
    [theme, width]
  );
  const tableInstance = useTable(
    {
      columns,
      data,
      initialState: {
        hiddenColumns: width < 1280 ? ['createdDate'] : []
      }
    },
    useFilters,
    useSortBy,
    usePagination
  );

  const chardonutoptions = {
    series: [
      mockOrganization.totalCloudOnlyRobotCount,
      mockOrganization.totalCloudPoweredRobotCount
    ],
    options: {
      labels: ['Cloud Only Robot', 'Cloud Powered Robot'],
      legend: {
        show: true,
        position: 'bottom',
        labels: {
          colors: theme == 'dark' ? '#fff' : '#000'
        }
      },
      stroke: {
        show: true,
        colors: [theme === 'dark' ? '#565674' : '#000'],
        width: 1
      },
      dataLabels: {
        style: {
          fontSize: '15px',
          fontWeight: 'medium'
        }
      },
      colors: ['#00e396', '#008ffb']
    }
  };
  const charsplineoptions = {
    series: [
      {
        name: 'Spending for Robotics Clouds',
        data: [31, 40, 28, 51, 42, 109, 100]
      },
      {
        name: 'Spending for Physical Instances',
        data: [11, 32, 45, 32, 34, 52, 41]
      }
    ],
    options: {
      theme: {
        mode: 'dark',
        palette: 'palette1'
      },
      chart: {
        height: 350,
        type: 'area',
        background: 'transparent'
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth'
      },
      xaxis: {
        type: 'date',
        categories: [
          '2018-09-19',
          '2018-09-19',
          '2018-09-19',
          '2018-09-19',
          '2018-09-19',
          '2018-09-19',
          '2018-09-19'
        ],
        labels: {
          style: {
            colors: ['white']
          }
        }
      },
      tooltip: {
        theme: 'dark',
        x: {
          format: 'dd/MM/yy'
        }
      }
    }
  };

  const dataBlockDataWidget = {
    type: 'Organization',
    data1: {
      title: 'Users',
      icon: 'organization',
      count: 10,
      subdata1: {
        icon: 'manager',
        count: 5,
        subtitle: 'Manager'
      },
      subdata2: {
        icon: 'user',
        count: 5,
        subtitle: 'Users'
      }
    },
    data2: {
      title: 'Teams',
      icon: 'team',
      count: 10,
      subdata1: {
        icon: 'power',
        count: 5,
        subtitle: 'Active Cloud'
      },
      subdata2: {
        icon: 'power-off',
        count: 5,
        subtitle: 'Stopped Cloud'
      }
    },
    data3: {
      title: 'Fleets',
      icon: 'fleet',
      count: 10,
      subdata1: {
        icon: 'power',
        count: 5,
        subtitle: 'Active Robot'
      },
      subdata2: {
        icon: 'power-off',
        count: 5,
        subtitle: 'Stopped Robot'
      }
    }
  };

  return (
    <div className="flex flex-col gap-10 lg:gap-6 animate__animated animate__fadeIn">
      <div className="grid grid-cols-4 gap-10 lg:gap-6 dark:text-white">
        <div className="2xl:col-span-2">
          <BlockDataWidget value={dataBlockDataWidget} />
        </div>
        <div className="flex flex-col gap-4 2xl:col-span-2 dark:bg-layer-200 dark:hover:bg-layer-300 border dark:border-layer-400 rounded-2xl p-6 transition-all">
          <div className="flex flex-col">
            <span className="text-lg font-semibold ">Robots</span>
            <span className="text-sm text-gray-200">Live Status</span>
          </div>
          <div className="flex justify-center items-center h-full">
            <ReactApexChart
              series={chardonutoptions.series}
              // @ts-ignore
              options={chardonutoptions.options}
              width={320}
              type="donut"
            />
          </div>
        </div>
        <div className="col-span-2 flex 2xl:col-span-4 flex-col h-full justify-between gap-4 dark:bg-layer-200 dark:hover:bg-layer-300 border dark:border-layer-400 rounded-2xl p-6 transition-all">
          <div className="flex flex-col">
            <span className="text-lg font-semibold ">Utilization</span>
            <span className="text-sm text-gray-200">Last Week Spending</span>
          </div>
          <div className="h-full w-full">
            <ReactApexChart
              series={charsplineoptions.series}
              // @ts-ignore
              options={charsplineoptions.options}
              type="area"
              height={'256'}
            />
          </div>
        </div>
      </div>
      <Table
        createButtonText="Create Team"
        icon="team"
        tableInstance={tableInstance}
        title="Teams"
        handleCreate={() => handleCreate}
      />
      <Modal open={isShowDepartmentModal} onClose={() => handleIsCloseDepartmentModal()} center>
        <div className="flex flex-col items-center">
          <span>{isActiveDepartmentModal}</span>
        </div>
      </Modal>
      <Modal open={isShowCreateModal} onClose={() => handleCreate()} center>
        <div className="flex flex-col items-center">
          <span className="text-md font-semibold pb-4">Create Team</span>
          <span className="text-sm">You can name the team you want to create.</span>
          <Formik
            initialValues={{
              teamName: ''
            }}
            onSubmit={(value) => {
              console.log(value);
              handleCreate();
            }}
            validationSchema={teamSchema}
          >
            <Form className="flex flex-col items-center gap-8">
              <InputField id="teamName" info="Type New Team Name" label="Team Name">
                <Field className="input-text" name="teamName" type="text" placeholder="NewMyTeam" />
              </InputField>
              <Button disabled={false} style="primary" type={'submit'} text={`Create Team`} />
            </Form>
          </Formik>
        </div>
      </Modal>
    </div>
  );
}

export default Organization;
