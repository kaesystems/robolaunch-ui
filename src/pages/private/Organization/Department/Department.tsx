import React, { useContext, useState } from 'react';
import { useFilters, usePagination, useSortBy, useTable } from 'react-table';
import 'animate.css';
import mockDepartment from '../../../../mock/mockDepartment.json';
import { ThemeContext } from '../../../../context/ThemeContext';
import Table from '../../../../components/Table/Table';
import BlockDataWidget from '../../../../components/Widgets/BlockDataWidget/BlockDataWidget';
import ReactApexChart from 'react-apexcharts';
import { Navigate, useNavigate } from 'react-router';
import useWindowDimensions from '../../../../hooks/useWindowDimensions';

const Department = () => {
  const { theme } = useContext(ThemeContext);
  const navigate = useNavigate();
  const { width, height } = useWindowDimensions();

  const handleCreate = () => {
    navigate('/create-robotics-cloud');
  };

  const data = React.useMemo(
    () =>
      mockDepartment.roboticsClouds.map((roboticsCloud) => {
        return {
          key: roboticsCloud.roboticsCloudName,
          regionName: roboticsCloud.regionName,
          resources: roboticsCloud.resources,
          fleets: roboticsCloud.fleets,
          robots: roboticsCloud.robots,
          status: roboticsCloud.status,
          users: roboticsCloud.users
        };
      }),
    []
  );
  const columns = React.useMemo(
    () => [
      {
        Header: 'Robotics Clouds',
        accessor: 'key',
        Cell: ({ cell: { value } }) => (
          <div>
            <span>{value}</span>
          </div>
        )
      },
      {
        Header: 'Region',
        accessor: 'regionName',
        Cell: ({ cell: { value } }) => (
          <div>
            <span>{value}</span>
          </div>
        )
      },
      {
        Header: 'Robotics Cloud Resources',
        accessor: 'resources',
        Cell: ({ cell: { value } }) => (
          <div className="grid grid-cols-2 grid-rows-2">
            <div className="flex items-center gap-2 xl:flex-col xl:gap-0">
              <img className="w-6 scale-150" src={`/icons/${theme}/cpu.svg`} />
              <span>
                <b>{value.cpu.cores} Core</b>
              </span>
            </div>
            <div className="flex items-center gap-2 xl:flex-col xl:gap-0">
              <img className="w-6 scale-150" src={`/icons/${theme}/cpu.svg`} />
              <span>
                <b>{value.gpu.cores} Core</b>
              </span>
            </div>
            <div className="flex items-center gap-2 xl:flex-col xl:gap-0">
              <img className="w-6 scale-150" src={`/icons/${theme}/cpu.svg`} />
              <span>
                <b>{value.ram.size} GB</b>
              </span>
            </div>
            <div className="flex items-center gap-2 lg:flex-col lg:gap-0">
              <img className="w-6 scale-150" src={`/icons/${theme}/storage.svg`} />
              <span>
                <b>{value.storage.size} GB</b>
              </span>
            </div>
          </div>
        )
      },
      {
        Header: 'Fleets',
        accessor: 'fleets',
        Cell: ({ cell: { value } }) => (
          <div className="flex flex-col gap-2">
            <div className="flex gap-2 items-center">
              <span>
                Fleets: <b>{value.totalCount}</b>
              </span>
            </div>
            <div className="flex gap-4 lg:hidden">
              <div className="flex items-center">
                <img className="w-6 scale-125" src={`/icons/${theme}/cloud.svg`} />
                <span>
                  <b>{value.activeCount}</b>
                </span>
              </div>
              <div className="flex items-center">
                <img className="w-6 scale-125" src={`/icons/${theme}/cloud-powered.svg`} />
                <span>
                  <b>{value.stoppedCount}</b>
                </span>
              </div>
            </div>
          </div>
        )
      },
      {
        Header: 'Robots',
        accessor: 'robots',
        Cell: ({ cell: { value } }) => (
          <div className="flex flex-col gap-2">
            <div className="flex gap-2 items-center">
              <span>
                <span className="xl:hidden">Robots: </span>
                <b>{value.totalCount}</b>
              </span>
            </div>
            <div className="flex gap-4 xl:hidden">
              <div className="flex items-center">
                <img className="w-6 scale-125" src={`/icons/${theme}/power.svg`} />
                <span>
                  <b>{value.activeCount}</b>
                </span>
              </div>
              <div className="flex items-center">
                <img className="w-6 scale-125" src={`/icons/${theme}/power-off.svg`} />
                <span>
                  <b>{value.stoppedCount}</b>
                </span>
              </div>
            </div>
          </div>
        )
      },
      {
        Header: 'Status',
        accessor: 'status',
        Cell: ({ cell: { value } }) => (
          <div className="flex items-center gap-2">
            <div
              className={`w-2 h-2 rounded-2xl ${
                value == 'Running'
                  ? ' bg-green-200'
                  : value == 'Stopped'
                  ? ' bg-red-200'
                  : ' bg-yellow-200'
              }`}
            ></div>
            <span>{value}</span>
          </div>
        )
      },
      {
        Header: 'Users',
        accessor: 'users',
        Cell: ({ cell: { value } }) => (
          <div className="flex flex-col gap-2">
            <div className="flex gap-2 items-center">
              <span>
                Users: <b>{value.totalCount}</b>
              </span>
            </div>
            <div className="flex gap-4">
              <div className="flex items-center">
                <img className="w-6 scale-150" src={`/icons/${theme}/user.svg`} />
                <span>
                  <b>{value.userCount}</b>
                </span>
              </div>
              <div className="flex items-center">
                <img className="w-6 scale-150" src={`/icons/${theme}/manager.svg`} />
                <span>
                  <b>{value.managerCount}</b>
                </span>
              </div>
            </div>
          </div>
        )
      }
    ],
    [theme, width]
  );
  const tableInstance = useTable(
    {
      columns,
      data,
      initialState: {
        hiddenColumns: width < 768 ? ['users', 'robots'] : width < 1280 ? ['users'] : []
      }
    },
    useFilters,
    useSortBy,
    usePagination
  );

  const chardonut = {
    series: [20, 10],
    options: {
      labels: ['Cloud Only Robot', 'Cloud Powered Robot'],
      legend: {
        show: true,
        position: 'bottom',
        labels: {
          colors: theme == 'dark' ? '#fff' : '#000'
        }
      },
      stroke: {
        show: true,
        colors: [theme === 'dark' ? '#565674' : '#000'],
        width: 1
      },
      dataLabels: {
        style: {
          fontSize: '15px',
          fontWeight: 'medium'
        }
      },
      colors: ['#00e396', '#008ffb']
    }
  };

  const charspline = {
    series: [
      {
        name: 'Spending for Department',
        data: [31, 40, 28, 51, 42, 109, 100]
      }
    ],
    options: {
      theme: {
        mode: 'dark',
        palette: 'palette1'
      },
      chart: {
        height: 350,
        type: 'area',
        background: 'transparent'
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth'
      },
      xaxis: {
        type: 'date',
        categories: [
          '2018-09-19',
          '2018-09-19',
          '2018-09-19',
          '2018-09-19',
          '2018-09-19',
          '2018-09-19',
          '2018-09-19'
        ],
        labels: {
          style: {
            colors: ['white']
          }
        }
      },
      tooltip: {
        theme: 'dark',
        x: {
          format: 'dd/MM/yy'
        }
      }
    }
  };

  const dataBlockDataWidget = {
    type: 'Department',
    data1: {
      title: 'Users',
      icon: 'team',
      count: 10,
      subdata1: {
        icon: 'manager',
        count: 5,
        subtitle: 'Manager'
      },
      subdata2: {
        icon: 'user',
        count: 5,
        subtitle: 'Users'
      }
    },
    data2: {
      title: 'Robotics Cloud',
      icon: 'server',
      count: 10,
      subdata1: {
        icon: 'cloud',
        count: 5,
        subtitle: 'Cloud Only'
      },
      subdata2: {
        icon: 'cloud-powered',
        count: 5,
        subtitle: 'Cloud Powered'
      }
    },
    data3: {
      title: 'Fleets',
      icon: 'fleet',
      count: 10,
      subdata1: {
        icon: 'cloud',
        count: 5,
        subtitle: 'Cloud Only'
      },
      subdata2: {
        icon: 'cloud-powered',
        count: 5,
        subtitle: 'Cloud Powered'
      }
    }
  };

  return (
    <div className="flex flex-col gap-10 animate__animated animate__fadeIn">
      <div className="grid grid-cols-4 gap-10 text-white">
        <div className="2xl:col-span-2">
          <BlockDataWidget value={dataBlockDataWidget} />
        </div>
        <div className="flex flex-col 2xl:col-span-2 gap-4 dark:bg-layer-200 dark:hover:bg-layer-300 border dark:border-layer-400 rounded-2xl p-6 transition-all">
          <div className="flex flex-col">
            <span className="text-lg font-semibold">Robots</span>
            <span className="text-sm text-gray-200">Live Status</span>
          </div>
          <div className="flex justify-center items-center h-full">
            <ReactApexChart
              series={chardonut.series}
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              options={chardonut.options}
              width={320}
              type="donut"
            />
          </div>
        </div>
        <div className="col-span-2 flex 2xl:col-span-4 flex-col h-full justify-between gap-4 dark:bg-layer-200 dark:hover:bg-layer-300 border dark:border-layer-400 rounded-2xl p-6 transition-all">
          <div className="flex flex-col">
            <span className="text-lg font-semibold ">Utilization</span>
            <span className="text-sm text-gray-200">Last Week Spending</span>
          </div>
          <div className="h-full w-full">
            <ReactApexChart
              series={charspline.series}
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              options={charspline.options}
              type="area"
              height={'256'}
            />
          </div>
        </div>
      </div>
      <Table
        title="Robotics Clouds"
        icon="server"
        createButtonText="Create Robotics Cloud"
        tableInstance={tableInstance}
        handleCreate={() => handleCreate}
      />
    </div>
  );
};

export default Department;
