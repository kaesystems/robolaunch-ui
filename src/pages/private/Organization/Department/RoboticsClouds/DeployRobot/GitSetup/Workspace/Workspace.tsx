/* eslint-disable react/jsx-key */
/* eslint-disable react/no-array-index-key */
import React from 'react';
import { FastField, FieldArray } from 'formik';
import InputField from '../../../../../../../../components/Form/InputField/InputField';
import BuildField from '../BuildField/BuildField';
import LabelWithInfo from '../../../../../../../../components/Form/LabelWithInfo/LabelWithInfo';
import Button from '../../../../../../../../components/Button/Button';
import AddField from '../../../../../../../../components/Pages/DeployRobot/AddField/AddField';

type WorkspaceType = {
  index1: number;
  values: any;
  setValues: React.Dispatch<React.SetStateAction<any>>;
  currentFields: any;
  setCurrentFields: any;
};

const Workspace = ({
  index1,
  values,
  setValues,
  currentFields,
  setCurrentFields
}: WorkspaceType) => {
  const handleAddRepository = (index) => {
    values.workspaces[index].repositories.push({
      gitURL: '',
      branch: '',
      rosPackages: [],
      customLaunchPath: false,
      env: []
    });
    setValues(values);
  };

  const handleAddCommand = () => {
    values.workspaces[index1].buildSteps.push({
      type: 'command',
      name: '',
      text: ''
    });
    setValues(values);
  };

  const handleAddScript = () => {
    values.workspaces[index1].buildSteps.push({
      type: 'script',
      name: '',
      text: ''
    });
    setValues(values);
  };

  const handlePreviousStep = () => {
    if (index1 === 0) {
      setCurrentFields('fleetSetup');
    } else {
      setCurrentFields(`workspaces.workspace${index1 - 1}`);
    }
  };

  const handleNextStep = () => {
    if (values.workspaces[index1].repositories.length === 0) {
      handleAddRepository(index1);
    }
    setCurrentFields(`workspaces.workspace${index1}.repositories.repository0`);
  };
  return (
    <div
      key={index1}
      className="flex flex-col items-center justify-center rounded mb-4 w-full"
      id="workspace"
    >
      <div className="text-sm font-bold uppercase w-full text-lowContrast">
        Workspace {index1 + 1}
      </div>
      <div className="w-full bg-light-200 dark:bg-layer-200 duration-300 transition-all mx-4 border border-light-400 rounded dark:border-layer-600 p-4">
        <InputField id="workspaceName" label="Workspace Name" info="Enter the workspace name.">
          <FastField
            className="h-input rounded p-2 bg-light-100 dark:bg-layer-100 border border-light-400 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100"
            placeholder="e.g airport"
            name={`workspaces[${index1}].name`}
          />
        </InputField>
        <InputField
          id="rosDistro"
          label="ROS Distro"
          info="Select the ROS Distro for this workspace."
        >
          <FastField
            className="h-input rounded p-2 bg-light-100 dark:bg-layer-100 border border-light-400 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100"
            as="select"
            name="rosDistro"
          >
            <option value="foxy">Foxy</option>
            <option value="galactic">Galactic</option>
          </FastField>
        </InputField>

        <label className="flex py-2 items-center">
          <FastField
            className="rounded py-2 bg-light-100 dark:bg-layer-100 placeholder:italic border border-light-400 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 mr-2 w-5 h-5"
            type="checkbox"
            name={`workspaces[${index1}].customBuild`}
          />
          <LabelWithInfo
            label="Custom Build"
            info="Select if the repository is for development purposes."
          />
        </label>

        {/* Custom Build Section  */}
        {values.workspaces[index1].customBuild && (
          <FieldArray name={`workspaces[${index1}].buildSteps`}>
            {(arrayHelpers4) => (
              <div className="bg-light-200 dark:bg-layer-200 rounded py-2">
                <div>
                  <p className="uppercase text-lowContrast text-sm font-bold">Build Steps</p>
                </div>
                <div className="w-full bg-light-200 dark:bg-layer-200 py-2">
                  {values.workspaces[index1].buildSteps &&
                  values.workspaces[index1].buildSteps.length > 0
                    ? values.workspaces[index1].buildSteps.map(
                        (workspaceBuild: any, index4: number) => {
                          return (
                            <BuildField
                              key={index4}
                              index1={index1}
                              index4={index4}
                              values={values}
                              arrayHelpers4={arrayHelpers4}
                            />
                          );
                        }
                      )
                    : ''}
                  <div className="flex justify-center items-center w-[100%]">
                    <AddField text="Add Command" handleClick={handleAddCommand} />
                    <AddField text="Add Script" handleClick={handleAddScript} />
                  </div>
                </div>
              </div>
            )}
          </FieldArray>
        )}
      </div>
      <div className="flex justify-center w-full mt-8 mb-8">
        <div className="flex justify-between flex-row-reverse w-full">
          <Button
            text="Next"
            type="button"
            disabled={false}
            style="primary"
            handleClick={handleNextStep}
          />
          <Button
            text="Previous"
            type="button"
            disabled={false}
            style="default"
            handleClick={handlePreviousStep}
          />
        </div>
      </div>
    </div>
  );
};

export default Workspace;
