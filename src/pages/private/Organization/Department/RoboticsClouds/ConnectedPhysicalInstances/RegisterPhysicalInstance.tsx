import React, { useState } from 'react';
import { Field, Form, Formik } from 'formik';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import Button from '../../../../../../components/Button/Button';
import InputField from '../../../../../../components/Form/InputField/InputField';
import 'animate.css';

const RegisterPhysicalInstance = () => {
  const [isFinished, setIsFinished] = useState(false);
  const navigate = useNavigate();

  const createCloudInstanceSchema = Yup.object().shape({
    region: Yup.string().required('Required'),
    instancetype: Yup.string().required('Required'),
    instancename: Yup.string()
      .required('Required')
      .matches(/^\w[a-zA-Z@#0-9.]*$/, 'Only English Characters and Numbers')
      .min(4, 'Minimum 4 Character')
      .max(16, 'Maximum 16 Character'),
    instancedisksize: Yup.number().required('Required').min(50, 'Minimum 50 GB')
  });

  return (
    <div className="flex flex-col h-screen justify-center items-center animate__animated animate__fadeIn">
      <div className="flex flex-col items-center justify-center gap-8 w-[40vw] h-[64vh] dark:bg-layer-200 border dark:border-layer-500 rounded-2xl">
        <span className="text-white text-md font-semibold">Register Physical Instance</span>
        <div className="flex flex-col gap-3 border dark:border-layer-400 w-[22vw] h-[48vh] p-8 rounded-xl">
          {!isFinished ? (
            <Formik
              initialValues={{
                region: 'eu-central-1',
                instancetype: 't3.medium',
                instancename: '',
                instancedisksize: 100
              }}
              onSubmit={(values) => {
                console.log(values);
                // dispatch(
                //   createCloudInstance({
                //     diskSize: values.instancedisksize,
                //     instanceType: values.instancetype,
                //     organization: {
                //       organization: currentOrganization.name
                //     },
                //     region: values.region,
                //     virtualClusterName: values.instancename
                //   })
                // );
                setIsFinished(!isFinished);
              }}
              validationSchema={createCloudInstanceSchema}
            >
              <Form>
                <InputField id="region" label="Region" info="Select Region">
                  <Field className="input-select" id="region" as="select" name="region">
                    <option value="eu-central-1">Europe | eu-central-1</option>
                  </Field>
                </InputField>
                <InputField id="instancetype" label="Instance Type" info="Select Instance Type">
                  <Field className="input-select" id="instancetype" as="select" name="instancetype">
                    <option value="t3.medium">T3 Medium</option>
                    <option value="t3.large">T3 Large</option>
                  </Field>
                </InputField>
                <InputField id="instancename" label="Instance Name" info="Type Instance Name">
                  <Field
                    className="input-text"
                    id="instancename"
                    name="instancename"
                    placeholder="eg. myCloudInstance"
                  ></Field>
                </InputField>
                <InputField
                  id="instancedisksize"
                  label="Instance Disk Size"
                  info="Type Instance Disk Size"
                >
                  <Field
                    className="input-text"
                    type="number"
                    id="instancedisksize"
                    name="instancedisksize"
                    placeholder="eg. 100"
                  ></Field>
                </InputField>
                <div className="flex justify-center pb-2 pt-6">
                  <Button
                    text="Register Physical Instance"
                    type="submit"
                    disabled={false}
                    style="primary"
                    icon="plus"
                  />
                </div>
              </Form>
            </Formik>
          ) : (
            <div className="flex flex-col h-full items-center justify-center gap-10 animate__animated animate__fadeIn">
              <div className="text-xl">🎉</div>
              <div className="text-center text-white">
                Congratulations! Your Robotics Cloud is now being created. You can follow its status
                on the Department page.
              </div>
              <div className="relative flex justify-center items-center mx-auto w-20 h-auto">
                <img className="absolute w-9" src="logo/logo.svg" />
                <img className="animate-spin" src="logo/ring.svg" />
              </div>
              <div onClick={() => navigate(-1)}>
                <Button
                  disabled={false}
                  style={`primary`}
                  type={`submit`}
                  text={`Redirect to Department Page`}
                />
              </div>
            </div>
          )}
        </div>
      </div>
      <div
        onClick={() => navigate(-1)}
        className="text-white bg-layer-200 m-6 py-2 px-4 border rounded-full dark:border-layer-500 hover:bg-layer-500 cursor-pointer"
      >
        &lt;
      </div>
    </div>
  );
};

export default RegisterPhysicalInstance;
