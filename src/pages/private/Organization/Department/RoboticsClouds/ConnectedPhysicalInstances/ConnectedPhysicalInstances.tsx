import React, { useContext } from 'react';
import { useFilters, usePagination, useSortBy, useTable } from 'react-table';
import { ThemeContext } from '../../../../../../context/ThemeContext';
import Table from '../../../../../../components/Table/Table';
import mockPhysicalInstances from '../../../../../../mock/mockPhysicalInstances.json';
import { useNavigate } from 'react-router';

function ConnectedPhysicalInstances() {
  const { theme } = useContext(ThemeContext);
  const navigate = useNavigate();

  const handleCreate = () => {
    navigate('/register-physical-instance');
  };

  const data = React.useMemo(
    () =>
      mockPhysicalInstances.map((instance) => {
        return {
          key: instance.physicalInstanceName,
          regionName: instance.regionName,
          connectedFleet: instance.connectedFleet,
          connectedRobot: instance.connectedRobot,
          resources: instance.resources,
          status: instance.status,
          users: instance.users
        };
      }),
    []
  );

  const columns = React.useMemo(
    () => [
      {
        Header: 'Instances Name',
        accessor: 'key',
        Cell: ({ cell: { value } }) => <span>{value}</span>
      },
      {
        Header: 'Region',
        accessor: 'regionName',
        Cell: ({ cell: { value } }) => <span>{value}</span>
      },
      {
        Header: 'Resources',
        accessor: 'resources',
        Cell: ({ cell: { value } }) => (
          <div className="grid grid-cols-2 grid-rows-2">
            <div className="flex items-center gap-2">
              <img className="w-8" src={`/icons/${theme}/cpu.svg`} />
              <span>
                <b>{value.cpu.cores} Core</b>
              </span>
            </div>
            <div className="flex items-center gap-2">
              <img className="w-8" src={`/icons/${theme}/cpu.svg`} />
              <span>
                <b>{value.gpu.cores} Core</b>
              </span>
            </div>
            <div className="flex items-center gap-2">
              <img className="w-8" src={`/icons/${theme}/cpu.svg`} />
              <span>
                <b>{value.ram.size} GB</b>
              </span>
            </div>
            <div className="flex items-center gap-2">
              <img className="w-8" src={`/icons/${theme}/cpu.svg`} />
              <span>
                <b>{value.storage.size} GB</b>
              </span>
            </div>
          </div>
        )
      },
      {
        Header: 'Connected Fleet',
        accessor: 'connectedFleet',
        Cell: ({ cell: { value } }) => <span>{value === null ? 'None' : value}</span>
      },
      {
        Header: 'Connected Robot',
        accessor: 'connectedRobot',
        Cell: ({ cell: { value } }) => <span>{value === null ? 'None' : value}</span>
      },
      {
        Header: 'Status',
        accessor: 'status',
        Cell: ({ cell: { value } }) => (
          <div className="flex gap-2 items-center">
            <div
              className={`w-2 h-2 rounded 
            ${value === 'Registering' && 'bg-yellow-200'}
            ${value === 'Connected' && 'bg-green-200'}
            ${value === null && 'bg-gray-200'}
            `}
            ></div>
            <span>{value === null ? 'None' : value}</span>
          </div>
        )
      },
      {
        Header: 'Users',
        accessor: 'users',
        Cell: ({ cell: { value } }) => (
          <div className="flex flex-col gap-2">
            <div className="flex gap-2 items-center">
              <span>
                Total Users: <b>{value.totalCount}</b>
              </span>
            </div>
            <div className="flex gap-4">
              <div className="flex items-center">
                <img className="w-6 scale-150" src={`/icons/${theme}/user.svg`} />
                <span>
                  <b>{value.userCount}</b>
                </span>
              </div>
              <div className="flex items-center">
                <img className="w-6 scale-150" src={`/icons/${theme}/manager.svg`} />
                <span>
                  <b>{value.managerCount}</b>
                </span>
              </div>
            </div>
          </div>
        )
      }
    ],
    [theme]
  );
  const tableInstance = useTable(
    {
      columns,
      data
    },
    useFilters,
    useSortBy,
    usePagination
  );

  return (
    <div className="animate__animated animate__fadeIn">
      <Table
        title="Connected Physical Instances"
        icon="cpu"
        createButtonText="Register Physical Instance"
        tableInstance={tableInstance}
        handleCreate={() => handleCreate}
      />
    </div>
  );
}

export default ConnectedPhysicalInstances;
