import React, { useContext, useState } from 'react';
import { useFilters, usePagination, useSortBy, useTable } from 'react-table';
import 'animate.css';
import Button from '../../../../../components/Button/Button';
import { ThemeContext } from '../../../../../context/ThemeContext';
import mockRoboticsCloud from '../../../../../mock/mockRoboticsCloud.json';
import Table from '../../../../../components/Table/Table';
import BlockDataWidget from '../../../../../components/Widgets/BlockDataWidget/BlockDataWidget';
import ReactApexChart from 'react-apexcharts';
import Modal from 'react-responsive-modal';
import { Field, Form, Formik } from 'formik';
import InputField from '../../../../../components/Form/InputField/InputField';
import { fleetSchema } from '../../../../../schemas/Schemas';
import useWindowDimensions from '../../../../../hooks/useWindowDimensions';

const RoboticsClouds = () => {
  const { theme } = useContext(ThemeContext);
  const [isShowCreateModal, setIsShowCreateModal] = useState(false);
  const { width, height } = useWindowDimensions();

  const handleCreate = () => {
    setIsShowCreateModal(!isShowCreateModal);
  };

  const data = React.useMemo(
    () =>
      mockRoboticsCloud.fleets.map((fleet) => {
        return {
          key: fleet.fleetName,
          fleetTypeModel: fleet.fleetType.model,
          fleetTypeStatus: fleet.fleetType.status,
          robots: fleet.robots,
          connectedRobot: fleet.connectedCloudPoweredRobotCount,
          users: fleet.users
        };
      }),
    []
  );
  const columns = React.useMemo(
    () => [
      {
        Header: 'Fleet Name',
        accessor: 'key',
        Cell: ({ cell: { value } }) => (
          <div>
            <span>{value}</span>
          </div>
        )
      },
      {
        Header: 'Fleet Type',
        accessor: 'fleetTypeModel',
        Cell: ({ cell: { value } }) => (
          <div className="flex items-center gap-8">
            <span>{value}</span>
          </div>
        )
      },
      {
        Header: 'Robots',
        accessor: 'robots',
        Cell: ({ cell: { value } }) => (
          <div className="flex flex-col gap-2">
            <div className="flex gap-2 items-center">
              <span>
                Robots: <b>{value.totalCount}</b>
              </span>
            </div>
            <div className="flex gap-4 md:hidden">
              <div className="flex items-center">
                <img className="w-6 scale-125" src={`/icons/${theme}/power.svg`} />
                <span>
                  Active: <b>{value.activeCount}</b>
                </span>
              </div>
              <div className="flex items-center">
                <img className="w-6 scale-125" src={`/icons/${theme}/power-off.svg`} />
                <span>
                  Stopped: <b>{value.stoppedCount}</b>
                </span>
              </div>
            </div>
          </div>
        )
      },
      {
        Header: 'Connected Physical Robots',
        accessor: 'connectedRobot',
        Cell: ({ cell: { value } }) => <span>{value}</span>
      },
      {
        Header: 'Status',
        accessor: 'fleetTypeStatus',
        Cell: ({ cell: { value } }) => (
          <div className="flex items-center gap-8">
            {value === null ? (
              <span>None</span>
            ) : (
              <div className="flex items-center gap-2">
                <div
                  className={`h-2 w-2 rounded ${value == 'Registered' && 'bg-yellow-200'} ${
                    value == 'Connected' && 'bg-green-200'
                  }`}
                ></div>
                <span>{value}</span>
              </div>
            )}
          </div>
        )
      },
      {
        Header: 'Users',
        accessor: 'users',
        Cell: ({ cell: { value } }) => (
          <div className="flex flex-col gap-2">
            <div className="flex gap-2 items-center">
              <span>
                Users: <b>{value.totalCount}</b>
              </span>
            </div>
            <div className="flex gap-4 lg:hidden">
              <div className="flex items-center">
                <img className="w-6 scale-150" src={`/icons/${theme}/user.svg`} />
                <span>
                  User: <b>{value.userCount}</b>
                </span>
              </div>
              <div className="flex items-center">
                <img className="w-6 scale-150" src={`/icons/${theme}/manager.svg`} />
                <span>
                  Manager: <b>{value.managerCount}</b>
                </span>
              </div>
            </div>
          </div>
        )
      }
    ],
    [theme, width]
  );
  const tableInstance = useTable(
    {
      columns,
      data,
      initialState: {
        hiddenColumns: width < 1280 ? ['connectedRobot'] : []
      }
    },
    useFilters,
    useSortBy,
    usePagination
  );

  const chardonut = {
    series: [8, 7],
    options: {
      labels: ['Cloud Only Robot', 'Cloud Powered Robot'],
      legend: {
        show: true,
        position: 'bottom',
        labels: {
          colors: theme == 'dark' ? '#fff' : '#000'
        }
      },
      stroke: {
        show: true,
        colors: [theme === 'dark' ? '#565674' : '#000'],
        width: 1
      },
      dataLabels: {
        style: {
          fontSize: '15px',
          fontWeight: 'medium'
        }
      },
      colors: ['#00e396', '#008ffb']
    }
  };

  const dataBlockDataWidget = {
    type: 'Robotics Cloud',
    data1: {
      title: 'Users',
      icon: 'team',
      count: 10,
      subdata1: {
        icon: 'manager',
        count: 5,
        subtitle: 'Manager'
      },
      subdata2: {
        icon: 'user',
        count: 5,
        subtitle: 'Users'
      }
    },

    data2: {
      title: 'Fleets',
      icon: 'fleet',
      count: 10,
      subdata1: {
        icon: 'cloud',
        count: 5,
        subtitle: 'Cloud Only'
      },
      subdata2: {
        icon: 'cloud-powered',
        count: 5,
        subtitle: 'Cloud Powered'
      }
    },
    data3: {
      title: 'Robot',
      icon: 'robots',
      count: 10,
      subdata1: {
        icon: 'cloud',
        count: 5,
        subtitle: 'Cloud Only'
      },
      subdata2: {
        icon: 'cloud-powered',
        count: 5,
        subtitle: 'Cloud Powered'
      }
    }
  };

  const charspline = {
    series: [
      {
        name: 'Spending for Robotic Cloud',
        data: [31, 40, 28, 51, 42, 109, 100]
      }
    ],
    options: {
      theme: {
        mode: 'dark',
        palette: 'palette1'
      },
      chart: {
        height: 350,
        type: 'area',
        background: 'transparent'
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth'
      },
      xaxis: {
        type: 'date',
        categories: [
          '2018-09-19',
          '2018-09-19',
          '2018-09-19',
          '2018-09-19',
          '2018-09-19',
          '2018-09-19',
          '2018-09-19'
        ],
        labels: {
          style: {
            colors: ['white']
          }
        }
      },
      tooltip: {
        theme: 'dark',
        x: {
          format: 'dd/MM/yy'
        }
      }
    }
  };

  return (
    <div className="flex flex-col gap-10 animate__animated animate__fadeIn">
      <div className="grid grid-cols-4 gap-10 text-white">
        <div className="xl:col-span-2">
          <BlockDataWidget value={dataBlockDataWidget} />
        </div>
        <div className="flex flex-col gap-4 xl:col-span-2 dark:bg-layer-200 dark:hover:bg-layer-300 border dark:border-layer-400 rounded-2xl p-6 transition-all">
          <div className="flex flex-col">
            <span className="text-lg font-semibold ">Robots</span>
            <span className="text-sm text-gray-200">Live Status</span>
          </div>
          <div className="flex justify-center items-center h-full">
            <ReactApexChart
              series={chardonut.series}
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              options={chardonut.options}
              width={320}
              type="donut"
            />
          </div>
        </div>
        <div className="col-span-2 flex xl:col-span-4 flex-col h-full justify-between gap-4 dark:bg-layer-200 dark:hover:bg-layer-300 border dark:border-layer-400 rounded-2xl p-6 transition-all">
          <div className="flex flex-col">
            <span className="text-lg font-semibold ">Utilization</span>
            <span className="text-sm text-gray-200">Last Week Spending</span>
          </div>
          <div className="h-full w-full">
            <ReactApexChart
              series={charspline.series}
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              options={charspline.options}
              type="area"
              height={'256'}
            />
          </div>
        </div>
      </div>
      <Table
        title="Fleets"
        icon="fleet"
        createButtonText="Create Fleet"
        tableInstance={tableInstance}
        handleCreate={() => handleCreate}
      />
      <Modal open={isShowCreateModal} onClose={() => handleCreate()} center>
        <div className="flex flex-col items-center">
          <span className="text-md font-semibold pb-4">Create Fleet</span>
          <span className="text-sm">You can name the fleet you want to create.</span>
          <Formik
            initialValues={{
              fleetName: '',
              fleetType: 'Cloud Only'
            }}
            onSubmit={(value) => {
              console.log(value);
              handleCreate();
            }}
            validationSchema={fleetSchema}
          >
            <Form className="flex flex-col items-center gap-4">
              <InputField id="fleetType" info="Select New Fleet Type" label="Fleet Type">
                <Field className="input-select w-full" name="fleetType" as="select">
                  <option value="Cloud Only Fleet">Cloud Only</option>
                  <option value="Cloud Powered Fleet">Cloud Powered</option>
                </Field>
              </InputField>
              <InputField id="fleetName" info="Type New Fleet Name" label="Fleet Name">
                <Field className="input-text" name="fleetName" type="text" />
              </InputField>
              <Button disabled={false} style="primary" type={'submit'} text={`Create Fleet`} />
            </Form>
          </Formik>
        </div>
      </Modal>
    </div>
  );
};

export default RoboticsClouds;
