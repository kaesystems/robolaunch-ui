import React from 'react';
import { useTable, useSortBy, useFilters, usePagination } from 'react-table';
import { useAppSelector } from '../../../../../../hooks/redux';
import ToggleOrGo from '../../../../../../components/ToggleOrGo/ToggleOrGo';
import mockRobots from '../../../../../../mock/mockRobots.json';
import { Link, useNavigate } from 'react-router-dom';
import Table from '../../../../../../components/Table/Table';

const Fleet = () => {
  const currentFleet = useAppSelector((state) => state.currentFleet);
  const navigate = useNavigate();

  const handleCreate = () => {
    navigate('/deploy-robot');
  };

  const data = React.useMemo(() => {
    return mockRobots
      .filter((robot) => robot.fleet === currentFleet.fleetName)
      .map((robot) => {
        return {
          key: robot.robotName,
          robotName: robot.robotName,
          status: robot.status,
          region: robot.region,
          distro: robot.distro,
          teleoperation: robot.teleoperation,
          cloudIDE: robot.cloudIDE,
          VDI: robot.VDI
        };
      });
  }, [currentFleet]);
  const columns = React.useMemo(
    () => [
      {
        Header: 'Robot Name',
        accessor: 'key',
        Cell: ({ cell: { value } }) => (
          <Link className="underline" to={`${value}`}>
            {value}
          </Link>
        )
      },
      {
        Header: 'Status',
        accessor: 'status',
        Cell: ({ cell: { value } }) => (
          <div className="flex items-center gap-2">
            <div
              className={`w-2 h-2 rounded-lg 
            ${value == 'Active' && 'bg-green-200'}
            ${value == 'Stopped' && 'bg-red-200'}`}
            ></div>
            <span>{value}</span>
          </div>
        )
      },
      {
        Header: 'Region',
        accessor: 'region',
        Cell: ({ cell: { value } }) => <div>{value}</div>
      },
      {
        Header: 'Distro',
        accessor: 'distro'
      },
      {
        Header: 'Teleoperation',
        accessor: 'teleoperation',
        Cell: ({ cell: { value, row } }) => (
          <ToggleOrGo
            value={value}
            link={`${row.original.robotName}/teleoperation`}
            kind="teleoperation"
          />
        )
      },
      {
        Header: 'VDI',
        accessor: 'VDI',
        Cell: ({ cell: { value, row } }) => (
          <ToggleOrGo value={value} link={`${row.original.robotName}/vdi`} kind="VDI" />
        )
      },
      {
        Header: 'CloudIDE',
        accessor: 'cloudIDE',
        Cell: ({ cell: { value, row } }) => (
          <ToggleOrGo value={value} link={`${row.original.robotName}/cloudide`} kind="Cloud IDE" />
        )
      }
    ],
    []
  );
  const tableInstance = useTable(
    {
      columns,
      data
    },
    useFilters,
    useSortBy,
    usePagination
  );

  return (
    <div className="flex flex-col items-center justify-center animate__animated animate__fadeIn">
      <Table
        title="Robots"
        icon="robots"
        createButtonText="Create Robot"
        tableInstance={tableInstance}
        handleCreate={() => handleCreate}
      />
    </div>
  );
};

export default Fleet;
