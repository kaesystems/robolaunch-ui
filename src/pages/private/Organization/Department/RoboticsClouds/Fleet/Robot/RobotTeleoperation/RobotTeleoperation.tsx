import React from 'react';
import { useParams } from 'react-router-dom';
import RobotInnerLayout from '../../../../../../../../layout/Robot/Robot';

const RobotTeleoperation = () => {
  const params = useParams();
  return (
    <div>
      <RobotInnerLayout id={params.id}>Robot Teleoperation</RobotInnerLayout>
    </div>
  );
};

export default RobotTeleoperation;
