import React, { useContext } from 'react';
import { useParams } from 'react-router-dom';
import RobotInnerLayout from '../../../../../../../../layout/Robot/Robot';
import ToggleOrGo from '../../../../../../../../components/ToggleOrGo/ToggleOrGo';
import { ThemeContext } from '../../../../../../../../context/ThemeContext';

const RobotConfiguration = () => {
  const params = useParams();
  const { theme } = useContext(ThemeContext);

  return (
    <RobotInnerLayout id={params.id}>
      <div className="grid grid-cols-2 lg:grid-cols-1 gap-4">
        <div className="flex flex-col border rounded border-layer-600 dark:bg-layer-200  lg:w-full">
          <div className="flex items-center dark:bg-layer-200">
            <img src={`/icons/${theme}/robot/robotInfo.svg`} alt="General Settings" />
            <span className="font-bold text-sm text-white uppercase">General Settings</span>
          </div>
          <div className="grid grid-cols-4 w-full justify-center gap-12 bg-layer-300 p-6">
            <div className="flex flex-col gap-2 items-center">
              <p className="text-white text-sm font-semibold">Teleoperation</p>
              <ToggleOrGo value={true} kind="Teleoperation" link={'link'} />
            </div>
            <div className="flex flex-col gap-2 items-center">
              <p className="text-white text-sm font-semibold">Simulation</p>
              <ToggleOrGo value={false} kind="Simulation" link={'link'} />
            </div>
            <div className="flex flex-col gap-2 items-center">
              <p className="text-white text-sm font-semibold">Development IDE</p>
              <ToggleOrGo value={true} kind="Development IDE" link={'link'} />
            </div>
            <div className="flex flex-col gap-2 items-center">
              <p className="text-white text-sm font-semibold">ROS Bridge Suit</p>
              <ToggleOrGo value={true} kind="ROS Bridge Suit" link={'link'} />
            </div>
          </div>
        </div>

        <div className="flex flex-col border rounded border-layer-600 dark:bg-layer-200  lg:w-full">
          <div className="flex items-center dark:bg-layer-200">
            <img src={`/icons/${theme}/robot/robotInfo.svg`} alt="General Settings" />
            <span className="font-bold text-sm text-white uppercase">General Settings</span>
          </div>
          <div className="grid grid-cols-4 w-full justify-center gap-12 bg-layer-300 p-6">
            <div className="flex flex-col gap-2 items-center">
              <p className="text-white text-sm font-semibold">Teleoperation</p>
              <ToggleOrGo value={true} kind="Teleoperation" link={'link'} />
            </div>
            <div className="flex flex-col gap-2 items-center">
              <p className="text-white text-sm font-semibold">Simulation</p>
              <ToggleOrGo value={false} kind="Simulation" link={'link'} />
            </div>
            <div className="flex flex-col gap-2 items-center">
              <p className="text-white text-sm font-semibold">Development IDE</p>
              <ToggleOrGo value={true} kind="Development IDE" link={'link'} />
            </div>
            <div className="flex flex-col gap-2 items-center">
              <p className="text-white text-sm font-semibold">ROS Bridge Suit</p>
              <ToggleOrGo value={true} kind="ROS Bridge Suit" link={'link'} />
            </div>
          </div>
        </div>
      </div>
    </RobotInnerLayout>
  );
};

export default RobotConfiguration;
