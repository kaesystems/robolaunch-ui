import React from 'react';
import { useParams } from 'react-router-dom';
import RobotInnerLayout from '../../../../../../../../layout/Robot/Robot';

const RobotRosbag = () => {
  const params = useParams();
  return (
    <div>
      <RobotInnerLayout id={params.id}>Robot Rosbag</RobotInnerLayout>
    </div>
  );
};

export default RobotRosbag;
