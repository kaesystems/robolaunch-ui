import React, { useContext } from 'react';
import { useParams } from 'react-router-dom';
import RobotInnerLayout from '../../../../../../../layout/Robot/Robot';
import RobotHero from '../../../../../../../components/Pages/Fleet/Robots/Robot/RobotHero/RobotHero';
import { ThemeContext } from '../../../../../../../context/ThemeContext';

const Robot = () => {
  const params = useParams();
  const { theme, setTheme } = useContext(ThemeContext);

  return (
    <RobotInnerLayout id={params.id}>
      <div className="animate__animated animate__fadeIn">
        {/* Robot Hero Component */}
        <div className="flex justify-center">
          <div className="flex flex-col items-center justify-center w-[80%] border border-light-400 dark:border-layer-600 shadow-md rounded mb-4">
            <div className="flex items-start justify-start w-full bg-light-200 dark:bg-layer-200">
              <div className="flex justify-start items-center w-full">
                <img src={`/icons/${theme}/robot/robotInfo.svg`} alt="info" />
                <p className="text-layer-600 dark:text-light-200 text-sm font-bold uppercase">
                  Robot Info
                </p>
              </div>
            </div>
            <RobotHero />
          </div>
        </div>

        {/* Robot Chart Component */}
        <div className="flex justify-center">
          <div className="flex flex-wrap w-[80%]">
            <div className="m-2 shadow-lg">
              <iframe
                src={`http://54.174.183.235:31294/d-solo/r6lloPJmz/ceph-cluster?orgId=1&from=1656904797450&to=1656947997450&theme=${theme}&panelId=1`}
                width="450"
                height="200"
                frameBorder="0"
              ></iframe>
            </div>
            <div className="m-2 shadow-lg">
              <iframe
                src={`http://54.174.183.235:31294/d-solo/r6lloPJmz/ceph-cluster?orgId=1&refresh=1m&from=1657021472890&to=1657064672890&theme=${theme}&panelId=102`}
                width="450"
                height="200"
                frameBorder="0"
              ></iframe>
            </div>
            <div className="m-2 shadow-lg">
              <iframe
                src={`http://54.174.183.235:31294/d-solo/r6lloPJmz/ceph-cluster?orgId=1&from=1657033151636&to=1657044144002&theme=${theme}&panelId=18`}
                width="450"
                height="200"
                frameBorder="0"
              ></iframe>
            </div>
          </div>
        </div>
      </div>
    </RobotInnerLayout>
  );
};

export default Robot;
