/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable react-hooks/rules-of-hooks */
import React, { useEffect, useState } from 'react';
import Stream from '../../../../../../../../components/Pages/Fleet/Robots/Robot/VDI/Stream/Stream';

const RobotVDI = () => {
  const [ip, setIp] = useState('3.127.135.6');
  const [port, setPort] = useState(31011);
  return <Stream port={port} ip={ip} />;
};

export default RobotVDI;
