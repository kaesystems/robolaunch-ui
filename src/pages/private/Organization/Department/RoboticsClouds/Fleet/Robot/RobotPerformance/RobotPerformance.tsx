import React from 'react';
import { useParams } from 'react-router-dom';
import RobotInnerLayout from '../../../../../../../../layout/Robot/Robot';

const RobotPerformance = () => {
  const params = useParams();
  return (
    <div>
      <RobotInnerLayout id={params.id}>Robot Performance</RobotInnerLayout>
    </div>
  );
};

export default RobotPerformance;
