import { Field, Form, Formik } from 'formik';
import React, { useState } from 'react';
import Button from '../../../../../../../components/Button/Button';
import InputField from '../../../../../../../components/Form/InputField/InputField';

const CreateRobot = () => {
  const [isRobotCount, setIsRobotCount] = useState(0);

  return (
    <div className="grid grid-cols-6 gap-10 w-[80vw] pt-24 mx-auto text-white">
      <div className="col-span-1 mx-auto border border-light-400 w-full">Create Robot</div>
      <div className="col-span-5 flex flex-col items-center bg-layer-200 rounded-xl">
        <span className="font-semibold text-md">Create Robot</span>
        <Formik
          initialValues={{
            organizationName: '',
            departmentName: 'department1',
            roboticsCloudName: 'roboticsCloud1',
            fleet: 'fleet1',
            robotCount: isRobotCount
          }}
          onSubmit={(values) => {
            console.log(values);
          }}
        >
          <Form>
            <InputField id="organizationName" info="Select Organization" label="Organizations">
              <Field className="input-select" name="organizationName" as="select">
                <option value="organization1">Organization 1</option>
                <option value="organization2">Organization 2</option>
              </Field>
            </InputField>
            <InputField id="departmentName" info="Select Department" label="Departments">
              <Field className="input-select" name="departmentName" as="select">
                <option value="department1">Department 1</option>
                <option value="department2">Department 2</option>
              </Field>
            </InputField>
            <InputField id="regionName" info="Select Region" label="Regions">
              <Field className="input-select" name="regionName" as="select">
                <option value="region1">Region 1</option>
                <option value="region2">Region 2</option>
              </Field>
            </InputField>
            <InputField id="roboticsCloudName" info="Select Robotics Cloud" label="Robotics Clouds">
              <Field className="input-select" name="roboticsCloudName" as="select">
                <option value="roboticsCloud1">Robotics Cloud 1</option>
                <option value="roboticsCloud2">Robotics Cloud 2</option>
              </Field>
            </InputField>
            <InputField id="fleetName" info="Select Fleet" label="Fleets">
              <Field className="input-select" name="fleetName" as="select">
                <option value="fleet1">Fleet 1</option>
                <option value="fleet2">Fleet 2</option>
              </Field>
            </InputField>
            <InputField id="robotCount" info="Select Robot Count" label="Robot Count">
              <input
                className="input-select"
                name="robotCount"
                type="number"
                value={isRobotCount}
                onChange={(e) => setIsRobotCount(Number(e.target.value))}
              />
            </InputField>
            <Button disabled={false} style="primary" type={'submit'} />
          </Form>
        </Formik>
      </div>
    </div>
  );
};

export default CreateRobot;
