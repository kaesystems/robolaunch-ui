import React, { useState } from 'react';
import Button from '../../../components/Button/Button';
import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';

const Dashboard = () => {
  return (
    <div className="dashboard  animate__animated animate__fadeIn">
      <div className="w-full p-8 rounded-2xl text-center border drop-shadow-xl bg-light-200 dark:bg-layer-200 border-light-300 dark:border-layer-300  text-dark  dark:text-white">
        <div className="relative flex justify-center items-center mx-auto w-32 h-auto">
          <img className="absolute w-16" src="logo/logo.svg" alt="Robolaunch" />
          <img className="animate-spin" src="logo/ring.svg" alt="Robolaunch" />
        </div>
        <h2 className="text-center font-semibold">Welcome to robolaunch! 🎊</h2>
        <div className="flex justify-center items-center mx-auto w-14 font-bold text-md border border-light-100 p-2 rounded-full ">
          1
        </div>
        <p className="w-[64%] mx-auto text-sm pt-4">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam cum, consequuntur quas
          magni earum commodi totam voluptates ad neque quo harum soluta commodi totam voluptates ad
          neque quo harum soluta dolor cumque amet placeat?
        </p>
        <div className="flex flex-col items-center pt-8 gap-6">
          <Button
            type={'submit'}
            disabled={false}
            style="primary"
            text="Create First Organization"
          />
        </div>
      </div>
      <div className="grid grid-cols-2 py-4 gap-4">
        <div className="w-full p-8 rounded-2xl text-center border drop-shadow-xl bg-light-200 dark:bg-layer-200 border-light-300 dark:border-layer-300  text-dark  dark:text-white">
          <h4 className="text-center text-semibold">Create First Cloud Instance</h4>
          <div className="flex justify-center items-center mx-auto w-14 font-bold text-md border border-light-100 p-2 rounded-full ">
            2
          </div>
          <p className="w-[64%] mx-auto text-sm pt-4">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta commodi totam
            voluptates ad nequet placeat?
          </p>
          <div className="flex flex-col items-center pt-8 gap-6">
            <Button
              type={'submit'}
              disabled={false}
              style="primary"
              text="Create First Cloud Instance"
            />
          </div>
        </div>
        <div className="w-full p-8 rounded-2xl text-center border drop-shadow-xl bg-light-200 dark:bg-layer-200 border-light-300 dark:border-layer-300  text-dark  dark:text-white">
          <h4 className="text-center text-semibold">Deploy Robot!</h4>
          <div className="flex justify-center items-center mx-auto w-14 font-bold text-md border border-light-100 p-2 rounded-full ">
            3
          </div>
          <p className="w-[64%] mx-auto text-sm pt-4">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta commodi totam
            voluptates ad nequet placeat?
          </p>
          <div className="flex flex-col items-center pt-8 gap-6">
            <Button type={'submit'} disabled={false} style="primary" text="Deploy Robot" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
