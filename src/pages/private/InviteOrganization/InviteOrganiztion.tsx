import React, { useEffect } from 'react';
import Button from '../../../components/Button/Button';
import { useSearchParams } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../../hooks/redux';
import 'animate.css';
import {
  clearStateInvitedUserAccepted,
  invitedUserAccepted,
  invitedUserRejected
} from '../../../store/features/organization/organizationSlice';
import Loader from '../../../components/Loader/Loader';
import { getCurrentUser } from '../../../store/features/user/userSlice';

const InviteOrganization = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const dispatch = useAppDispatch();
  const {
    isFetchingInvitedUserAccepted,
    isFetchingInvitedUserRejected,
    isSuccessInvitedUserAccepted
  } = useAppSelector((state) => state.organization);
  const { currentUser } = useAppSelector((state) => state.user);

  useEffect(() => {
    if (searchParams.get('email') != currentUser.email) {
      window.location.href = '/';
    }

    if (isSuccessInvitedUserAccepted) {
      setTimeout(() => {
        () => {
          dispatch(clearStateInvitedUserAccepted());
          window.location.href = '/dashboard';
        };
      }, 3000);
    }
  }, [isFetchingInvitedUserAccepted]);

  return (
    <div className="w-screen h-screen flex justify-center items-center robolaunch-bg animate__animated animate__fadeIn">
      <div className="flex flex-col justify-center rounded-2xl border drop-shadow-xl text-center p-8 bg-light-200 dark:bg-layer-200 border-light-300 dark:border-layer-300 gap-8">
        <img className="w-48 h-auto mx-auto" src="/logo/plain.svg" alt="" />
        <h4 className="text-white font-semibold text-md my-2">Invite Organization</h4>
        <span className="text-lowContrast font-semibold">{searchParams.get('email')}</span>
        <p className="text-lowContrast w-[50%] mx-auto">
          Hello! You have received an invitation from organization
          <span className="font-semibold"> {searchParams.get('organization')}</span>. Would you like
          to be involved in this organization?
        </p>
        <div className="flex w-[80%] mx-auto justify-center gap-20 py-4">
          {isFetchingInvitedUserAccepted || isFetchingInvitedUserRejected ? (
            <Loader />
          ) : (
            <>
              <div
                onClick={() => {
                  dispatch(
                    invitedUserAccepted({
                      organization: {
                        name: searchParams.get('organization')
                      },
                      user: {
                        email: searchParams.get('email')
                      },
                      token: searchParams.get('token')
                    })
                  );
                }}
              >
                <Button disabled={false} style={'green'} type={'submit'} text={'Accept'} />
              </div>
              <div
                onClick={() => {
                  dispatch(
                    invitedUserRejected({
                      organization: {
                        name: searchParams.get('organization')
                      },
                      user: {
                        email: searchParams.get('email')
                      },
                      token: searchParams.get('token')
                    })
                  );
                }}
              >
                <Button disabled={false} style={'delete'} type={'submit'} text={'Reject'} />
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
};
export default InviteOrganization;
