import React, { useContext } from 'react';
import { ThemeContext } from '../../../context/ThemeContext';
import { Link } from 'react-router-dom';
import { useLocation } from 'react-router';
import slugify from 'react-slugify';
import path from 'path';

function UserRoleManagementLayout({ children }) {
  const { theme } = useContext(ThemeContext);
  const url = useLocation();

  console.log('a', url.pathname.split('/')[3]);

  const locationpath = () => {
    if (url.pathname.split('/')[3] == 'users') return 'users';
    if (url.pathname.split('/')[3] == 'departments') return 'departments';
    if (url.pathname.split('/')[3] == 'invited-users') return 'invited-users';
  };
  const mockCurrentOrg = 'Organization1';

  return (
    <section>
      <div className="flex gap-x-8">
        <div className="w-64 shadow-md bg-light-200 dark:bg-layer-200 rounded border border-light-300 dark:border-layer-600 cursor-pointer transition-all h-fit p-3">
          <ul className="flex flex-col gap-y-2">
            <Link to={`/${slugify(mockCurrentOrg)}/user-role-management/users`}>
              <li
                className={`flex items-center text-sm hover:bg-light-300 hover:rounded dark:hover:bg-layer-400 p-1 transition-al dark:text-light-100 ${
                  locationpath() == 'users' ? 'rounded bg-light-300 dark:bg-layer-400' : ''
                } `}
              >
                <img className="w-9" src={`/icons/${theme}/users.svg`} alt="Users" />
                <span>Users</span>
              </li>
            </Link>
            <Link to={`/${slugify(mockCurrentOrg)}/user-role-management/departments`}>
              <li
                className={`flex items-center text-sm hover:bg-light-300 hover:rounded dark:hover:bg-layer-400 p-1 transition-al dark:text-light-100 ${
                  locationpath() == 'departments' ? 'rounded bg-light-300 dark:bg-layer-400' : ''
                } `}
              >
                <img className="w-9" src={`/icons/${theme}/departments.svg`} alt="Departments" />
                <span>Departments</span>
              </li>
            </Link>
            <Link to={`/${slugify(mockCurrentOrg)}/user-role-management/invited-users`}>
              <li
                className={`flex items-center text-sm hover:bg-light-300 hover:rounded dark:hover:bg-layer-400 p-1 transition-al dark:text-light-100 ${
                  locationpath() == 'invited-users' ? 'rounded bg-light-300 dark:bg-layer-400' : ''
                } `}
              >
                <img className="w-9" src={`/icons/${theme}/departments.svg`} alt="Departments" />
                <span>Invited Users</span>
              </li>
            </Link>
          </ul>
        </div>
        <div className="flex-auto p-10 bg-light-200 dark:bg-layer-200 rounded border shadow-md border-light-300 dark:border-layer-600 transition-all  animate__animated animate__fadeIn">
          {children}
        </div>
      </div>
    </section>
  );
}
export default UserRoleManagementLayout;
