import React, { useEffect, useState, useMemo } from 'react';
import { useTable, useSortBy, useFilters, usePagination } from 'react-table';
import Table from '../../../../components/URMTable/Table';
import ReactModal from 'react-modal';
import { modalStyles } from '../../../../utils/css/modalStyles';
import { useAppDispatch, useAppSelector } from '../../../../hooks/redux';
import {
  clearState,
  clearStateCreateDepartment,
  clearStateRenameDepartment,
  clearStateDeleteDepartment,
  getDepartments
} from '../../../../store/features/department/departmentSlice';
import UserRoleManagementLayout from '../UserRoleManagementLayout';
import CreateDepartmentModal from '../Modals/CreateDepartmentModal';
import DepartmentName from '../TablesOptions/DepartmentName';

const Departments = () => {
  const [filterInputGroup, setFilterInputGroup] = useState('');
  const [isDepartmentCreateModal, setIsDepartmentCreateModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useAppDispatch();
  const { currentOrganization } = useAppSelector((state) => state.organization);
  const {
    departments,
    isSuccessCreateDepartment,
    isSuccessDeleteDepartment,
    isSuccessRenameDepartment
  } = useAppSelector((state) => state.department);

  const handleDepartmentCreateModal = () => {
    setIsDepartmentCreateModal(!isDepartmentCreateModal);
  };
  const handleFilterChangeDepartment = (e) => {
    const value = e.target.value || undefined;
    tableInstanceDepartment.setFilter('name', value);
    setFilterInputGroup(value);
  };

  useEffect(() => {
    setIsLoading(true);
    dispatch(clearStateCreateDepartment());
    dispatch(clearStateRenameDepartment());
    dispatch(clearStateDeleteDepartment());
    dispatch(clearState());
    (async () => {
      await dispatch(getDepartments(currentOrganization.name));
      setIsLoading(false);
    })();
  }, []);

  useEffect(() => {
    if (isSuccessCreateDepartment || isSuccessRenameDepartment || isSuccessDeleteDepartment) {
      setIsLoading(true);
      dispatch(clearStateCreateDepartment());
      dispatch(clearStateRenameDepartment());
      dispatch(clearStateDeleteDepartment());
      dispatch(clearState());
      (async () => {
        await dispatch(getDepartments(currentOrganization.name));
        setIsLoading(false);
      })();
    }
  }, [isSuccessCreateDepartment, isSuccessRenameDepartment, isSuccessDeleteDepartment]);

  const dataDepartment = useMemo(() => {
    if (departments?.departments?.length > 0) {
      return departments.departments.map((department) => {
        return {
          name: department.name,
          users: department.users.length,
          organization: currentOrganization.name
        };
      });
    } else {
      return [];
    }
  }, [departments, currentOrganization]);

  const columnDepartment = useMemo(
    () => [
      {
        Header: 'Department Name',
        accessor: 'name',
        Cell: ({ cell: { value } }) => <DepartmentName value={value} />
      },
      {
        Header: 'Members',
        accessor: 'users'
      },
      {
        Header: 'Organization',
        accessor: 'organization'
      }
    ],
    []
  );
  const tableInstanceDepartment = useTable(
    {
      columns: columnDepartment,
      data: dataDepartment
    },
    useFilters,
    useSortBy,
    usePagination
  );

  return (
    <UserRoleManagementLayout>
      <h3 className="text-base font-semibold m-0 pb-10 dark:text-light-100">
        Organization Departments
      </h3>
      <div className="grid grid-cols-2 gap-6 text-sm w-[100%] pt-4">
        <div className="col-span-2">
          <Table
            createType="button"
            filterInput={filterInputGroup}
            handleFilterChange={handleFilterChangeDepartment}
            loading={isLoading}
            placeholderSearch="Search Department"
            tableInstance={tableInstanceDepartment}
            title="organization departments"
            buttonTitle="Create Department"
            handleOpenModal={handleDepartmentCreateModal}
            tableType="departments"
          />
        </div>
      </div>
      <ReactModal
        isOpen={isDepartmentCreateModal}
        onRequestClose={handleDepartmentCreateModal}
        shouldCloseOnOverlayClick={false}
        style={modalStyles}
        contentLabel="Create Department"
      >
        <CreateDepartmentModal handleClose={handleDepartmentCreateModal} />
      </ReactModal>
    </UserRoleManagementLayout>
  );
};

export default Departments;
