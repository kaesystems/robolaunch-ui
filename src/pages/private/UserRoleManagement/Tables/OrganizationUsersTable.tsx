import React, { useEffect, useState, useMemo } from 'react';
import { useTable, useSortBy, useFilters, usePagination } from 'react-table';
import Table from '../../../../components/URMTable/Table';
import UserRoleManagementLayout from '../UserRoleManagementLayout';
import { useAppDispatch, useAppSelector } from '../../../../hooks/redux';
import {
  clearState,
  clearStateInviteUser,
  clearStateDeleteUserFromOrganization,
  getOrganizationUsers
} from '../../../../store/features/organization/organizationSlice';
import ReactModal from 'react-modal';
import { modalStyles } from '../../../../utils/css/modalStyles';
import InviteUser from '../Modals/InviteUserToOrganizationModal';

interface IOrganizationUsers {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  admin: boolean;
}

const Users = () => {
  const [isShowInviteUserModal, setIsShowInviteUserModal] = useState(false);
  const [organizationUsers, setOrganizationUsers] = useState<IOrganizationUsers[]>();
  const [isLoading, setIsLoading] = useState(false);
  const [filterInputUser, setFilterInputUser] = useState('');
  const dispatch = useAppDispatch();
  const {
    currentOrganization,
    isSuccessInviteUserToOrganization,
    isSuccessDeleteUserFromOrganization
  } = useAppSelector((state) => state.organization);

  const handleFilterChangeUser = (e) => {
    const value = e.target.value || undefined;
    tableInstanceUser.setFilter('username', value);
    setFilterInputUser(value);
  };

  const handleShowInviteUserModal = () => {
    setIsShowInviteUserModal(!isShowInviteUserModal);
  };

  useEffect(() => {
    dispatch(clearStateInviteUser());
    dispatch(clearStateDeleteUserFromOrganization());
    (async () => {
      setIsLoading(true);
      await dispatch(getOrganizationUsers(currentOrganization.name)).then((res: any) => {
        setOrganizationUsers(res.payload.data.users);
      });
      setIsLoading(false);
    })();
    dispatch(clearState());
  }, []);

  useEffect(() => {
    if (isSuccessInviteUserToOrganization || isSuccessDeleteUserFromOrganization) {
      dispatch(clearStateInviteUser());
      dispatch(clearStateDeleteUserFromOrganization());
      (async () => {
        setIsLoading(true);
        await dispatch(getOrganizationUsers(currentOrganization.name)).then((res: any) => {
          setOrganizationUsers(res.payload.data.users);
        });
        setIsLoading(false);
      })();
      dispatch(clearState());
    }
  }, [isSuccessDeleteUserFromOrganization, isSuccessInviteUserToOrganization]);

  const dataUser = useMemo(() => {
    if (organizationUsers?.length > 0) {
      return organizationUsers.map((user) => {
        return {
          admin: user.admin ? 'Organization Manager' : 'Organization User',
          email: user.email,
          username: user.username,
          firstName: user.firstName,
          lastName: user.lastName,
          fullName: user.firstName + ' ' + user.lastName
        };
      });
    } else {
      return [];
    }
  }, [currentOrganization, organizationUsers, isSuccessInviteUserToOrganization]);

  const columnUser = useMemo(
    () => [
      {
        Header: 'Username',
        accessor: 'username'
      },
      {
        Header: 'Full Name',
        accessor: 'fullName'
      },
      {
        Header: 'Email',
        accessor: 'email'
      },
      {
        Header: 'Organization Member Type',
        accessor: 'admin'
      }
    ],
    []
  );
  const tableInstanceUser = useTable(
    {
      columns: columnUser,
      data: dataUser
    },
    useFilters,
    useSortBy,
    usePagination
  );
  return (
    <UserRoleManagementLayout>
      <>
        <h3 className="text-base font-semibold m-0 pb-10 dark:text-light-100">
          Organization Users
        </h3>
        <div className="grid grid-cols-2 gap-6 text-sm w-[100%] pt-4">
          <div className="col-span-2">
            <Table
              createType="button"
              filterInput={filterInputUser}
              handleFilterChange={handleFilterChangeUser}
              loading={isLoading}
              placeholderSearch="Search User"
              tableInstance={tableInstanceUser}
              title="organization users"
              buttonTitle="Invite User"
              handleOpenModal={handleShowInviteUserModal}
              tableType="users"
            />
          </div>
        </div>
        <ReactModal
          isOpen={isShowInviteUserModal}
          onRequestClose={handleShowInviteUserModal}
          shouldCloseOnOverlayClick={false}
          style={modalStyles}
          contentLabel="Create Department"
        >
          <InviteUser handleClose={handleShowInviteUserModal} />
        </ReactModal>
      </>
    </UserRoleManagementLayout>
  );
};

export default Users;
