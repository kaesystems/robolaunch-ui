import React, { useEffect, useMemo, useState } from 'react';
import ReactModal from 'react-modal';
import { Link, useParams } from 'react-router-dom';
import { useTable, useSortBy, useFilters, usePagination } from 'react-table';
import Table from '../../../../components/URMTable/Table';
import { useAppDispatch, useAppSelector } from '../../../../hooks/redux';
import {
  getDepartmentUsers,
  clearStateDeleteUserFromDepartment,
  clearStateAddUserToDepartment,
  clearStateDeleteUserManagershipFromDepartment,
  clearStateAddUserToDepartmentAsManager
} from '../../../../store/features/department/departmentSlice';
import { modalStyles } from '../../../../utils/css/modalStyles';
import UserRoleManagementLayout from '../UserRoleManagementLayout';
import AddUserToDepartmentModal from '../Modals/AddUserToDepartmentModal';

function Department() {
  const params = useParams();
  const [isUserAddShowModal, setIsUserAddShowModal] = useState(false);
  const [filterInputUser, setFilterInputUser] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [departmentUsers, setDepartmentUsers] = useState([]);
  const dispatch = useAppDispatch();
  const { currentOrganization } = useAppSelector((state) => state.organization);

  const {
    isSuccessAddUserToDepartment,
    isSuccessDeleteUserFromDepartment,
    isSuccessDeleteUserManagershipFromDepartment,
    isSuccessAddUserToDepartmentAsManager
  } = useAppSelector((state) => state.department);

  const handleUserAddShowModal = () => {
    setIsUserAddShowModal(!isUserAddShowModal);
  };

  const handleFilterChangeUser = (e) => {
    const value = e.target.value || undefined;
    tableInstanceDepartment.setFilter('fullName', value);
    setFilterInputUser(value);
  };

  useEffect(() => {
    setIsLoading(true);
    dispatch(clearStateDeleteUserManagershipFromDepartment());
    dispatch(clearStateAddUserToDepartment());
    dispatch(clearStateAddUserToDepartmentAsManager());
    dispatch(clearStateDeleteUserFromDepartment());
    (async () => {
      await dispatch(
        getDepartmentUsers({ organization: currentOrganization.name, department: params.id })
      ).then((res: any) => {
        setDepartmentUsers(res.payload.data.users);
      });
      setIsLoading(false);
    })();
  }, [
    isSuccessDeleteUserFromDepartment,
    isSuccessAddUserToDepartment,
    isSuccessDeleteUserManagershipFromDepartment,
    isSuccessAddUserToDepartmentAsManager
  ]);

  useEffect(() => {
    if (
      isSuccessAddUserToDepartment ||
      isSuccessAddUserToDepartment ||
      isSuccessAddUserToDepartmentAsManager ||
      isSuccessDeleteUserManagershipFromDepartment
    ) {
      setIsLoading(true);
      dispatch(clearStateDeleteUserManagershipFromDepartment());
      dispatch(clearStateAddUserToDepartment());
      dispatch(clearStateAddUserToDepartmentAsManager());
      dispatch(clearStateDeleteUserFromDepartment());
      (async () => {
        await dispatch(
          getDepartmentUsers({ organization: currentOrganization.name, department: params.id })
        ).then((res: any) => {
          setDepartmentUsers(res.payload.data.users);
        });
        setIsLoading(false);
      })();
    }
  }, [
    isSuccessDeleteUserFromDepartment,
    isSuccessAddUserToDepartment,
    isSuccessDeleteUserManagershipFromDepartment,
    isSuccessAddUserToDepartmentAsManager
  ]);

  const columnUser = useMemo(
    () => [
      {
        Header: 'Username',
        accessor: 'username'
      },
      {
        Header: 'Full Name',
        accessor: 'fullName'
      },
      {
        Header: 'Email',
        accessor: 'email'
      },
      {
        Header: 'Department Member Type',
        accessor: 'admin'
      }
    ],
    []
  );
  const dataUser = useMemo(() => {
    if (departmentUsers?.length > 0) {
      return departmentUsers.map((user) => {
        return {
          admin: user.admin ? 'Department Manager' : 'Department User',
          email: user.email,
          username: user.username,
          firstName: user.firstName,
          lastName: user.lastName,
          fullName: user.firstName + ' ' + user.lastName
        };
      });
    } else {
      return [];
    }
  }, [departmentUsers]);

  const tableInstanceDepartment = useTable(
    {
      columns: columnUser,
      data: dataUser
    },
    useFilters,
    useSortBy,
    usePagination
  );

  return (
    <UserRoleManagementLayout>
      <h3 className="font-semibold text-base m-0 pb-10 underline dark:text-light-100">
        <Link to={'/user-role-management/departments'}>Departments</Link> &gt; {params.id}
      </h3>

      <div className="grid grid-cols-2 gap-6 text-sm w-[100%] pt-4">
        <div className="col-span-2">
          <Table
            createType="button"
            filterInput={filterInputUser}
            handleFilterChange={handleFilterChangeUser}
            loading={isLoading}
            placeholderSearch="Search User"
            tableInstance={tableInstanceDepartment}
            title="users"
            buttonTitle="Add User"
            handleOpenModal={handleUserAddShowModal}
            tableType="departmentUsers"
          />
        </div>
        <ReactModal
          isOpen={isUserAddShowModal}
          onRequestClose={handleUserAddShowModal}
          shouldCloseOnOverlayClick={false}
          style={modalStyles}
          contentLabel="Add User"
        >
          <AddUserToDepartmentModal
            department={params.id}
            departmentUsers={departmentUsers}
            handleClose={handleUserAddShowModal}
          />
        </ReactModal>
      </div>
    </UserRoleManagementLayout>
  );
}
export default Department;
