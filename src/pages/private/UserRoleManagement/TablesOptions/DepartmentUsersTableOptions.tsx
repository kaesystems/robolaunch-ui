import React, { useState, useRef, useEffect, useContext } from 'react';
import ReactModal from 'react-modal';
import { ThemeContext } from '../../../../context/ThemeContext';
import { modalStyles } from '../../../../utils/css/modalStyles';
import DeleteUserFromDepartmentModal from '../Modals/DeleteUserFromDepartmentModal';
import AddUserToDepartmentAsManagerModal from '../Modals/AddUserToDepartmentAsManagerModal';
import DeleteUserManagershipFromDepartmentModal from '../Modals/DeleteUserManagershipFromDepartmentModal';

const DepartmentUsersTableOptions = (rowValue) => {
  const ref = useRef<HTMLUListElement>();
  const [isOptionsOpen, setIsOptionsOpen] = useState(false);
  const [confirmDeleteModal, setConfirmDeleteModal] = useState(false);
  const [isShowAddUserToDepartmentModal, setIsShowAddUserToDepartmentModal] = useState(false);
  const [
    isShowDeleteUserManagershipFromDepartmentModal,
    setIsShowDeleteUserManagershipFromDepartmentModal
  ] = useState(false);

  const { theme } = useContext(ThemeContext);

  const handleToggleOptions = (e) => {
    e.stopPropagation();
    setIsOptionsOpen(!isOptionsOpen);
  };
  useEffect(() => {
    const checkIfClickedOutside = (e) => {
      if (isOptionsOpen && ref.current && !ref.current.contains(e.target)) {
        setIsOptionsOpen(false);
      }
    };

    document.addEventListener('mousedown', checkIfClickedOutside);

    return () => {
      document.removeEventListener('mousedown', checkIfClickedOutside);
    };
  }, [isOptionsOpen]);

  const handleDeleteModal = () => {
    setConfirmDeleteModal(!confirmDeleteModal);
  };

  const handleShowAddUserToDepartmentAsManagerModal = () => {
    setIsShowAddUserToDepartmentModal(!isShowAddUserToDepartmentModal);
  };

  const handleShowDeleteUserManagershipFromDepartmentModal = () => {
    setIsShowDeleteUserManagershipFromDepartmentModal(
      !isShowDeleteUserManagershipFromDepartmentModal
    );
  };

  return (
    <td className="w-[2rem]">
      <div className="relative">
        <div onClick={handleToggleOptions} className="cursor-pointer">
          <img src={`/icons/${theme}/dots.svg`} />
        </div>
        {isOptionsOpen && (
          <ul
            ref={ref}
            className="absolute right-6 w-56 border border-light-400 dark:border-layer-600 rounded bg-light-200 dark:bg-layer-300"
          >
            {rowValue.rowValue.admin == 'Department User' ? (
              <li
                onClick={() => {
                  handleShowAddUserToDepartmentAsManagerModal();
                }}
                className="flex items-center font-semibold hover:bg-light-300 dark:hover:bg-layer-400 p-3 cursor-pointer dark:text-white"
              >
                <img src={`/icons/${theme}/upgrade.svg`} className="mr-2" />
                <p>Assign to Manager</p>
              </li>
            ) : (
              <li
                onClick={() => {
                  handleShowDeleteUserManagershipFromDepartmentModal();
                }}
                className="flex items-center font-semibold hover:bg-light-300 dark:hover:bg-layer-400 p-3 cursor-pointer dark:text-white"
              >
                <img src={`/icons/${theme}/downgrade.svg`} className="mr-2" />
                <p>Unassign to Manager</p>
              </li>
            )}
            <li
              onClick={() => {
                handleDeleteModal();
              }}
              className="flex items-center bg-red-100 hover:bg-red-200 text-white font-semibold p-3 cursor-pointer"
            >
              <img src="/icons/trash.svg" className="mr-2" />
              <p>Delete from Department</p>
            </li>
          </ul>
        )}
        <ReactModal
          isOpen={isShowAddUserToDepartmentModal}
          onRequestClose={handleShowAddUserToDepartmentAsManagerModal}
          shouldCloseOnOverlayClick={true}
          style={modalStyles}
          contentLabel="Assign Manager From Department"
        >
          <AddUserToDepartmentAsManagerModal
            row={rowValue.rowValue}
            handleClose={handleShowAddUserToDepartmentAsManagerModal}
          />
        </ReactModal>
        <ReactModal
          isOpen={isShowDeleteUserManagershipFromDepartmentModal}
          onRequestClose={handleShowDeleteUserManagershipFromDepartmentModal}
          shouldCloseOnOverlayClick={true}
          style={modalStyles}
          contentLabel="Unassign Manager From Department"
        >
          <DeleteUserManagershipFromDepartmentModal
            row={rowValue.rowValue}
            handleClose={handleShowDeleteUserManagershipFromDepartmentModal}
          />
        </ReactModal>
        <ReactModal
          isOpen={confirmDeleteModal}
          onRequestClose={handleDeleteModal}
          shouldCloseOnOverlayClick={true}
          style={modalStyles}
          contentLabel="Delete Department"
        >
          <DeleteUserFromDepartmentModal row={rowValue.rowValue} handleClose={handleDeleteModal} />
        </ReactModal>
      </div>
    </td>
  );
};

export default DepartmentUsersTableOptions;
