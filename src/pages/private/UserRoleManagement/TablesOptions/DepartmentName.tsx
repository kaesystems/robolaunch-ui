import React from 'react';
import { Link } from 'react-router-dom';

const DepartmentName = ({ value }) => {
  return (
    <div>
      <Link className="underline text-sm" to={`/user-role-management/department/${value}`}>
        {value}
      </Link>
    </div>
  );
};

export default DepartmentName;
