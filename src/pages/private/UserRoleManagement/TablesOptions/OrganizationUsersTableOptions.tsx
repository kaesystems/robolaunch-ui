import React, { useState, useRef, useEffect, useContext } from 'react';
import ReactModal from 'react-modal';
import { ThemeContext } from '../../../../context/ThemeContext';
import { modalStyles } from '../../../../utils/css/modalStyles';
import DeleteUserFromOrganizationModal from '../Modals/DeleteUserFromOrganizationModal';

const UserTableOptions = (rowValue) => {
  const ref = useRef<HTMLUListElement>();
  const [isOptionsOpen, setIsOptionsOpen] = useState(false);
  const { theme } = useContext(ThemeContext);
  const [isShowDeleteModal, setIsShowDeleteModal] = useState(false);

  const [isEditOpen, setIsEditOpen] = useState(false);

  const handleToggleOptions = (e) => {
    e.stopPropagation();
    setIsOptionsOpen(!isOptionsOpen);
  };

  const handleShowDeleteModal = () => {
    setIsShowDeleteModal(!isShowDeleteModal);
  };

  const handleCloseEditModal = () => {
    setIsEditOpen(!isEditOpen);
  };

  useEffect(() => {
    const checkIfClickedOutside = (e) => {
      if (isOptionsOpen && ref.current && !ref.current.contains(e.target)) {
        setIsOptionsOpen(false);
      }
    };

    document.addEventListener('mousedown', checkIfClickedOutside);

    return () => {
      document.removeEventListener('mousedown', checkIfClickedOutside);
    };
  }, [isOptionsOpen]);
  return (
    <td className="w-[2rem]">
      <div className="relative">
        <div onClick={handleToggleOptions} className="cursor-pointer">
          <img src={`/icons/${theme}/dots.svg`} />
        </div>
        {isOptionsOpen && (
          <ul
            ref={ref}
            className="absolute top-100 right-full w-[120px] border border-light-400 dark:border-layer-600 rounded bg-light-200 dark:bg-layer-300"
          >
            <li
              onClick={() => {
                console.log(rowValue);
              }}
              className="flex items-center font-semibold hover:bg-light-300 dark:hover:bg-layer-400 p-3 cursor-pointer dark:text-light-100"
            >
              <img src={`/icons/${theme}/robot/edit.svg`} className="mr-2" />
              <p>Details</p>
            </li>
            <li
              onClick={() => handleShowDeleteModal()}
              className="flex items-center bg-red-100 hover:bg-red-200 text-white font-semibold p-3 cursor-pointer"
            >
              <img src="/icons/trash.svg" className="mr-2" />
              <p>Delete</p>
            </li>
          </ul>
        )}
        <ReactModal
          isOpen={false}
          onRequestClose={handleCloseEditModal}
          shouldCloseOnOverlayClick={true}
          style={modalStyles}
          contentLabel="Edit User"
        >
          <div>asd</div>
        </ReactModal>
        <ReactModal
          isOpen={isShowDeleteModal}
          onRequestClose={handleShowDeleteModal}
          shouldCloseOnOverlayClick={true}
          style={modalStyles}
          contentLabel="Edit Department"
        >
          <DeleteUserFromOrganizationModal
            row={rowValue.rowValue}
            handleClose={handleShowDeleteModal}
          />
        </ReactModal>
      </div>
    </td>
  );
};

export default UserTableOptions;
