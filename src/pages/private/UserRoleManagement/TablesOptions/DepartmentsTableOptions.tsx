import React, { useState, useRef, useEffect, useContext } from 'react';
import ReactModal from 'react-modal';
import { ThemeContext } from '../../../../context/ThemeContext';
import { modalStyles } from '../../../../utils/css/modalStyles';
import DeleteDepartmentModal from '../Modals/DeleteDepartmentModal';
import EditDepartmentModal from '../Modals/EditDepartmentModal';

const DepartmentTableOptions = (rowValue) => {
  const ref = useRef<HTMLUListElement>();
  const [isOptionsOpen, setIsOptionsOpen] = useState(false);
  const [confirmDeleteModal, setConfirmDeleteModal] = useState(false);
  const [isShowEditDepartmentModal, setIsShowEditDepartmentModal] = useState(false);
  const { theme } = useContext(ThemeContext);

  const handleToggleOptions = (e) => {
    e.stopPropagation();
    setIsOptionsOpen(!isOptionsOpen);
  };
  useEffect(() => {
    const checkIfClickedOutside = (e) => {
      if (isOptionsOpen && ref.current && !ref.current.contains(e.target)) {
        setIsOptionsOpen(false);
      }
    };

    document.addEventListener('mousedown', checkIfClickedOutside);

    return () => {
      document.removeEventListener('mousedown', checkIfClickedOutside);
    };
  }, [isOptionsOpen]);

  const handleShowEditModal = () => {
    setIsShowEditDepartmentModal(!isShowEditDepartmentModal);
  };

  const handleDeleteModal = () => {
    setConfirmDeleteModal(!confirmDeleteModal);
  };

  return (
    <td className="w-[2rem]">
      <div className="relative">
        <div onClick={handleToggleOptions} className="cursor-pointer">
          <img src={`/icons/${theme}/dots.svg`} />
        </div>
        {isOptionsOpen && (
          <ul
            ref={ref}
            className="absolute top-100 right-full w-[120px] border border-light-400 dark:border-layer-600 rounded bg-light-200 dark:bg-layer-300"
          >
            <li
              onClick={() => {
                handleShowEditModal();
              }}
              className="flex items-center font-semibold hover:bg-light-300 dark:hover:bg-layer-400 p-3 cursor-pointer dark:text-light-100"
            >
              <img src={`/icons/${theme}/robot/edit.svg`} className="mr-2" />
              <p>Rename</p>
            </li>
            <li
              onClick={() => {
                handleDeleteModal();
              }}
              className="flex items-center bg-red-100 hover:bg-red-200 text-white font-semibold p-3 cursor-pointer"
            >
              <img src="/icons/trash.svg" className="mr-2" />
              <p>Delete</p>
            </li>
          </ul>
        )}
        <ReactModal
          isOpen={confirmDeleteModal}
          onRequestClose={handleDeleteModal}
          shouldCloseOnOverlayClick={true}
          style={modalStyles}
          contentLabel="Delete Department"
        >
          <DeleteDepartmentModal row={rowValue.rowValue} handleClose={handleDeleteModal} />
        </ReactModal>
        <ReactModal
          isOpen={isShowEditDepartmentModal}
          onRequestClose={handleShowEditModal}
          shouldCloseOnOverlayClick={true}
          style={modalStyles}
          contentLabel="Edit Department"
        >
          <EditDepartmentModal row={rowValue.rowValue} handleClose={handleShowEditModal} />
        </ReactModal>
      </div>
    </td>
  );
};

export default DepartmentTableOptions;
