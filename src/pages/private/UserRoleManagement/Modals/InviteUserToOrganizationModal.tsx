import React from 'react';
import Modal from '../../../../components/Modal/Modal';
import { Field, Form, Formik } from 'formik';
import InputField from '../../../../components/Form/InputField/InputField';
import Button from '../../../../components/Button/Button';
import { inviteUser } from '../../../../store/features/organization/organizationSlice';
import { useAppDispatch, useAppSelector } from '../../../../hooks/redux';
import * as Yup from 'yup';

interface IInviteUser {
  handleClose: any;
}

const InviteUser = ({ handleClose }: IInviteUser) => {
  const dispatch = useAppDispatch();
  const { currentOrganization } = useAppSelector((state) => state.organization);

  const InviteUserSchema = Yup.object().shape({
    email: Yup.string().email('Invalid email').required('Email is required')
  });

  return (
    <Modal title="Invite User" handleClose={handleClose}>
      <Formik
        initialValues={{
          email: ''
        }}
        onSubmit={(values) => {
          dispatch(
            inviteUser({
              organization: {
                name: currentOrganization.name
              },
              email: values.email
            })
          );
          handleClose();
        }}
        validationSchema={InviteUserSchema}
      >
        <Form>
          <div className="grid grid-cols-2 gap-3">
            <div className="col-start-1 col-end-3">
              <InputField id="email" label="User Email Address" info="Type User Email Address">
                <Field
                  id="email"
                  name="email"
                  type="text"
                  placeholder="example@domain.com"
                  className="h-input rounded p-2 bg-light-100 dark:bg-layer-200 border border-light-400 dark:border-layer-500 outline-none focus:outline-1 focus:outline-layer-600 focus:border-none"
                  required
                />
              </InputField>
            </div>
          </div>
          <div className="w-full flex justify-center pt-6">
            <Button disabled={false} style="primary" type="submit" text="Invite User"></Button>
          </div>
        </Form>
      </Formik>
    </Modal>
  );
};

export default InviteUser;
