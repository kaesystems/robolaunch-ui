import React from 'react';
import Modal from '../../../../components/Modal/Modal';
import { Field, Form, Formik } from 'formik';
import InputField from '../../../../components/Form/InputField/InputField';
import { useAppDispatch, useAppSelector } from '../../../../hooks/redux';
import Button from '../../../../components/Button/Button';
import { deleteUserFromOrganization } from '../../../../store/features/organization/organizationSlice';

const DeleteUserFromOrganizationModal = ({ handleClose, row }) => {
  const dispatch = useAppDispatch();
  const { currentOrganization } = useAppSelector((state) => state.organization);

  const handleDeleteUserFromOrganization = (value) => {
    dispatch(
      deleteUserFromOrganization({
        organization: { name: currentOrganization.name },
        user: { username: value.username }
      })
    );
    handleClose();
  };

  return (
    <Modal title="Delete User From Organization" handleClose={handleClose}>
      <div>
        <Formik
          initialValues={{ username: '' }}
          onSubmit={(value) => {
            if (row == value.username) {
              handleDeleteUserFromOrganization(value);
            }
          }}
        >
          <Form>
            <div className="flex flex-col gap-6">
              <InputField
                info="Type Username"
                id="username"
                label={'Please Type  ' + row + ' Username'}
              >
                <Field
                  className="h-input rounded p-2 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                  id="username"
                  name="username"
                  type="text"
                  placeholder="e.g Johndoe"
                />
              </InputField>
              <div className="flex justify-center">
                <Button disabled={false} style="delete" type="submit" text="Delete User" />
              </div>
            </div>
          </Form>
        </Formik>
      </div>
    </Modal>
  );
};

export default DeleteUserFromOrganizationModal;
