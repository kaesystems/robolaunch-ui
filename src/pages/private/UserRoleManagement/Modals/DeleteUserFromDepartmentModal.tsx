import React from 'react';
import Modal from '../../../../components/Modal/Modal';
import { Field, Form, Formik } from 'formik';
import InputField from '../../../../components/Form/InputField/InputField';
import { useAppDispatch, useAppSelector } from '../../../../hooks/redux';
import { deleteUserFromDepartment } from '../../../../store/features/department/departmentSlice';
import Button from '../../../../components/Button/Button';
import { useParams } from 'react-router';

const DeleteUserFromDepartment = ({ handleClose, row }) => {
  const param = useParams();
  const dispatch = useAppDispatch();
  const { currentOrganization } = useAppSelector((state) => state.organization);
  return (
    <Modal title="Delete User From Department" handleClose={handleClose}>
      <div>
        <Formik
          initialValues={{ name: '' }}
          onSubmit={(value) => {
            if (row.username == value.name) {
              dispatch(
                deleteUserFromDepartment({
                  organization: currentOrganization.name,
                  username: value.name,
                  department: param.id
                })
              );
              handleClose();
            }
          }}
        >
          <Form>
            <div className="flex flex-col gap-6">
              <InputField
                info="Type Department Name"
                id="name"
                label={'Please Type ' + row.username + ' Username'}
              >
                <Field
                  className="h-input rounded p-2 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                  id="name"
                  name="name"
                  type="text"
                  placeholder="e.g Xorg_Sales"
                />
              </InputField>

              <div className="flex justify-center">
                <Button
                  disabled={false}
                  style="delete"
                  type={'submit'}
                  text="Delete User From Department"
                />
              </div>
            </div>
          </Form>
        </Formik>
      </div>
    </Modal>
  );
};

export default DeleteUserFromDepartment;
