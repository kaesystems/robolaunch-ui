import React from 'react';
import { Field, Form, Formik } from 'formik';
import Button from '../../../../components/Button/Button';
import InputField from '../../../../components/Form/InputField/InputField';
import { createOrganization } from '../../../../store/features/organization/organizationSlice';
import { useAppDispatch } from '../../../../hooks/redux';
import Modal from '../../../../components/Modal/Modal';
import 'animate.css';
import * as Yup from 'yup';

interface ICreateOrganization {
  handleClose: any;
}

const CreateOrganization = ({ handleClose }: ICreateOrganization) => {
  const dispatch = useAppDispatch();

  const CreateOrganizationSchema = Yup.object().shape({
    name: Yup.string()
      .required('Required')
      .min(3, 'Min 3 Character')
      .max(64, 'Max 64 Character')
      .matches(/^\w[a-zA-Z@#0-9.]*$/, 'Only english characters and numbers')
  });

  return (
    <Modal title="Create Organization" handleClose={handleClose}>
      <Formik
        initialValues={{
          name: '',
          isEnterprise: false
        }}
        onSubmit={(value) => {
          dispatch(createOrganization(value));
          handleClose();
        }}
        validationSchema={CreateOrganizationSchema}
      >
        <Form autoComplete="off">
          <div className="grid grid-cols-1 grid-rows-1">
            <div className="col-start-1 col-end-2">
              <p>
                Want to create a new organization? All right, so let&apos;s give the organization a
                new name.
              </p>
              <div className="my-2">
                <InputField id="name" info="name address of the user." label="Name">
                  <Field
                    className="h-input rounded p-2 dark:bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-light-100 focus:ring-1 focus:dark:ring-primary-100 placeholder:italic"
                    id="name"
                    name="name"
                    label="Name"
                    placeholder="e.g MyJobsOrganization"
                  />
                </InputField>
                <InputField id="isEnterprise" info="name address of the user." label="isEnterprise">
                  <Field className="my-1" name="isEnterprise" type="checkbox" />
                </InputField>
              </div>
            </div>
          </div>
          <div className="flex justify-center my-4">
            <Button
              disabled={false}
              style="primary"
              icon="plus"
              type="submit"
              text="Create Organization"
            />
          </div>
        </Form>
      </Formik>
    </Modal>
  );
};

export default CreateOrganization;
