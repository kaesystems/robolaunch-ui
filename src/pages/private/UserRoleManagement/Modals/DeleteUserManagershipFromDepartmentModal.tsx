import React from 'react';
import Modal from '../../../../components/Modal/Modal';
import Button from '../../../../components/Button/Button';
import { deleteUserManagershipFromDepartment } from '../../../../store/features/department/departmentSlice';
import { useAppDispatch, useAppSelector } from '../../../../hooks/redux';
import { useParams } from 'react-router-dom';

interface IDeleteUserManagershipFromDepartmentModal {
  handleClose: any;
  row: any;
}

const DeleteUserManagershipFromDepartmentModal = ({
  row,
  handleClose
}: IDeleteUserManagershipFromDepartmentModal) => {
  const dispatch = useAppDispatch();
  const { currentOrganization } = useAppSelector((state) => state.organization);
  const param = useParams();

  return (
    <Modal title="Unassign Manager From Department" handleClose={handleClose}>
      <div className="pb-3">
        <p>
          Do you want the user named <b>{row.username}</b> to be an user of the department named
          <b> {param.id}</b>?
        </p>
      </div>
      <div className="flex gap-5">
        <div
          onClick={() => {
            handleClose();
          }}
        >
          <Button disabled={false} style="delete" type={'submit'} text="Cancel" />
        </div>
        <div
          onClick={() => {
            dispatch(
              deleteUserManagershipFromDepartment({
                organization: {
                  name: currentOrganization.name
                },
                user: {
                  username: row.username
                },
                department: {
                  name: param.id
                }
              })
            );
            handleClose();
          }}
        >
          <Button disabled={false} style="primary" type={'submit'} text="Accept" />
        </div>
      </div>
    </Modal>
  );
};

export default DeleteUserManagershipFromDepartmentModal;
