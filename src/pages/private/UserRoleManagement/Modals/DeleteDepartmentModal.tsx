import React from 'react';
import Modal from '../../../../components/Modal/Modal';
import { Field, Form, Formik } from 'formik';
import InputField from '../../../../components/Form/InputField/InputField';
import { useAppDispatch, useAppSelector } from '../../../../hooks/redux';
import { deleteDepartments } from '../../../../store/features/department/departmentSlice';
import Button from '../../../../components/Button/Button';

const DeleteDepartment = ({ handleClose, row }) => {
  const dispatch = useAppDispatch();
  const { currentOrganization } = useAppSelector((state) => state.organization);

  const handleDeleteDepartments = (value) => {
    dispatch(deleteDepartments(value));
    handleClose();
  };
  return (
    <Modal title="Delete Department" handleClose={handleClose}>
      <div>
        <Formik
          initialValues={{ departmentName: '' }}
          onSubmit={(value) => {
            if (row == value.departmentName) {
              handleDeleteDepartments({ row, currentOrganization });
            }
          }}
        >
          <Form>
            <div className="flex flex-col gap-6">
              <InputField
                info="Type Department Name"
                id="name"
                label={'Please Type  ' + row + '  Department Name'}
              >
                <Field
                  className="h-input rounded p-2 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                  id="departmentName"
                  name="departmentName"
                  type="text"
                  placeholder="e.g Xorg_Sales"
                />
              </InputField>
              <div className="flex justify-center">
                <Button disabled={false} style="delete" type="submit" text="Delete Department" />
              </div>
            </div>
          </Form>
        </Formik>
      </div>
    </Modal>
  );
};

export default DeleteDepartment;
