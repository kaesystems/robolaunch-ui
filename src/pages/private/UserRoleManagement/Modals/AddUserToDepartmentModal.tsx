import React, { useContext, useEffect, useState } from 'react';
import Modal from '../../../../components/Modal/Modal';
import { Field, Form, Formik } from 'formik';
import { useAppDispatch, useAppSelector } from '../../../../hooks/redux';
import { getOrganizationUsers } from '../../../../store/features/organization/organizationSlice';
import { addUserToDepartment } from '../../../../store/features/department/departmentSlice';
import Button from '../../../../components/Button/Button';
import { ThemeContext } from '../../../../context/ThemeContext';
import Loader from '../../../../components/Loader/Loader';
interface IAddUserToDepartment {
  handleClose: any;
  departmentUsers: any;
  department: any;
}

const AddUserToDepartment = ({
  handleClose,
  departmentUsers,
  department
}: IAddUserToDepartment) => {
  const dispatch = useAppDispatch();
  const { currentOrganization, isFetching } = useAppSelector((state) => state.organization);
  const [organizationUsers, setOrganizationUsers] = useState([]);
  const { theme } = useContext(ThemeContext);

  const [filteredUsers, setFilteredUsers] = useState<any>();

  useEffect(() => {
    setFilteredUsers(
      organizationUsers.filter((organizationUser) => {
        return departmentUsers.every((depuser) => {
          if (depuser.username !== organizationUser.username) {
            return true;
          }
          return false;
        });
      })
    );
  }, [departmentUsers, organizationUsers]);

  useEffect(() => {
    (async () => {
      await dispatch(getOrganizationUsers(currentOrganization.name)).then((res: any) => {
        setOrganizationUsers(res.payload.data.users);
      });
    })();
  }, [currentOrganization.name]);

  const handleAddUserToDepartment = (values) => {
    if (values.selected.length > 0) {
      values.selected.map((user) => {
        dispatch(
          addUserToDepartment({
            organization: currentOrganization.name,
            department: department,
            username: user
          })
        );
      });
    }
    handleClose();
  };

  const handleFilterChangeDepartment = (e) => {
    const value = e.target.value || undefined;
    filteredUsers.setFilter('name', value);
  };

  return (
    <Modal title="Add User To Department" handleClose={handleClose}>
      <div>
        <Formik
          initialValues={{
            selected: []
          }}
          onSubmit={(values) => {
            handleAddUserToDepartment(values);
          }}
        >
          <Form>
            <div>
              <p>Organization Users List</p>
              <div className="my-6 p-2 border rounded-md border-light-400 dark:border-layer-600 overflow-y-auto h-64 w-96">
                {isFetching ? (
                  <>
                    <div className="flex h-full justify-center items-center">
                      <Loader />
                    </div>
                  </>
                ) : (
                  <>
                    {' '}
                    {filteredUsers?.length > 0 ? (
                      <>
                        <label className="relative m-4 text-gray-400 focus-within:text-gray-600 block">
                          <img
                            src={`/icons/${theme}/search.svg`}
                            width="20px"
                            className="pointer-events-none absolute transform translate-y-1/2 translate-x-1/2"
                          />
                          <input
                            type="text"
                            value={''}
                            onChange={handleFilterChangeDepartment}
                            placeholder={'Search'}
                            className="min-w-full pl-10 bg-light-100 dark:bg-layer-100 border border-light-400 dark:border-layer-600 max-w-[200px] p-2 rounded-lg focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 text-layer-100 dark:text-light-300"
                          />
                        </label>
                        {filteredUsers?.map((user, index) => {
                          return (
                            <div
                              className="flex justify-between gap-6 p-2 m-4 rounded-md bg-light-100 dark:bg-layer-100 border border-light-400 dark:border-layer-600"
                              key={index}
                            >
                              <label className="flex gap-2 items-center">
                                <Field type="checkbox" name="selected" value={user.username} />
                                {user.username}
                              </label>
                            </div>
                          );
                        })}
                      </>
                    ) : (
                      <div className="flex h-full justify-center text-center items-center">
                        There are no users in the organization that can be added
                      </div>
                    )}
                  </>
                )}
              </div>
              <div className="flex justify-center">
                <Button disabled={false} style="green" type="submit" text="Add User" />
              </div>
            </div>
          </Form>
        </Formik>
      </div>
    </Modal>
  );
};

export default AddUserToDepartment;
