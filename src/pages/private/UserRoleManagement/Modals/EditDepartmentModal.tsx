import React from 'react';
import Modal from '../../../../components/Modal/Modal';
import { Field, Form, Formik } from 'formik';
import InputField from '../../../../components/Form/InputField/InputField';
import { useAppDispatch, useAppSelector } from '../../../../hooks/redux';
import { renameDepartments } from '../../../../store/features/department/departmentSlice';
import Button from '../../../../components/Button/Button';
import * as Yup from 'yup';

const EditDepartment = ({ handleClose, row }) => {
  const dispatch = useAppDispatch();
  const { currentOrganization } = useAppSelector((state) => state.organization);

  const handleRenameDepartment = (value) => {
    dispatch(renameDepartments({ value, currentOrganization, row }));
    handleClose();
  };

  const EditDepartmentSchema = Yup.object().shape({
    departmentName: Yup.string()
      .required('Required')
      .min(3, 'Min 3 Character')
      .max(64, 'Max 64 Character')
      .matches(/^\w[a-zA-Z@#0-9.]*$/, 'Only English Characters and Numbers')
  });

  return (
    <Modal title="Rename Department" handleClose={handleClose}>
      <div>
        <Formik
          initialValues={{ departmentName: '' }}
          onSubmit={(value) => handleRenameDepartment(value)}
          validationSchema={EditDepartmentSchema}
        >
          <Form>
            <div className="flex flex-col gap-6">
              <InputField
                info="Type New Department Name"
                id="name"
                label={'Please Type New Department Name'}
              >
                <Field
                  className="h-10 rounded p-2 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                  id="departmentName"
                  name="departmentName"
                  type="text"
                  placeholder="e.g Xorg_Sales"
                  required
                />
              </InputField>
              <div className="flex justify-center">
                <Button disabled={false} style="primary" type={'submit'} text="Rename Department" />
              </div>
            </div>
          </Form>
        </Formik>
      </div>
    </Modal>
  );
};

export default EditDepartment;
