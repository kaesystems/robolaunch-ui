import React from 'react';
import Modal from '../../../../components/Modal/Modal';
import { Field, Form, Formik } from 'formik';
import InputField from '../../../../components/Form/InputField/InputField';
import { useAppDispatch, useAppSelector } from '../../../../hooks/redux';
import { createDepartment } from '../../../../store/features/department/departmentSlice';
import Button from '../../../../components/Button/Button';
import * as Yup from 'yup';

interface ICreateUser {
  handleClose: any;
}

const CreateDepartment = ({ handleClose }: ICreateUser) => {
  const dispatch = useAppDispatch();
  const { currentOrganization } = useAppSelector((state) => state.organization);

  const handleCreateDepartment = (value) => {
    dispatch(
      createDepartment({
        organizationName: currentOrganization.name,
        departmentName: value.departmentName
      })
    );
    handleClose();
  };

  const CreateDepartmentSchema = Yup.object().shape({
    departmentName: Yup.string()
      .required('Required')
      .min(3, 'Min 3 Character')
      .max(64, 'Max 64 Character')
      .matches(/^\w[a-zA-Z@#0-9.]*$/, 'Only English Characters and Numbers')
  });

  return (
    <Modal title="Create Department" handleClose={handleClose}>
      <div>
        <Formik
          initialValues={{ departmentName: '' }}
          onSubmit={(value) => handleCreateDepartment(value)}
          validationSchema={CreateDepartmentSchema}
        >
          <Form className="flex flex-col items-center gap-12" action="">
            <InputField id="departmentName" info="Type Department Name" label="Department Name">
              <Field
                className="h-input rounded p-2 bg-layer-100 border border-layer-600 focus:outline-none focus:border-primary-100 focus:ring-1 focus:ring-primary-100 placeholder:italic"
                id="departmentName"
                name="departmentName"
                placeholder="e.g. Sales"
                type="text"
                required
              />
            </InputField>
            <Button
              disabled={false}
              style="green"
              type={'submit'}
              text="Create Department"
            ></Button>
          </Form>
        </Formik>
      </div>
    </Modal>
  );
};

export default CreateDepartment;
