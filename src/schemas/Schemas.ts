import * as Yup from 'yup';

export const teamSchema = Yup.object().shape({
  teamName: Yup.string()
    .required('Required')
    .min(3, 'Min 3 Character')
    .max(64, 'Max 64 Character')
    .matches(/^\w[a-zA-Z@#0-9.]*$/, 'Only English Characters and Numbers')
});

export const fleetSchema = Yup.object().shape({
  fleetName: Yup.string()
    .required('Required')
    .min(3, 'Min 3 Character')
    .max(32, 'Max 32 Character')
    .matches(/^\w[a-zA-Z@#0-9.]*$/, 'Only English Characters and Numbers'),

  fleetType: Yup.string().required('Required')
});

export const emailSchema = Yup.object().shape({
  email: Yup.string().required('Required').email('Invalid Email')
});

export const roboticsCloudSchema = Yup.object().shape({
  organization: Yup.string().required('Required'),
  team: Yup.string().required('Required'),
  region: Yup.string().required('Required'),
  instancetype: Yup.string().required('Required'),
  instancename: Yup.string()
    .required('Required')
    .matches(/^\w[a-zA-Z@#0-9.]*$/, 'Only English Characters and Numbers')
    .min(4, 'Minimum 4 Character')
    .max(16, 'Maximum 16 Character'),
  instancedisksize: Yup.number().required('Required').min(50, 'Minimum 50 GB')
});
