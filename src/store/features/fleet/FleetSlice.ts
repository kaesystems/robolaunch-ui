import { createSlice } from '@reduxjs/toolkit';
import mockFleets from '../../../mock/mockFleets.json';

interface ICurrentFleet {
  organization: number;
  id: number;
  fleetName: string;
  cluster: string;
}

const initialState: ICurrentFleet = mockFleets[0];

export const currentFleetSlice = createSlice({
  name: 'currentFleet',
  initialState: initialState,
  reducers: {
    changeCurrentFleet: (state, action) => {
      state.organization = action.payload.organization;
      state.id = action.payload.id;
      state.fleetName = action.payload.fleetName;
      state.cluster = action.payload.cluster;
    }
  }
});

export const { changeCurrentFleet } = currentFleetSlice.actions;

export default currentFleetSlice.reducer;
