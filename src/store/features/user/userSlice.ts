import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import EnvVariables from '../../../constants/EnvVariables';
import jwt_decode from 'jwt-decode';
import axiosInstance from '../../../utils/axiosInstance';
import { toast } from 'react-toastify';

let id: any;
function toastPush(message) {
  id = toast.loading(message, {
    position: 'bottom-right',
    theme: 'dark'
  });
}

const notificationProperties: any = {
  theme: 'dark',
  position: 'bottom-right',
  isLoading: false,
  autoClose: 5000,
  pauseOnHover: true
};

interface IRegisterUser {
  user: {
    username: string;
    firstName: string;
    lastName: string;
    email: string;
  };
}

export const loginUser = createAsyncThunk('user/login', async (loginRequest: any, thunkAPI) => {
  try {
    localStorage.clear();
    const response = await fetch(`${EnvVariables.BACKEND}/userLogin`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(loginRequest)
    });
    const data = await response.json();
    if (response.status === 201) {
      return data;
    } else {
      return thunkAPI.rejectWithValue(data);
    }
  } catch (error) {
    console.log('Error login! ', error);
  }
});

export const registerUser = createAsyncThunk(
  'user/register',
  async ({ user }: IRegisterUser, thunkAPI) => {
    try {
      const response = await fetch(`${EnvVariables.BACKEND}/userRegistration`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          user
        })
      });
      const data = await response.json();
      if (response.status === 201) {
        return data.user;
      } else {
        return thunkAPI.rejectWithValue(data);
      }
    } catch (e) {
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
);

export const forgotPassword = createAsyncThunk(
  'user/forgotPassword',
  async ({ value }: any, thunkAPI) => {
    try {
      const response = await fetch(`${EnvVariables.BACKEND}/forgotPassword`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ user: { username: value } })
      });

      const data = await response.json();
      if (response.status === 201) {
        return data.user;
      } else {
        return thunkAPI.rejectWithValue(data);
      }
    } catch (e) {
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
);

export const userLogout = createAsyncThunk('user/logout', async (_, thunkAPI) => {
  try {
    localStorage.clear();
    window.location.href = '/login';
  } catch (error) {
    console.log(error);
  }
});

export const resetPassword = null;

export const getCurrentUser = createAsyncThunk(
  'organizations/getCurrentUser',
  async (value: string, thunkAPI) => {
    try {
      const response = await axiosInstance.post(`/getCurrentUser`, {
        organization: {
          name: value
        }
      });
      if (response.status === 201) {
        return response;
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (e) {
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
);

export const userSlice = createSlice({
  name: 'user',
  initialState: {
    user: {},
    currentUser: {
      username: '',
      firstName: '',
      lastName: '',
      email: '',
      admin: '',
      departments: [
        {
          name: '',
          admin: false
        }
      ]
    },
    isFetching: false,
    isSuccess: false,
    isError: false,
    isLoggedIn: false,
    errorMessage: '',

    isFetchingLoginUser: false,

    isFetchingRegisterUser: false,
    isSuccessRegisterUser: false,

    isFetchingGetCurrentUser: false,
    isSuccessGetCurrentUser: false,
    isErrorGetCurrentUser: false
  },
  reducers: {
    clearState: (state) => {
      state.isError = false;
      state.isSuccess = false;
      state.isFetching = false;
      return state;
    },
    clearStateLoginUser: (state) => {
      state.isFetchingLoginUser = false;
      return state;
    },
    clearStateRegisterUser: (state) => {
      state.isFetchingRegisterUser = false;
      state.isSuccessRegisterUser = false;
      return state;
    },
    clearStateGetCurrentUser: (state) => {
      state.isFetchingGetCurrentUser = false;
      state.isSuccessGetCurrentUser = false;
      state.isErrorGetCurrentUser = false;
      return state;
    }
  },
  extraReducers: {
    [getCurrentUser.fulfilled.toString()]: (state, { payload }) => {
      console.log('Get currentUser successful', payload);
      state.currentUser = payload.data.currentUser;
      state.isFetchingGetCurrentUser = false;
      state.isSuccessGetCurrentUser = true;
    },
    [getCurrentUser.pending.toString()]: (state, { payload }) => {
      console.log('Get currentUser pending');
      state.isFetchingGetCurrentUser = true;
      state.isSuccessGetCurrentUser = false;
    },
    [getCurrentUser.rejected.toString()]: (state, { payload }) => {
      console.log('Get currentUser rejected', payload);
      state.isFetchingGetCurrentUser = false;
      state.isErrorGetCurrentUser = true;
    },
    [loginUser.fulfilled.toString()]: (state, { payload }) => {
      localStorage.setItem('authTokens', JSON.stringify(payload.loginResponse));
      state.user = jwt_decode(payload.loginResponse.id_token);
      state.isFetchingLoginUser = false;
      window.location.reload();
    },
    [loginUser.pending.toString()]: (state) => {
      state.isFetchingLoginUser = true;
    },
    [loginUser.rejected.toString()]: (state, { payload }) => {
      state.isFetchingLoginUser = false;
      toast.error(payload.message, {
        type: 'error',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [registerUser.fulfilled.toString()]: (state, { payload }) => {
      state.isFetchingRegisterUser = false;
      state.isSuccessRegisterUser = true;
      toast.update(id, {
        render:
          'Registration successfull, please check your email address for further information.',
        type: 'success',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [registerUser.pending.toString()]: (state) => {
      state.isFetchingRegisterUser = true;
      toastPush('Registering...');
    },
    [registerUser.rejected.toString()]: (state, { payload }) => {
      state.isFetchingRegisterUser = false;
      toast.update(id, {
        render: `${payload.message}`,
        type: 'error',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [forgotPassword.fulfilled.toString()]: (state) => {
      toast.update(id, {
        render: "We've sent you an email with a link to reset your password.",
        type: 'success',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [forgotPassword.pending.toString()]: (state) => {
      toastPush('Checking...');
    },
    [forgotPassword.rejected.toString()]: (state, { payload }) => {
      toast.update(id, {
        render: `${payload.message}`,
        type: 'error',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    }
  }
});

export const { clearState, clearStateLoginUser, clearStateRegisterUser, clearStateGetCurrentUser } =
  userSlice.actions;

export default userSlice.reducer;
