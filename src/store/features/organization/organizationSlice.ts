import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { toast } from 'react-toastify';
import EnvVariables from '../../../constants/EnvVariables';
import axiosInstance from '../../../utils/axiosInstance';

let id: any;
function toastPush(message) {
  id = toast.loading(message, {
    position: 'bottom-right',
    theme: 'dark'
  });
}

const notificationProperties: any = {
  theme: 'dark',
  position: 'bottom-right',
  isLoading: false,
  autoClose: 5000,
  pauseOnHover: true
};

export const createOrganization = createAsyncThunk(
  'organizations/create',
  async (value: any, thunkAPI) => {
    console.log('ALOO', value);
    try {
      const response = await axiosInstance.post(`/createOrganization`, {
        organization: {
          name: value.name,
          enterprise: value.isEnterprise
        }
      });
      if (response.status === 201) {
        return response;
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (e) {
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
);

export const getOrganizations = createAsyncThunk(
  'organizations/getUserOrganizations',
  async (_, thunkAPI) => {
    try {
      const response = await axiosInstance.post('/getUserOrganizations', {});
      if (response.status === 201) {
        return response;
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (e) {
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
);

export const getOrganizationUsers = createAsyncThunk(
  'organizations/getOrganizationUsers',
  async (value: any, thunkAPI) => {
    try {
      const response = await axiosInstance.post('/getOrganizationUsers', {
        organization: {
          name: value
        }
      });
      if (response.status === 201) {
        return response;
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (e) {
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
);

export const inviteUser = createAsyncThunk('/inviteUser', async (value: any, thunkAPI) => {
  try {
    const response = await axiosInstance.post('/inviteUser', {
      organization: {
        name: value.organization.name
      },
      email: value.email
    });
    if (response.status === 201) {
      return response;
    } else {
      return thunkAPI.rejectWithValue(response);
    }
  } catch (e) {
    return thunkAPI.rejectWithValue(e.response.data);
  }
});

export const invitedUserRegistration = createAsyncThunk(
  'organizations/invitedUserRegistration',
  async (value: any, thunkAPI) => {
    try {
      const response = await fetch(`${EnvVariables.BACKEND}/invitedUserRegistration`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(value)
      });
      const data = await response.json();
      if (response.status === 201) {
        return data.user;
      } else {
        return thunkAPI.rejectWithValue(data);
      }
    } catch (e) {
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
);

export const invitedUserAccepted = createAsyncThunk(
  '/invitedUserAccepted',
  async (value: any, thunkAPI) => {
    try {
      const response = await axiosInstance.post('/invitedUserAccepted', value);
      if (response.status === 201) {
        return response;
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (e) {
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
);

export const invitedUserRejected = createAsyncThunk(
  '/invitedUserRejected',
  async (value: any, thunkAPI) => {
    try {
      const response = await axiosInstance.post('/invitedUserRejected', value);
      if (response.status === 201) {
        return response;
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (e) {
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
);

export const deleteUserFromOrganization = createAsyncThunk(
  '/deleteUserFromOrganization',
  async (value: any, thunkAPI) => {
    try {
      const response = await axiosInstance.post('/deleteUserFromOrganization', {
        organization: {
          name: value.organization.name
        },
        user: {
          username: value.user.username
        }
      });
      if (response.status === 201) {
        return response;
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (e) {
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
);

export const organizationSlice = createSlice({
  name: 'organizations',
  initialState: {
    organizations: [],
    currentOrganization: {
      name: ''
    },
    isFetching: false,
    isSuccess: false,
    isError: false,

    isFetchingisSuccessGetOrganizations: false,

    isSuccessInviteUser: false,
    isSuccessInvitedUserRegistration: false,
    isSuccessCreateOrganization: false,
    isSuccessDeleteUserFromOrganization: false,

    isFetchingInvitedUserAccepted: false,
    isSuccessInvitedUserAccepted: false,
    isFetchingInvitedUserRejected: false
  },
  reducers: {
    clearState: (state) => {
      state.isError = false;
      state.isSuccess = false;
      state.isFetching = false;
      return state;
    },
    changeCurrentOrganization: (state, action) => {
      state.currentOrganization = action.payload;
    },

    clearStateInviteUser: (state) => {
      state.isSuccessInviteUser = false;
      return state;
    },
    clearStateDeleteUserFromOrganization: (state) => {
      state.isSuccessDeleteUserFromOrganization = false;
      return state;
    },
    clearStateInvitedUserAccepted: (state) => {
      state.isFetchingInvitedUserAccepted = false;
      state.isSuccessInvitedUserAccepted = false;
    }
  },
  extraReducers: {
    [getOrganizations.fulfilled.toString()]: (state, { payload }) => {
      console.log('Get organizations successful', payload);
      state.organizations = payload.data.organizations;
      if (state.currentOrganization.name == '' && state.organizations.length > 0) {
        state.currentOrganization = payload.data.organizations[0];
      }
      state.isFetchingisSuccessGetOrganizations = false;
    },
    [getOrganizations.pending.toString()]: (state) => {
      console.log('Get organizations pending');
      state.isFetching = true;
    },
    [getOrganizations.rejected.toString()]: (state, { payload }) => {
      console.log('Get organizations rejected');
      state.isFetching = false;
      state.isError = true;
    },
    [getOrganizationUsers.fulfilled.toString()]: (state, { payload }) => {
      console.log('Success getOrganizationUsers', payload);
      state.isFetching = false;
      state.isSuccess = true;
    },
    [getOrganizationUsers.pending.toString()]: (state) => {
      console.log('Pending getOrganizationUsers');
      state.isFetching = true;
    },
    [getOrganizationUsers.rejected.toString()]: (state, { payload }) => {
      console.log('Rejected getOrganizationUsers' + payload);
      state.isFetching = false;
      state.isError = true;
    },
    [createOrganization.fulfilled.toString()]: (state, { payload }) => {
      toast.update(id, {
        render: 'Succesfully! Created organization.',
        type: 'success',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
      state.isSuccessCreateOrganization = true;
      window.location.reload();
    },
    [createOrganization.pending.toString()]: (state) => {
      toastPush('Creating organization...');
    },
    [createOrganization.rejected.toString()]: (state, { payload }) => {
      toast.update(id, {
        render: `${payload.message}`,
        type: 'error',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [inviteUser.fulfilled.toString()]: (state, { payload }) => {
      state.isSuccessInviteUser = true;
      toast.update(id, {
        render: 'Succesfully! Invited User to organization.',
        type: 'success',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [inviteUser.pending.toString()]: (state) => {
      toastPush('Inviting user to organization...');
    },
    [inviteUser.rejected.toString()]: (state, { payload }) => {
      toast.update(id, {
        render: `${payload.message}`,
        type: 'error',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [invitedUserRegistration.fulfilled.toString()]: (state, { payload }) => {
      state.isSuccessInvitedUserRegistration = true;
      toast.update(id, {
        render: 'Succesfully! Invited User to organization.',
        type: 'success',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [invitedUserRegistration.pending.toString()]: (state) => {
      toastPush('Inviting user to organization...');
    },
    [invitedUserRegistration.rejected.toString()]: (state, { payload }) => {
      toast.update(id, {
        render: `${payload.message}`,
        type: 'error',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [invitedUserAccepted.fulfilled.toString()]: (state, { payload }) => {
      state.isSuccessInvitedUserAccepted = true;
      state.isFetchingInvitedUserAccepted = false;
      toast.update(id, {
        render: 'Succesfully! Added to the organization.',
        type: 'success',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
      window.location.href = '/dashboard';
    },
    [invitedUserAccepted.pending.toString()]: (state) => {
      state.isFetchingInvitedUserAccepted = true;
      toastPush('Adding to organization...');
    },
    [invitedUserAccepted.rejected.toString()]: (state, { payload }) => {
      state.isFetchingInvitedUserAccepted = false;
      toast.update(id, {
        render: `${payload.message}`,
        type: 'error',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [invitedUserRejected.fulfilled.toString()]: (state, { payload }) => {
      state.isFetchingInvitedUserRejected = false;
      toast.update(id, {
        render: 'Succesfully! Rejected to the organization.',
        type: 'success',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [invitedUserRejected.pending.toString()]: (state) => {
      state.isFetchingInvitedUserRejected = true;
      toastPush('Rejecting to the organization...');
    },
    [invitedUserRejected.rejected.toString()]: (state, { payload }) => {
      state.isFetchingInvitedUserRejected = false;
      toast.update(id, {
        render: `${payload.message}`,
        type: 'error',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [deleteUserFromOrganization.pending.toString()]: () => {
      toastPush('Deleting user from organization...');
    },
    [deleteUserFromOrganization.fulfilled.toString()]: (state) => {
      state.isSuccessDeleteUserFromOrganization = true;
      toast.update(id, {
        render: 'Succesfully! User deleted from organization.',
        type: 'success',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [deleteUserFromOrganization.rejected.toString()]: (state, { payload }) => {
      toast.update(id, {
        render: `${payload.message}`,
        type: 'error',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    }
  }
});

export const {
  clearState,
  changeCurrentOrganization,
  clearStateInviteUser,
  clearStateDeleteUserFromOrganization,
  clearStateInvitedUserAccepted
} = organizationSlice.actions;

export default organizationSlice.reducer;
