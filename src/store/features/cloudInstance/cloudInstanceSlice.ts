import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import EnvVariables from '../../../constants/EnvVariables';

export const createCloudInstance = createAsyncThunk(
  'virtualcluster/createCloudInstance',
  async (values: any, thunkAPI) => {
    try {
      const response = await fetch(`${EnvVariables.BACKEND}/createCloudInstance`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(values)
      });
      const data = await response.json();
      console.log(data);
      if (response.status === 201) {
        return data;
      } else {
        return thunkAPI.rejectWithValue(data);
      }
    } catch (error) {
      console.log('Error createCloudInstance ', error);
    }
  }
);

export const createCloudInstanceSlice = createSlice({
  name: 'virtualcluster',
  initialState: {
    isFetching: false,
    isSuccess: false,
    isError: false,
    isLoggedIn: false,
    errorMessage: ''
  },
  reducers: {
    clearState: (state) => {
      state.isError = false;
      state.isSuccess = false;
      state.isFetching = false;
      return state;
    }
  },
  extraReducers: {
    [createCloudInstance.fulfilled.toString()]: (state, { payload }) => {
      console.log('createVirtualCluster successful', payload);
      state.isFetching = false;
      state.isSuccess = true;
    },
    [createCloudInstance.pending.toString()]: (state, { payload }) => {
      console.log('createVirtualCluster pending');
      state.isFetching = true;
      state.isSuccess = false;
    },
    [createCloudInstance.rejected.toString()]: (state, { payload }) => {
      console.log('createVirtualCluster rejected', payload);
      state.isFetching = false;
      state.isError = true;
    }
  }
});

export const { clearState } = createCloudInstanceSlice.actions;

export default createCloudInstanceSlice.reducer;
