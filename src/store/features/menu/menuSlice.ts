import { createSlice } from '@reduxjs/toolkit';

interface IMenu {
  isOpenMenu: boolean;
  isCreateMenu: boolean;
}

const initialState: IMenu = {
  isOpenMenu: true,
  isCreateMenu: false
};

export const menu = createSlice({
  name: 'menu',
  initialState: initialState,
  reducers: {
    setIsOpenMenu: (state) => {
      state.isOpenMenu = !state.isOpenMenu;
    },
    setIsCreateMenu: (state) => {
      state.isCreateMenu = !state.isCreateMenu;
    }
  }
});

export const { setIsOpenMenu, setIsCreateMenu } = menu.actions;

export default menu.reducer;
