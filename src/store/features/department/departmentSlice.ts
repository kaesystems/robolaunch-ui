import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { toast } from 'react-toastify';
import axiosInstance from '../../../utils/axiosInstance';

let id: any;
function toastPush(message) {
  id = toast.loading(message, {
    position: 'bottom-right',
    theme: 'dark'
  });
}

const notificationProperties: any = {
  theme: 'dark',
  position: 'bottom-right',
  isLoading: false,
  autoClose: 5000,
  pauseOnHover: true
};

export const createDepartment = createAsyncThunk(
  'departments/createDepartments',
  async (values: any, thunkAPI) => {
    try {
      const response = await axiosInstance.post('/createDepartment', {
        organization: {
          name: values.organizationName
        },
        department: {
          name: values.departmentName
        }
      });
      if (response.status === 201) {
        return response;
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const getDepartments = createAsyncThunk(
  'departments/getDepartments',
  async (values: any, thunkAPI) => {
    try {
      const response = await axiosInstance.post('/getDepartments', {
        organization: {
          name: values
        }
      });
      if (response.status === 201) {
        return response;
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const renameDepartments = createAsyncThunk(
  'departments/changeDepartmentName',
  async (values: any, thunkAPI) => {
    try {
      const response = await axiosInstance.post('/changeDepartmentName', {
        newName: values.value.departmentName,
        organization: {
          name: values.currentOrganization.name
        },
        department: {
          name: values.row
        }
      });
      if (response.status === 201) {
        return response;
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const deleteDepartments = createAsyncThunk(
  'departments/deleteDepartments',
  async (values: any, thunkAPI) => {
    try {
      const response = await axiosInstance.post('/deleteDepartment', {
        organization: {
          name: values.currentOrganization.name
        },
        department: values.row
      });
      if (response.status === 201) {
        return response;
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const getDepartmentUsers = createAsyncThunk(
  'departments/getDepartmentUsers',
  async (values: any, thunkAPI) => {
    try {
      const response = await axiosInstance.post('/getDepartmentUsers', {
        organization: {
          name: values.organization
        },
        department: {
          name: values.department
        }
      });
      if (response.status === 201) {
        return response;
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (e) {
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
);

export const addUserToDepartment = createAsyncThunk(
  'departments/addUserToDepartment',
  async (values: any, thunkAPI) => {
    try {
      const response = await axiosInstance.post('/addUserToDepartment', {
        organization: {
          name: values.organization
        },
        department: {
          name: values.department
        },
        user: {
          username: values.username
        }
      });
      if (response.status === 201) {
        return response;
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (e) {
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
);
export const deleteUserFromDepartment = createAsyncThunk(
  'departments/deleteUserFromDepartment',
  async (values: any, thunkAPI) => {
    try {
      const response = await axiosInstance.post('/deleteUserFromDepartment', {
        organization: {
          name: values.organization
        },
        department: {
          name: values.department
        },
        user: {
          username: values.username
        }
      });
      if (response.status === 201) {
        return response;
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (e) {
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
);

export const addUserToDepartmentAsManager = createAsyncThunk(
  'departments/addUserToDepartmentAsManager',
  async (values: any, thunkAPI) => {
    try {
      const response = await axiosInstance.post('/addUserToDepartmentAsManager', values);
      if (response.status === 201) {
        return response;
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (e) {
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
);

export const deleteUserManagershipFromDepartment = createAsyncThunk(
  'departments/deleteUserManagershipFromDepartment',
  async (values: any, thunkAPI) => {
    try {
      const response = await axiosInstance.post('/deleteUserManagershipFromDepartment', values);
      if (response.status === 201) {
        return response;
      } else {
        return thunkAPI.rejectWithValue(response);
      }
    } catch (e) {
      return thunkAPI.rejectWithValue(e.response.data);
    }
  }
);

export const departmentSlice = createSlice({
  name: 'departments',
  initialState: {
    departments: [],

    isFetching: false,
    isSuccess: false,
    isError: false,

    isSuccessCreateDepartment: false,

    isSuccessRenameDepartment: false,

    isSuccessDeleteDepartment: false,

    isSuccessDeleteUserFromDepartment: false,

    isSuccessAddUserToDepartment: false,

    isSuccessDeleteUserManagershipFromDepartment: false,

    isSuccessAddUserToDepartmentAsManager: false
  },
  reducers: {
    clearState: (state) => {
      state.isError = false;
      state.isSuccess = false;
      state.isFetching = false;
      return state;
    },
    clearStateAddUserToDepartment: (state) => {
      state.isSuccessAddUserToDepartment = false;
      return state;
    },
    clearStateDeleteUserManagershipFromDepartment: (state) => {
      state.isSuccessDeleteUserManagershipFromDepartment = false;
      return state;
    },
    clearStateAddUserToDepartmentAsManager: (state) => {
      state.isSuccessAddUserToDepartmentAsManager = false;
      return state;
    },
    clearStateCreateDepartment: (state) => {
      state.isSuccessCreateDepartment = false;
      return state;
    },
    clearStateRenameDepartment: (state) => {
      state.isSuccessRenameDepartment = false;
      return state;
    },
    clearStateDeleteDepartment: (state) => {
      state.isSuccessDeleteDepartment = false;
      return state;
    },
    clearStateDeleteUserFromDepartment: (state) => {
      state.isSuccessDeleteUserFromDepartment = false;
      return state;
    }
  },
  extraReducers: {
    [createDepartment.fulfilled.toString()]: (state, { payload }) => {
      state.isSuccessCreateDepartment = true;
      toast.update(id, {
        render: 'Department created successfully.',
        type: 'success',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [createDepartment.pending.toString()]: (state) => {
      toastPush('Creating Department...');
    },
    [createDepartment.rejected.toString()]: (state, { payload }) => {
      toast.update(id, {
        render: `${payload.message}`,
        type: 'error',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [addUserToDepartment.fulfilled.toString()]: (state, { payload }) => {
      toast.update(id, {
        render: 'User added to department successfully.',
        type: 'success',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
      state.isSuccessAddUserToDepartment = true;
    },
    [addUserToDepartment.pending.toString()]: (state) => {
      toastPush('Adding user to department...');
    },
    [addUserToDepartment.rejected.toString()]: (state, { payload }) => {
      toast.update(id, {
        render: `${payload.message}`,
        type: 'error',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [renameDepartments.fulfilled.toString()]: (state, { payload }) => {
      state.departments = payload.data;
      state.isSuccessRenameDepartment = true;
      toast.update(id, {
        render: 'Renamed to department successfully.',
        type: 'success',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [renameDepartments.pending.toString()]: (state) => {
      toastPush('In Progress...');
    },
    [renameDepartments.rejected.toString()]: (state, { payload }) => {
      toast.update(id, {
        render: `${payload.message}`,
        type: 'error',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [deleteDepartments.fulfilled.toString()]: (state, { payload }) => {
      state.departments = payload.data;
      state.isSuccessDeleteDepartment = true;
      toast.update(id, {
        render: 'Department deleted successfully.',
        type: 'success',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [deleteDepartments.pending.toString()]: (state) => {
      toastPush('Deleting Department...');
    },
    [deleteDepartments.rejected.toString()]: (state, { payload }) => {
      toast.update(id, {
        render: `${payload.message}`,
        type: 'error',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [deleteUserFromDepartment.fulfilled.toString()]: (state, { payload }) => {
      state.isSuccessDeleteUserFromDepartment = true;
      toast.update(id, {
        render: 'User deleted from department successfully.',
        type: 'success',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [deleteUserFromDepartment.pending.toString()]: (state) => {
      toastPush('Deleting User from Department...');
    },
    [deleteUserFromDepartment.rejected.toString()]: (state, { payload }) => {
      toast.update(id, {
        render: `${payload.message}`,
        type: 'error',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [deleteUserManagershipFromDepartment.fulfilled.toString()]: (state, { payload }) => {
      state.isSuccessDeleteUserFromDepartment = true;
      toast.update(id, {
        render: 'User managership deleted successfully.',
        type: 'success',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [deleteUserManagershipFromDepartment.pending.toString()]: (state) => {
      toastPush('Deleting managership from department...');
    },
    [deleteUserManagershipFromDepartment.rejected.toString()]: (state, { payload }) => {
      toast.update(id, {
        render: `${payload.message}`,
        type: 'error',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [addUserToDepartmentAsManager.fulfilled.toString()]: (state, { payload }) => {
      state.isSuccessAddUserToDepartmentAsManager = true;
      toast.update(id, {
        render: 'User managership added successfully.',
        type: 'success',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [addUserToDepartmentAsManager.pending.toString()]: (state) => {
      toastPush('Adding managership from department...');
    },
    [addUserToDepartmentAsManager.rejected.toString()]: (state, { payload }) => {
      toast.update(id, {
        render: `${payload.message}`,
        type: 'error',
        theme: notificationProperties.theme,
        position: notificationProperties.position,
        isLoading: notificationProperties.isLoading,
        autoClose: notificationProperties.autoClose,
        pauseOnHover: notificationProperties.pauseOnHover
      });
    },
    [getDepartments.fulfilled.toString()]: (state, { payload }) => {
      console.log('Success Get OrganizationDepartments: ', payload.data);
      state.departments = payload.data;
      state.isFetching = false;
      state.isSuccess = true;
    },
    [getDepartments.pending.toString()]: (state) => {
      console.log('Departments: pending');
      state.isFetching = true;
    },
    [getDepartments.rejected.toString()]: (state, { payload }) => {
      console.log('Departments: rejected' + payload.data);
      state.isFetching = false;
      state.isError = true;
    },
    [getDepartmentUsers.fulfilled.toString()]: (state, { payload }) => {
      console.log('Success Get DepartmentUsers', payload);
      state.isFetching = false;
      state.isSuccess = true;
    },
    [getDepartmentUsers.pending.toString()]: (state) => {
      console.log('Pending Get DepartmentUsers');
      state.isFetching = true;
    },
    [getDepartmentUsers.rejected.toString()]: (state, { payload }) => {
      console.log('Rejected Get DepartmentUsers' + payload);
      state.isFetching = false;
      state.isError = true;
    }
  }
});

export const {
  clearState,
  clearStateCreateDepartment,
  clearStateRenameDepartment,
  clearStateDeleteDepartment,
  clearStateDeleteUserFromDepartment,
  clearStateAddUserToDepartment,
  clearStateDeleteUserManagershipFromDepartment,
  clearStateAddUserToDepartmentAsManager
} = departmentSlice.actions;

export default departmentSlice.reducer;
