import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/dist/query';
import { pokemonApi } from './services/testings';

import menuReducer from './features/menu/menuSlice';
import currentFleetReducer from './features/fleet/FleetSlice';
import userReducer from './features/user/userSlice';
import organizationReducer from './features/organization/organizationSlice';
import departmentReducer from './features/department/departmentSlice';
import cloudInstanceReducer from './features/cloudInstance/cloudInstanceSlice';

import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

/* Data must be persisted in order not to lose them on page refresh. */
/* Redux-persist basically saves the data into localStorage so the data is not lost on refresh. */
const persistConfigCurrentFleet = {
  key: 'currentFleet',
  version: 1,
  storage
};

const persistConfigOrganizations = {
  key: 'organizations',
  version: 1,
  storage
};

const persistConfigUser = {
  key: 'user',
  version: 1,
  storage
};

const persistConfigDepartment = {
  key: 'department',
  version: 1,
  storage
};

const persistedReducerUser = persistReducer<any, any>(persistConfigUser, userReducer);

const persistedReducerCurrentFleet = persistReducer<any, any>(
  persistConfigCurrentFleet,
  currentFleetReducer
);

const persistedReducerOrganizations = persistReducer<any, any>(
  persistConfigOrganizations,
  organizationReducer
);

const persistReducerDepartment = persistReducer<any, any>(
  persistConfigDepartment,
  departmentReducer
);

const store = configureStore({
  reducer: {
    menu: menuReducer,
    currentFleet: persistedReducerCurrentFleet,
    user: persistedReducerUser,
    organization: persistedReducerOrganizations,
    department: persistReducerDepartment,
    cloudInstance: cloudInstanceReducer,
    [pokemonApi.reducerPath]: pokemonApi.reducer
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false
    })
});

setupListeners(store.dispatch);

export const persistor = persistStore(store);

export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;

export default store;
