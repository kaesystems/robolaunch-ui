import axios from 'axios';
import jwt_decode from 'jwt-decode';
import dayjs from 'dayjs';
import EnvVariables from '../constants/EnvVariables';

let authTokens = localStorage.getItem('authTokens')
  ? JSON.parse(localStorage.getItem('authTokens'))
  : null;

const axiosInstance = axios.create({
  baseURL: EnvVariables.BACKEND,
  headers: { Authorization: `Bearer ${authTokens?.id_token}` }
});

axiosInstance.interceptors.request.use(async (req) => {
  if (!authTokens) {
    authTokens = localStorage.getItem('authTokens')
      ? JSON.parse(localStorage.getItem('authTokens'))
      : null;
    req.headers.Authorization = `Bearer ${authTokens?.id_token}`;
  }

  const user: any = jwt_decode(authTokens.id_token);
  const isExpired = dayjs.unix(user.exp).diff(dayjs()) < 1;
  console.log('is token expired? ', isExpired);

  if (!isExpired) return req;
  console.log('token expired');
  const response = await axios.post(`${EnvVariables.BACKEND}/refreshToken`, {
    loginRequest: {
      client_id: EnvVariables.CLIENT_ID,
      client_secret: EnvVariables.CLIENT_SECRET,
      grant_type: 'refresh_token',
      refresh_token: authTokens.refresh_token
    }
  });
  console.log('Refresh response', response);
  localStorage.setItem('authTokens', JSON.stringify(response.data.loginResponse));
  req.headers.Authorization = `Bearer ${response.data.loginResponse.id_token}`;
  console.log('token renewed');
  window.location.reload();
  return req;
});

export default axiosInstance;
