export const createModalStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: '66%',
    minHeight: '40%',
    maxHeight: '80%',
    overflow: 'auto',
    padding: 0,
    zIndex: '9999',
    border: '2px solid var(--layer-500)',
    borderRadius: 'var(--border-radius-sm)',
    backgroundColor: 'var(--layer-100)'
  },
  overlay: {
    background: 'rgba(0, 0, 0, 0.50)',
    zIndex: '999999'
  }
};
