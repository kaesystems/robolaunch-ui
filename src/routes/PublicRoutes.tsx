import React from 'react';
import { useLocation } from 'react-router';
import { Navigate } from 'react-router-dom';

const PublicRoutes = ({ children }) => {
  const user: object = JSON.parse(localStorage.getItem('authTokens'))?.id_token;
  const url = useLocation();
  if (user) {
    return <Navigate to="/dashboard" />;
  }
  if (url.pathname === '/') {
    return <Navigate to="/login" />;
  }
  return children;
};

export default PublicRoutes;
