import React from 'react';
import { Navigate, useLocation } from 'react-router-dom';
import Header from '../layout/Header';
import PageLayout from '../layout/PageLayout';
import { useAppSelector } from '../hooks/redux';

interface IPrivateRoute {
  children: any;
  singlePage?: boolean;
  header?: boolean;
}

const PrivateRoute = ({ children, singlePage = false, header = false }: IPrivateRoute) => {
  let user = JSON.parse(localStorage.getItem('authTokens'))?.id_token;
  const { organizations } = useAppSelector((state) => state.organization);
  const url = useLocation();

  user = true; // DELETE THİS LINE!
  if (user) {
    if (url.pathname === '/') {
      return <Navigate to="/dashboard" />;
    }
    if (singlePage) {
      return children;
    }
    if (header) {
      return (
        <>
          <Header />
          {children}
        </>
      );
    }
    return <PageLayout>{children}</PageLayout>;
  }
  return <Navigate to="/login" />;
};

export default PrivateRoute;
