import React, { useEffect, useState } from 'react';
import { Route, Routes } from 'react-router-dom';
import Dashboard from './pages/private/Dashboard/Dashboard';
import Marketplace from './pages/private/Marketplace/Marketplace';
import Accounting from './pages/private/Accounting/Accounting';
import NotFound from './pages/private/NotFound/NotFound';
import RobotConfiguration from './pages/private/Organization/Department/RoboticsClouds/Fleet/Robot/RobotConfiguration/RobotConfiguration';
import RobotDevelopment from './pages/private/Organization/Department/RoboticsClouds/Fleet/Robot/RobotDevelopment/RobotDevelopment';
import RobotTeleoperation from './pages/private/Organization/Department/RoboticsClouds/Fleet/Robot/RobotTeleoperation/RobotTeleoperation';
import RobotTasks from './pages/private/Organization/Department/RoboticsClouds/Fleet/Robot/RobotTasks/RobotTasks';
import RobotROSBAG from './pages/private/Organization/Department/RoboticsClouds/Fleet/Robot/RobotRosbag/RobotRosbag';
import RobotPerformance from './pages/private/Organization/Department/RoboticsClouds/Fleet/Robot/RobotPerformance/RobotPerformance';
import RobotFault from './pages/private/Organization/Department/RoboticsClouds/Fleet/Robot/RobotFault/RobotFault';
import RobotSecurity from './pages/private/Organization/Department/RoboticsClouds/Fleet/Robot/RobotSecurity/RobotSecurity';
import RobotVDI from './pages/private/Organization/Department/RoboticsClouds/Fleet/Robot/RobotVDI/RobotVDI';
import CreateRoboticsCloud from './pages/private/Organization/Department/RoboticsClouds/CreateRoboticsCloud';
import Register from './pages/public/Register/Register';
import { useAppDispatch } from './hooks/redux';
import DeployRobot from './pages/private/Organization/Department/RoboticsClouds/DeployRobot/DeployRobot';
import Login from './pages/public/Login/Login';
import ProfileLayout from './pages/private/Profile/ProfileLayout';
import { ToastContainer } from 'react-toastify';
import ForgotPassword from './pages/public/ForgotPassword/ForgotPassword';
import { getOrganizations } from './store/features/organization/organizationSlice';
import { getCurrentUser } from './store/features/user/userSlice';
import Users from './pages/private/UserRoleManagement/Tables/OrganizationUsersTable';
import Departments from './pages/private/UserRoleManagement/Tables/DepartmentsTable';
import DepartmentTable from './pages/private/UserRoleManagement/Tables/DepartmentTable';
import Loading from './pages/private/Loading/Loading';
import PrivateRoute from './routes/PrivateRoutes';
import RegistrationInvitedUser from './pages/public/InvitedUserRegistration/RegistrationInvitedUser';
import PublicRoutes from './routes/PublicRoutes';
import InviteOrganization from './pages/private/InviteOrganization/InviteOrganiztion';
import RateLimit from './pages/public/RateLimit/RateLimit';
import InvitedUsers from './pages/private/UserRoleManagement/Tables/InvitedUsersTable';
import { useLocation } from 'react-router';
import Organization from './pages/private/Organization/Organization';
import Department from './pages/private/Organization/Department/Department';
import Instance from './pages/private/Organization/Department/RoboticsClouds/RoboticsClouds';
import Fleet from './pages/private/Organization/Department/RoboticsClouds/Fleet/Fleet';
import TaskManagement from './pages/private/Organization/Department/RoboticsClouds/Fleet/TaskManagement/TaskManagement';
import slugify from 'react-slugify';
import ConnectedPhysicalInstances from './pages/private/Organization/Department/RoboticsClouds/ConnectedPhysicalInstances/ConnectedPhysicalInstances';
import Create from './pages/private/Create/Create';
import Robot from './pages/private/Organization/Department/RoboticsClouds/Fleet/Robot/Robot';
import 'animate.css';
import RegisterPhysicalInstance from './pages/private/Organization/Department/RoboticsClouds/ConnectedPhysicalInstances/RegisterPhysicalInstance';
import CreateRobot from './pages/private/Organization/Department/RoboticsClouds/Fleet/CreateDeployRobot/CreateRobot';

const App = () => {
  const user: object = JSON.parse(localStorage.getItem('authTokens'))?.id_token;
  const dispatch = useAppDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [isUnmounted, setIsUnmounted] = useState(false);
  const url = useLocation();

  useEffect(() => {
    if (user) {
      setIsLoading(true);
      (async () => {
        await dispatch(getOrganizations()).then((getOrganizationsRes: any) => {
          (async () => {
            await dispatch(
              getCurrentUser(getOrganizationsRes.payload.data.organizations[0].name)
            ).then((getCurrentUsersRes: any) => {
              unMountLoading();
            });
          })();
        });
      })();
    }
  }, []);

  const unMountLoading = () => {
    setIsUnmounted(true);
    setTimeout(() => {
      setIsLoading(false);
    }, 1250);
  };

  if (isLoading) {
    return <Loading unmount={isUnmounted} />;
  }

  const mockCurrentOrg = 'Organization1';

  return (
    <>
      <ToastContainer />
      <Routes>
        <Route
          path={`/login`}
          element={
            <PublicRoutes>
              <Login />
            </PublicRoutes>
          }
        />
        <Route
          path={`/register`}
          element={
            <PublicRoutes>
              <Register />
            </PublicRoutes>
          }
        />
        <Route
          path={`/forgot-password`}
          element={
            <PublicRoutes>
              <ForgotPassword />
            </PublicRoutes>
          }
        />
        <Route
          path={`/invited-user`}
          element={
            <PublicRoutes>
              <RegistrationInvitedUser />
            </PublicRoutes>
          }
        />
        <Route
          path={`/rate-limit`}
          element={
            <PublicRoutes>
              <RateLimit />
            </PublicRoutes>
          }
        />
        <Route
          path={`/dashboard`}
          element={
            <PrivateRoute>
              <Dashboard />
            </PrivateRoute>
          }
        />

        <Route
          path={`/create-robotics-cloud`}
          element={
            <PrivateRoute singlePage={true}>
              <CreateRoboticsCloud />
            </PrivateRoute>
          }
        />
        <Route
          path={`/register-physical-instance`}
          element={
            <PrivateRoute singlePage={true}>
              <RegisterPhysicalInstance />
            </PrivateRoute>
          }
        />
        <Route
          path="/marketplace"
          element={
            <PrivateRoute>
              <Marketplace />
            </PrivateRoute>
          }
        />
        <Route
          path="/accounting"
          element={
            <PrivateRoute>
              <Accounting />
            </PrivateRoute>
          }
        />
        <Route
          path="/profile"
          element={
            <PrivateRoute>
              <ProfileLayout />
            </PrivateRoute>
          }
        />
        <Route
          path={`/invite-organization`}
          element={
            <PrivateRoute singlePage={true}>
              <InviteOrganization />
            </PrivateRoute>
          }
        />
        <Route
          path={`/create`}
          element={
            <PrivateRoute>
              <Create />
            </PrivateRoute>
          }
        />
        <Route
          path={`/create-robot`}
          element={
            <PrivateRoute singlePage={true}>
              <CreateRobot />
            </PrivateRoute>
          }
        />
        <Route
          path={`/deploy-robot`}
          element={
            <PrivateRoute singlePage={true}>
              <DeployRobot />
            </PrivateRoute>
          }
        />
        <Route
          path={`/${slugify(mockCurrentOrg)}/marketplace`}
          element={
            <PrivateRoute>
              <Marketplace />
            </PrivateRoute>
          }
        />
        <Route
          path={`/${slugify(mockCurrentOrg)}/accounting`}
          element={
            <PrivateRoute>
              <Accounting />
            </PrivateRoute>
          }
        />

        <Route
          path={`/${slugify(mockCurrentOrg)}/user-role-management/users`}
          element={
            <PrivateRoute>
              <Users />
            </PrivateRoute>
          }
        />
        <Route
          path={`/${slugify(mockCurrentOrg)}/user-role-management/departments`}
          element={
            <PrivateRoute>
              <Departments />
            </PrivateRoute>
          }
        />
        <Route
          path={`/${slugify(mockCurrentOrg)}/user-role-management/invited-users`}
          element={
            <PrivateRoute>
              <InvitedUsers />
            </PrivateRoute>
          }
        />
        <Route
          path={`/${slugify(mockCurrentOrg)}/user-role-management/department/:id`}
          element={
            <PrivateRoute>
              <DepartmentTable />
            </PrivateRoute>
          }
        />

        <Route
          path={`${slugify(mockCurrentOrg)}`}
          element={
            <PrivateRoute>
              <Organization />
            </PrivateRoute>
          }
        />

        <Route
          path={`/${slugify(mockCurrentOrg)}/${url.pathname.split('/')[2]}`}
          element={
            <PrivateRoute>
              <Department />
            </PrivateRoute>
          }
        />

        <Route
          path={`/${slugify(mockCurrentOrg)}/${url.pathname.split('/')[2]}/${
            url.pathname.split('/')[3]
          }`}
          element={
            <PrivateRoute>
              <Instance />
            </PrivateRoute>
          }
        />
        <Route
          path={`/${slugify(mockCurrentOrg)}/${url.pathname.split('/')[2]}/${
            url.pathname.split('/')[3]
          }/connected-physical-instances`}
          element={
            <PrivateRoute>
              <ConnectedPhysicalInstances />
            </PrivateRoute>
          }
        />
        <Route
          path={`/${slugify(mockCurrentOrg)}/${url.pathname.split('/')[2]}/${
            url.pathname.split('/')[3]
          }/${url.pathname.split('/')[4]}`}
          element={
            <PrivateRoute>
              <Fleet />
            </PrivateRoute>
          }
        />
        <Route
          path={`/${slugify(mockCurrentOrg)}/${url.pathname.split('/')[2]}/${
            url.pathname.split('/')[3]
          }/${url.pathname.split('/')[4]}/:id`}
          element={
            <PrivateRoute>
              <Robot />
            </PrivateRoute>
          }
        />
        <Route
          path={`/${slugify(mockCurrentOrg)}/${url.pathname.split('/')[2]}/${
            url.pathname.split('/')[3]
          }/${url.pathname.split('/')[4]}/task-management`}
          element={
            <PrivateRoute>
              <TaskManagement />
            </PrivateRoute>
          }
        />
        <Route
          path={`robots/:id/development`}
          element={
            <PrivateRoute>
              <RobotDevelopment />
            </PrivateRoute>
          }
        />
        <Route
          path={`robots/:id/vdi`}
          element={
            <PrivateRoute singlePage={true}>
              <RobotVDI />
            </PrivateRoute>
          }
        />
        <Route
          path={`robots/:id/teleoperation`}
          element={
            <PrivateRoute>
              <RobotTeleoperation />
            </PrivateRoute>
          }
        />
        <Route
          path={`robots/:id/tasks`}
          element={
            <PrivateRoute>
              <RobotTasks />
            </PrivateRoute>
          }
        />
        <Route
          path={`robots/:id/rosbag`}
          element={
            <PrivateRoute>
              <RobotROSBAG />
            </PrivateRoute>
          }
        />
        <Route
          path={`robots/:id/performance`}
          element={
            <PrivateRoute>
              <RobotPerformance />
            </PrivateRoute>
          }
        />
        <Route
          path={`robots/:id/fault`}
          element={
            <PrivateRoute>
              <RobotFault />
            </PrivateRoute>
          }
        />
        <Route
          path={`robots/:id/security`}
          element={
            <PrivateRoute>
              <RobotSecurity />
            </PrivateRoute>
          }
        />
        <Route
          path={`robots/:id/configuration`}
          element={
            <PrivateRoute>
              <RobotConfiguration />
            </PrivateRoute>
          }
        />

        <Route
          path="*"
          element={
            <PrivateRoute>
              <NotFound />
            </PrivateRoute>
          }
        />
      </Routes>
    </>
  );
};

export default App;
